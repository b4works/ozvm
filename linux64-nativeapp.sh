# *************************************************************************************
# Build script for ExcelsiorJet V10 Linux Native application of OZvm for AMD64
# (C) Gunther Strube (hello@bits4fun.net) 2000-2024
#
# OZvm is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with OZvm;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *************************************************************************************

# ExcelsiorJet must have been installed and available via PATH

# first compile jar file
./makejar.sh

# build Excelsior Jet native
jc =p =a "./linux64-z88.prj"

# cleanup prev. build of distributable
rm -fR ./z88-native

# generate ozvm as distributable with ROM images
xpack linux64-z88.jpn

echo creating compressed archive of the 'z88-native' distributable folder...
rm -f z88-native-linux64.tar z88-native-linux64.tar.bz2
tar vcf z88-native-linux64.tar z88-native
bzip2 -v z88-native-linux64.tar
ls -lh z88-native-linux64.tar.bz2

