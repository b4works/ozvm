/*
 * DebugGui.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Gui framework OZvm debugging mode.
 */
public class DebugGui extends JFrame {

    private static final class singletonContainer {
        static DebugGui singleton = new DebugGui();
    }

    public static DebugGui getInstance() {
        if (singletonContainer.singleton == null)
            singletonContainer.singleton = new DebugGui();
        return singletonContainer.singleton;
    }

    private Z80Processor z80;
    private Blink bl;

    private javax.swing.JPanel jZ88HardwareView;
    private javax.swing.JTextField z80A;
    private javax.swing.JTextField z80F;
    private javax.swing.JTextField z80Flags;
    private javax.swing.JTextField z80Ax;
    private javax.swing.JTextField z80Fx;
    private javax.swing.JTextField z80BC;
    private javax.swing.JTextField z80DE;
    private javax.swing.JTextField z80HL;
    private javax.swing.JTextField z80BCx;
    private javax.swing.JTextField z80DEx;
    private javax.swing.JTextField z80HLx;
    private javax.swing.JTextField z80IX;
    private javax.swing.JTextField z80IY;
    private javax.swing.JTextField z80SP;
    private javax.swing.JTextField z80PC;

    private javax.swing.JTextField blSR0;
    private javax.swing.JTextField blSR1;
    private javax.swing.JTextField blSR2;
    private javax.swing.JTextField blSR3;

    private javax.swing.JTextField blTIM0;
    private javax.swing.JTextField blTIM1;
    private javax.swing.JTextField blTIM2;
    private javax.swing.JTextField blTIM3;
    private javax.swing.JTextField blTIM4;

    private javax.swing.JTextField blINT;
    private javax.swing.JTextField blSTA;
    private javax.swing.JTextField blTSTA;

    private JTable stackView;
    private Object[][] stackData; // The stack view (2 columns)
    private final int stackViewRows = 256;
    private final int stackViewCols = 2;

    JConsole console;

    /**
     * This is the default constructor
     */
    private DebugGui() {
        super();
        initialize();
    }

    /**
     * This method initializes the z88 debug window and menus
     */
    private void initialize() {

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        z80 = Z88.getInstance().getProcessor();
        bl = Z88.getInstance().getBlink();

        setIconImage(new ImageIcon(this.getClass().getResource("/pixel/debug.gif")).getImage());

        stackData = new String[stackViewRows][stackViewCols];
        console = new JConsole();
        
        getContentPane().add(console, BorderLayout.CENTER);
        getContentPane().add(getZ88MachinePanel(), BorderLayout.LINE_END);

        this.setTitle("OZvm Debugger");
        this.setResizable(true);
        
        this.setSize(OZvm.getInstance().getDebugWinWidth(),OZvm.getInstance().getDebugWinHeight());
        this.setVisible(true);

        this.addWindowListener(new java.awt.event.WindowAdapter() {

            public void windowClosing(java.awt.event.WindowEvent e) {
                // when Debug Gui is closed, clear reference to it..
                singletonContainer.singleton = null;
            }
        });

        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                // Remember Debug Window size in ozvm.ini
                OZvm.getInstance().setDebugWinWidth(e.getComponent().getWidth());
                OZvm.getInstance().setDebugWinHeight(e.getComponent().getHeight());
            }
        });
        
        z80A.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("a "+z80A.getText());
            }
        });

        z80Ax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("a' "+z80Ax.getText());
            }
        });

        z80F.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("f "+z80F.getText());
            }
        });

        z80Fx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("f' "+z80Fx.getText());
            }
        });

        z80BC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("bc "+z80BC.getText());
            }
        });

        z80BCx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("bc' "+z80BCx.getText());
            }
        });

        z80DE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("de "+z80DE.getText());
            }
        });

        z80DEx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("de' "+z80DEx.getText());
            }
        });

        z80HL.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("hl "+z80HL.getText());
            }
        });

        z80HLx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("hl' "+z80HLx.getText());
            }
        });

        z80IX.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("ix "+z80IX.getText());
            }
        });

        z80IY.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("iy "+z80IY.getText());
            }
        });

        z80PC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("pc "+z80PC.getText());
            }
        });

        z80SP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("sp "+z80SP.getText());
            }
        });

        // remove bank number of PC, when editing PC register, and restore bank number when focus is lost
        z80PC.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                z80PC.setText( Dz.addrToHex(z80.PC(), false) );
            }

            public void focusLost(FocusEvent e) {
                z80PC.setText( Dz.byteToHex( bl.decodeLocalAddress(z80.PC()) >>> 16, false) + Dz.addrToHex(z80.PC(), false));
            }
        });

        blSR0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("sr0 "+blSR0.getText());
            }
        });

        blSR1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("sr1 "+blSR1.getText());
            }
        });

        blSR2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("sr2 "+blSR2.getText());
            }
        });

        blSR3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("sr3 "+blSR3.getText());
            }
        });

        blTIM0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("tim0 "+blTIM0.getText());
            }
        });

        blTIM1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("tim1 "+blTIM1.getText());
            }
        });

        blTIM2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("tim2 "+blTIM2.getText());
            }
        });

        blTIM3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("tim3 "+blTIM3.getText());
            }
        });

        blTIM4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                CommandLine.getInstance().parseCommandLine("tim4 "+blTIM4.getText());
            }
        });
        
    }

    public JConsole getConsole() {
        return console;
    }
            
    public void refreshZ88HardwareInfo() {
        // get content of current Z80 registers
        z80A.setText( Dz.byteToHex( z80.A(), false));
        z80F.setText( Dz.byteToHex( z80.F(), false));
        z80Flags.setText( Z88Info.z80Flags(z80.F()).toString() );
        z80Ax.setText( Dz.byteToHex( z80.AFx() >> 8, false));
        z80Fx.setText( Dz.byteToHex( z80.AFx() & 0xFF, false));
        z80BC.setText( Dz.addrToHex( z80.BC(), false));
        z80DE.setText( Dz.addrToHex( z80.DE(), false));
        z80HL.setText( Dz.addrToHex( z80.HL(), false));
        z80BCx.setText( Dz.addrToHex( z80.BCx(), false));
        z80DEx.setText( Dz.addrToHex( z80.DEx(), false));
        z80HLx.setText( Dz.addrToHex( z80.HLx(), false));
        z80IX.setText( Dz.addrToHex( z80.IX(), false));
        z80IY.setText( Dz.addrToHex( z80.IY(), false));
        z80SP.setText( Dz.addrToHex( z80.SP(), false));
        z80PC.setText( Dz.byteToHex( bl.decodeLocalAddress(z80.PC()) >>> 16, false) + Dz.addrToHex(z80.PC(), false));
        updateStackView();

        // get content of current segment bank binding registers
        blSR0.setText( Dz.byteToHex( bl.getSegmentBank(0), false));
        blSR1.setText( Dz.byteToHex( bl.getSegmentBank(1), false));
        blSR2.setText( Dz.byteToHex( bl.getSegmentBank(2), false));
        blSR3.setText( Dz.byteToHex( bl.getSegmentBank(3), false));

        // get content of real time clock registers
        blTIM0.setText( Dz.byteToHex( bl.getTim0(), false));
        blTIM1.setText( Dz.byteToHex( bl.getTim1(), false));
        blTIM2.setText( Dz.byteToHex( bl.getTim2(), false));
        blTIM3.setText( Dz.byteToHex( bl.getTim3(), false));
        blTIM4.setText( Dz.byteToHex( bl.getTim4(), false));

        blINT.setText( Dz.byteToBin(bl.getInt(), false));
        blSTA.setText( Dz.byteToBin(bl.getSta(), false));
        blTSTA.setText( Dz.byteToBin(bl.getTsta(), false));
    }

    /**
     * Disable/enable Z88 Machine Panel input fields
     */
    public void lockZ88MachinePanel(boolean lockState) {
        z80A.setEnabled(lockState);
        z80F.setEnabled(lockState);
        z80Flags.setEnabled(false); // flags is always read-only, changed via command line
        z80Ax.setEnabled(lockState);
        z80Fx.setEnabled(lockState);
        z80BC.setEnabled(lockState);
        z80DE.setEnabled(lockState);
        z80HL.setEnabled(lockState);
        z80BCx.setEnabled(lockState);
        z80DEx.setEnabled(lockState);
        z80HLx.setEnabled(lockState);
        z80IX.setEnabled(lockState);
        z80IY.setEnabled(lockState);
        z80SP.setEnabled(lockState);
        z80PC.setEnabled(lockState);
        stackView.setEnabled(lockState);

        blSR0.setEnabled(lockState);
        blSR1.setEnabled(lockState);
        blSR2.setEnabled(lockState);
        blSR3.setEnabled(lockState);

        blTIM0.setEnabled(lockState);
        blTIM1.setEnabled(lockState);
        blTIM2.setEnabled(lockState);
        blTIM3.setEnabled(lockState);
        blTIM4.setEnabled(lockState);

        blINT.setEnabled(false);
        blSTA.setEnabled(false);
        blTSTA.setEnabled(false);
    }

    /**
     * Fill the stack view with current Stack memory, placing the current SP
     * in the middle
     */
    private void updateStackView() {
        int stackTopAddress = z80.SP() + 64; // Top of Stack is 32 addresses higher
        for (int stackItemIdx=0; stackItemIdx<stackViewRows; stackItemIdx++) {
            stackView.setValueAt(Dz.addrToHex(stackTopAddress, false), stackItemIdx, 0); // SP address
            stackView.setValueAt(Dz.addrToHex(z80.readWord(stackTopAddress), false), stackItemIdx, 1); // contents of SP address

            if (stackTopAddress == z80.SP()) {
                stackView.setRowSelectionInterval(stackItemIdx,stackItemIdx);
            }

            stackTopAddress -= 2;
        }
    }

    public JTable getZ80StackView() {
        if (stackView == null) {
            String[] columns = new String[] {"SP", "(SP)"};
            //create table with data
            stackView = new JTable(stackData, columns);
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment( SwingConstants.CENTER );
            stackView.getColumnModel().getColumn(0).setHeaderRenderer( centerRenderer );
            stackView.getColumnModel().getColumn(1).setHeaderRenderer( centerRenderer );
            stackView.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
            stackView.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
        }

        return stackView;
    }

    /**
     * This method initializes the Z88 Machine Panel
     *
     * @return javax.swing.JPanel
     */
    public javax.swing.JPanel getZ88MachinePanel() {
        JLabel l;

        if (jZ88HardwareView == null) {
            jZ88HardwareView = new JPanel(new GridBagLayout());
            JLabel lHdr = new JLabel("Z80 CPU Registers", JLabel.CENTER);
            Font defaultFont = lHdr.getFont();
            lHdr.setFont(new Font(defaultFont.getName(), Font.BOLD, 14));

            GridBagConstraints cHdr = new GridBagConstraints();
            cHdr.gridx = 0; cHdr.gridy = 0; cHdr.anchor = GridBagConstraints.PAGE_START; cHdr.gridwidth = 6; cHdr.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(lHdr,cHdr);

            getZ80StackView().getTableHeader().setFont(new Font(defaultFont.getName(), Font.BOLD, 14));
            getZ80StackView().setFont(new Font(defaultFont.getName(), Font.PLAIN, 13));

            GridBagConstraints cStack = new GridBagConstraints();
            cStack.gridx = 3; cStack.gridy = 1; cStack.gridwidth = 3; cStack.insets = new Insets(3, 3, 0, 0); cStack.weightx = 1.1; cStack.gridheight = 14;cStack.fill = GridBagConstraints.BOTH;
            jZ88HardwareView.add(new JScrollPane(getZ80StackView()), cStack);

            GridBagConstraints clAf = new GridBagConstraints();
            clAf.gridx = 0; clAf.gridy = 1; clAf.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("AF", JLabel.TRAILING), clAf);

            GridBagConstraints cA = new GridBagConstraints();
            cA.gridx = 1; cA.gridy = 1; cA.weightx = 0.7; cA.fill = GridBagConstraints.HORIZONTAL;
            z80A = new JTextField(2); z80A.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80A,cA);

            GridBagConstraints cF = new GridBagConstraints();
            cF.gridx = 2; cF.gridy = 1; cF.weightx = 0.7; cF.fill = GridBagConstraints.HORIZONTAL;
            z80F = new JTextField(2); z80F.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80F,cF);

            GridBagConstraints clAfx = new GridBagConstraints();
            clAfx.gridx = 0; clAfx.gridy = 2; clAfx.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("AF'", JLabel.TRAILING), clAfx);

            GridBagConstraints cAx = new GridBagConstraints();
            cAx.gridx = 1; cAx.gridy = 2; cAx.weightx = 0.7; cAx.fill = GridBagConstraints.HORIZONTAL;
            z80Ax = new JTextField(2); z80Ax.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80Ax,cAx);

            GridBagConstraints cFx = new GridBagConstraints();
            cFx.gridx = 2; cFx.gridy = 2; cFx.weightx = 0.7; cFx.fill = GridBagConstraints.HORIZONTAL;
            z80Fx = new JTextField(2); z80Fx.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80Fx,cFx);

            GridBagConstraints clFlags = new GridBagConstraints();
            clFlags.gridx = 0; clFlags.gridy = 3; clFlags.weightx = 0.0; clFlags.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("Flags", JLabel.TRAILING),clFlags);

            GridBagConstraints cFlags = new GridBagConstraints();
            cFlags.gridx = 1; cFlags.gridy = 3; cFlags.weightx = 0.0; cFlags.gridwidth = 2; cFlags.fill = GridBagConstraints.HORIZONTAL;
            z80Flags = new JTextField(4); z80Flags.setHorizontalAlignment(JTextField.CENTER);
            z80Flags.setFont(new Font("Monospaced", Font.PLAIN, 13));
            jZ88HardwareView.add(z80Flags, cFlags);

            GridBagConstraints clPC = new GridBagConstraints();
            clPC.gridx = 0; clPC.gridy = 4; clPC.weightx = 0.0; clPC.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("PC", JLabel.TRAILING),clPC);

            GridBagConstraints cPC = new GridBagConstraints();
            cPC.gridx = 1; cPC.gridy = 4; cPC.weightx = 0.0; cPC.gridwidth = 2; cPC.fill = GridBagConstraints.HORIZONTAL;
            z80PC = new JTextField(4); z80PC.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80PC, cPC);

            GridBagConstraints clSP = new GridBagConstraints();
            clSP.gridx = 0; clSP.gridy = 5; clSP.weightx = 0.0; clSP.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("SP", JLabel.TRAILING),clSP);

            GridBagConstraints cSP = new GridBagConstraints();
            cSP.gridx = 1; cSP.gridy = 5; cSP.weightx = 0.0; cSP.gridwidth = 2; cSP.fill = GridBagConstraints.HORIZONTAL;
            z80SP = new JTextField(4); z80SP.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80SP, cSP);

            GridBagConstraints clBC = new GridBagConstraints();
            clBC.gridx = 0; clBC.gridy = 6; clBC.weightx = 0.0; clBC.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("BC", JLabel.TRAILING),clBC);

            GridBagConstraints cBC = new GridBagConstraints();
            cBC.gridx = 1; cBC.gridy = 6; cBC.weightx = 0.0; cBC.gridwidth = 2; cBC.fill = GridBagConstraints.HORIZONTAL;
            z80BC = new JTextField(4); z80BC.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80BC, cBC);

            GridBagConstraints clDE = new GridBagConstraints();
            clDE.gridx = 0; clDE.gridy = 7; clDE.weightx = 0.0; clDE.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("DE", JLabel.TRAILING),clDE);

            GridBagConstraints cDE = new GridBagConstraints();
            cDE.gridx = 1; cDE.gridy = 7; cDE.weightx = 0.0; cDE.gridwidth = 2; cDE.fill = GridBagConstraints.HORIZONTAL;
            z80DE = new JTextField(4); z80DE.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80DE, cDE);

            GridBagConstraints clHL = new GridBagConstraints();
            clHL.gridx = 0; clHL.gridy = 8; clHL.weightx = 0.0; clHL.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("HL", JLabel.TRAILING),clHL);

            GridBagConstraints cHL = new GridBagConstraints();
            cHL.gridx = 1; cHL.gridy = 8; cHL.weightx = 0.0; cHL.gridwidth = 2; cHL.fill = GridBagConstraints.HORIZONTAL;
            z80HL = new JTextField(4); z80HL.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80HL, cHL);

            GridBagConstraints clIX = new GridBagConstraints();
            clIX.gridx = 0; clIX.gridy = 9; clIX.weightx = 0.0; clIX.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("IX", JLabel.TRAILING),clIX);

            GridBagConstraints cIX = new GridBagConstraints();
            cIX.gridx = 1; cIX.gridy = 9; cIX.weightx = 0.0; cIX.gridwidth = 2; cIX.fill = GridBagConstraints.HORIZONTAL;
            z80IX = new JTextField(4); z80IX.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80IX, cIX);

            GridBagConstraints clIY = new GridBagConstraints();
            clIY.gridx = 0; clIY.gridy = 10; clIY.weightx = 0.0; clIY.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("IY", JLabel.TRAILING),clIY);

            GridBagConstraints cIY = new GridBagConstraints();
            cIY.gridx = 1; cIY.gridy = 10; cIY.weightx = 0.0; cIY.gridwidth = 2; cIY.fill = GridBagConstraints.HORIZONTAL;
            z80IY = new JTextField(4); z80IY.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80IY, cIY);

            GridBagConstraints clBCx = new GridBagConstraints();
            clBCx.gridx = 0; clBCx.gridy = 11; clBCx.weightx = 0.0; clBCx.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("BC'", JLabel.TRAILING),clBCx);

            GridBagConstraints cBCx = new GridBagConstraints();
            cBCx.gridx = 1; cBCx.gridy = 11; cBCx.weightx = 0.0; cBCx.gridwidth = 2; cBCx.fill = GridBagConstraints.HORIZONTAL;
            z80BCx = new JTextField(4); z80BCx.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80BCx, cBCx);

            GridBagConstraints clDEx = new GridBagConstraints();
            clDEx.gridx = 0; clDEx.gridy = 12; clDEx.weightx = 0.0; clDEx.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("DE'", JLabel.TRAILING),clDEx);

            GridBagConstraints cDEx = new GridBagConstraints();
            cDEx.gridx = 1; cDEx.gridy = 12; cDEx.weightx = 0.0; cDEx.gridwidth = 2; cDEx.fill = GridBagConstraints.HORIZONTAL;
            z80DEx = new JTextField(4); z80DEx.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80DEx, cDEx);

            GridBagConstraints clHLx = new GridBagConstraints();
            clHLx.gridx = 0; clHLx.gridy = 13; clHLx.weightx = 0.0; clHLx.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("HL'", JLabel.TRAILING),clHLx);

            GridBagConstraints cHLx = new GridBagConstraints();
            cHLx.gridx = 1; cHLx.gridy = 13; cHLx.weightx = 0.0; cHLx.gridwidth = 2; cHLx.fill = GridBagConstraints.HORIZONTAL;
            z80HLx = new JTextField(4); z80HLx.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(z80HLx, cHLx);

            JLabel lHdr2 = new JLabel("Z88 Blink Registers", JLabel.CENTER);
            lHdr2.setFont(new Font(defaultFont.getName(), Font.BOLD, 14));

            GridBagConstraints cHdr2 = new GridBagConstraints();
            cHdr2.gridx = 0; cHdr2.gridy = 15; cHdr2.anchor = GridBagConstraints.PAGE_START; cHdr2.gridwidth = 6; cHdr2.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(lHdr2,cHdr2);

            GridBagConstraints clSR = new GridBagConstraints();
            clSR.gridx = 0; clSR.gridy = 16; clSR.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel(" SR0-3", JLabel.TRAILING), clSR);

            GridBagConstraints cBlSr0 = new GridBagConstraints();
            cBlSr0.gridx = 1; cBlSr0.gridy = 16; cBlSr0.weightx = 0.5; cBlSr0.gridwidth = 1; cBlSr0.fill = GridBagConstraints.HORIZONTAL;
            //cBlSr0.insets = new Insets(3, 3, 0, 0);
            blSR0 = new JTextField(2); blSR0.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blSR0,cBlSr0);

            GridBagConstraints cBlSr1 = new GridBagConstraints();
            cBlSr1.gridx = 2; cBlSr1.gridy = 16; cBlSr1.weightx = 0.5; cBlSr1.gridwidth = 1; cBlSr1.fill = GridBagConstraints.HORIZONTAL;
            //cBlSr1.insets = new Insets(3, 3, 0, 0);
            blSR1 = new JTextField(2); blSR1.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blSR1,cBlSr1);

            GridBagConstraints cBlSr2 = new GridBagConstraints();
            cBlSr2.gridx = 3; cBlSr2.gridy = 16; cBlSr2.weightx = 0.6; cBlSr2.gridwidth = 1; cBlSr2.fill = GridBagConstraints.HORIZONTAL;
            //cBlSr2.insets = new Insets(3, 3, 0, 0);
            blSR2 = new JTextField(2); blSR2.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blSR2,cBlSr2);

            GridBagConstraints cBlSr3 = new GridBagConstraints();
            cBlSr3.gridx = 4; cBlSr3.gridy = 16; cBlSr3.weightx = 0.6; cBlSr3.gridwidth = 1; cBlSr3.fill = GridBagConstraints.HORIZONTAL;
            //cBlSr3.insets = new Insets(0, 0, 0, 16);
            blSR3 = new JTextField(2); blSR3.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blSR3,cBlSr3);

            GridBagConstraints clTIM = new GridBagConstraints();
            clTIM.gridx = 0; clTIM.gridy = 17; clTIM.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("TIM0-4", JLabel.TRAILING), clTIM);

            GridBagConstraints cBlTim0 = new GridBagConstraints();
            cBlTim0.gridx = 1; cBlTim0.gridy = 17; cBlTim0.weightx = 0.5; cBlTim0.gridwidth = 1; cBlTim0.fill = GridBagConstraints.HORIZONTAL;
            blTIM0 = new JTextField(2); blTIM0.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTIM0,cBlTim0);

            GridBagConstraints cBlTim1 = new GridBagConstraints();
            cBlTim1.gridx = 2; cBlTim1.gridy = 17; cBlTim1.weightx = 0.5; cBlTim1.gridwidth = 1; cBlTim1.fill = GridBagConstraints.HORIZONTAL;
            blTIM1 = new JTextField(2); blTIM1.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTIM1,cBlTim1);

            GridBagConstraints cBlTim2 = new GridBagConstraints();
            cBlTim2.gridx = 3; cBlTim2.gridy = 17; cBlTim2.weightx = 0.6; cBlTim2.gridwidth = 1; cBlTim2.fill = GridBagConstraints.HORIZONTAL;
            blTIM2 = new JTextField(2); blTIM2.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTIM2,cBlTim2);

            GridBagConstraints cBlTim3 = new GridBagConstraints();
            cBlTim3.gridx = 4; cBlTim3.gridy = 17; cBlTim3.weightx = 0.6; cBlTim3.gridwidth = 1; cBlTim3.fill = GridBagConstraints.HORIZONTAL;
            blTIM3 = new JTextField(2); blTIM3.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTIM3,cBlTim3);

            GridBagConstraints cBlTim4 = new GridBagConstraints();
            cBlTim4.gridx = 5; cBlTim4.gridy = 17; cBlTim4.weightx = 0.6; cBlTim4.gridwidth = 1; cBlTim4.fill = GridBagConstraints.HORIZONTAL;
            blTIM4 = new JTextField(2); blTIM4.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTIM4,cBlTim4);

            GridBagConstraints clINT = new GridBagConstraints();
            clINT.gridx = 0; clINT.gridy = 18; clINT.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("INT", JLabel.TRAILING), clINT);

            GridBagConstraints cBlInt = new GridBagConstraints();
            cBlInt.gridx = 1; cBlInt.gridy = 18; cBlInt.weightx = 0.6; cBlInt.gridwidth = 5; cBlInt.fill = GridBagConstraints.HORIZONTAL;
            blINT = new JTextField(2); blINT.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blINT,cBlInt);

            GridBagConstraints clSTA = new GridBagConstraints();
            clSTA.gridx = 0; clSTA.gridy = 19; clSTA.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("STA", JLabel.TRAILING), clSTA);

            GridBagConstraints cBlSta = new GridBagConstraints();
            cBlSta.gridx = 1; cBlSta.gridy = 19; cBlSta.weightx = 0.6; cBlSta.gridwidth = 5; cBlSta.fill = GridBagConstraints.HORIZONTAL;
            blSTA = new JTextField(2); blSTA.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blSTA,cBlSta);

            GridBagConstraints clTSTA = new GridBagConstraints();
            clTSTA.gridx = 0; clTSTA.gridy = 20; clTSTA.fill = GridBagConstraints.HORIZONTAL;
            jZ88HardwareView.add(new JLabel("TSTA", JLabel.TRAILING), clTSTA);

            GridBagConstraints cBlTsta = new GridBagConstraints();
            cBlTsta.gridx = 1; cBlTsta.gridy = 20; cBlTsta.weightx = 0.6; cBlTsta.gridwidth = 5; cBlTsta.fill = GridBagConstraints.HORIZONTAL;
            blTSTA = new JTextField(2); blTSTA.setHorizontalAlignment(JTextField.CENTER);
            jZ88HardwareView.add(blTSTA,cBlTsta);

            jZ88HardwareView.setPreferredSize(new Dimension(220, 400));
            jZ88HardwareView.setFont(new java.awt.Font("Monospaced", java.awt.Font.PLAIN, 11));
        }
        return jZ88HardwareView;
    }


    public void clearConsole() {
        console.clsConsole();
    }

    public void consoleOutput(String msg) {
        console.println(msg);
    }

    public void activateDebugCommandLine() {
        toFront();
        console.grabFocus();
        lockZ88MachinePanel( Z88.getInstance().getProcessorThread() == null );
    }

    public void disableConsoleInput() {
        console.setEnabled(false);
    }

    public void enableConsoleInput() {
        console.setEnabled(true);
    }
    
}
