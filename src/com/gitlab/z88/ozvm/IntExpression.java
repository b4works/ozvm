/*
 * IntExpression.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import java.util.Stack;

/**
 * Simple 32bit integer expression evaluation.
 *
 * Original implementation is copied from
 * https://www.geeksforgeeks.org/expression-evaluation/
 * which is the Shunting Yard Algorithm by Edgar Dijkstra.
 *
 * Improved for OZvm by handling unary minus, adding various new operators, 
 * hex+binary constants and identifying register Z80 mnemonics as constant 
 * specifiers in expressions.
 *
 * Operators accepted:
 *      + - * % / & | ^ < >
 * Z80 register aware:
 *      a, f, a', f', 
 *      b, c, d, e, h, l, b', c', d', e', h', l',
 *      ixh, ixl, iyh, iyl,
 *      af, af', bc, de, hl, bc', de', hl', 
 *      ix, iy, sp, pc,
 *      i, r
 *
 * Usage, for example:
 *      EvaluateString.evaluate("$ff")         // just a hexadecimal constant
 *      EvaluateString.evaluate("-10 + 2 * 6") // unary minus and simple expression
 *      EvaluateString.evaluate("@111 ^ @001") // binary constants and XOR operator
 *      EvaluateString.evaluate("hl+bc")       // adding two Z80 registers
 *      EvaluateString.evaluate("hl+bc+@111")  // expression with binary constant
 *      EvaluateString.evaluate("ix-77")       // get address of IX negative offset
 */
public class IntExpression
{
    /**
     * Return integer result of evaluated expression in string
     * In case of error, an exception is thrown
     *
     * @param expression
     * @return if an error occurs during parsing, return various -99999999x integers
     */
    public static int evaluate(String expression)
    {
        StringBuffer sbuf;
        Z80Processor z80 = Z88.getInstance().getProcessor();
        
        /* Pre-process expression for space and unary minus */
        expression = expression.replace(" ", "").replace("(-", "(0-");
        if (expression.charAt(0) == '-') {
            expression = "0" + expression;
        }        
        
        char[] tokens = expression.toCharArray();

         // Stack for numbers: 'values'
        Stack<Integer> values = new Stack<Integer>();

        // Stack for Operators: 'ops'
        Stack<Character> ops = new Stack<Character>();

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i] == '$') {
                i++; // point at first hexadecimal digit
                // Current token is a hexa decimal number, push it to stack for numbers
                sbuf = new StringBuffer();
                // There may be more than one hex digit in number
                while (i < tokens.length && Character.digit(tokens[i],16) >= 0)
                    sbuf.append(tokens[i++]);
                values.push(Integer.parseInt(sbuf.toString(),16));

                // constant pushed on value stack, adjust for i to point on first char
                // after constant after loop increment
                --i;
            }

            else if (tokens[i] == '@') {
                i++; // point at first binary digit
                // Current token is a hexa decimal number, push it to stack for numbers
                sbuf = new StringBuffer();
                // There may be more than one hex digit in number
                while (i < tokens.length && Character.digit(tokens[i],2) >= 0)
                    sbuf.append(tokens[i++]);
                values.push(Integer.parseInt(sbuf.toString(),2));

                // constant pushed on value stack, adjust for i to point on first char
                // after constant after loop increment
                --i;
            }

            else if (Character.isDigit(tokens[i])) {
                // Current token is a decimal number, push it to stack for numbers
                sbuf = new StringBuffer();
                // There may be more than one digits in number
                while (i < tokens.length && Character.isDigit(tokens[i]))
                    sbuf.append(tokens[i++]);
                values.push(Integer.parseInt(sbuf.toString(),10));

                // constant pushed on value stack, adjust for i to point on first char
                // after constant when loop increment
                --i;
            }

            else if (Character.isLetter(tokens[i])) {
                // current token is a Register mnemonic
                sbuf = new StringBuffer();
                // There may be more than one digits in number
                while (i < tokens.length && (Character.isLetter(tokens[i]) || tokens[i] == '\''))
                    sbuf.append(tokens[i++]);

                if ( sbuf.toString().toLowerCase() == "af" )
                    values.push(z80.AF());
                else if ( sbuf.toString().toLowerCase() == "af'" )
                    values.push(z80.AFx());
                else if ( sbuf.toString().toLowerCase() == "bc" )
                    values.push(z80.BC());
                else if ( sbuf.toString().toLowerCase() == "bc'" )
                    values.push(z80.BCx());
                else if ( sbuf.toString().toLowerCase() == "de" )
                    values.push(z80.DE());
                else if ( sbuf.toString().toLowerCase() == "de'" )
                    values.push(z80.DEx());
                else if ( sbuf.toString().toLowerCase() == "hl" )
                    values.push(z80.HL());
                else if ( sbuf.toString().toLowerCase() == "hl'" )
                    values.push(z80.HLx());
                else if ( sbuf.toString().toLowerCase() == "ix" )
                    values.push(z80.IX());
                else if ( sbuf.toString().toLowerCase() == "iy" )
                    values.push(z80.IY());
                else if ( sbuf.toString().toLowerCase() == "sp" )
                    values.push(z80.SP());
                else if ( sbuf.toString().toLowerCase() == "pc" )
                    values.push(z80.PC());
                else if ( sbuf.toString().toLowerCase() == "a" )
                    values.push(z80.A());
                else if ( sbuf.toString().toLowerCase() == "a'" )
                    values.push(z80.Ax());
                else if ( sbuf.toString().toLowerCase() == "f" )
                    values.push(z80.F());
                else if ( sbuf.toString().toLowerCase() == "f'" )
                    values.push(z80.Fx());
                else if ( sbuf.toString().toLowerCase() == "b" )
                    values.push(z80.B());
                else if ( sbuf.toString().toLowerCase() == "b'" )
                    values.push(z80.Bx());
                else if ( sbuf.toString().toLowerCase() == "c" )
                    values.push(z80.C());
                else if ( sbuf.toString().toLowerCase() == "c'" )
                    values.push(z80.Cx());
                else if ( sbuf.toString().toLowerCase() == "d" )
                    values.push(z80.D());
                else if ( sbuf.toString().toLowerCase() == "d'" )
                    values.push(z80.Dx());
                else if ( sbuf.toString().toLowerCase() == "e" )
                    values.push(z80.E());
                else if ( sbuf.toString().toLowerCase() == "e'" )
                    values.push(z80.Ex());
                else if ( sbuf.toString().toLowerCase() == "h" )
                    values.push(z80.H());
                else if ( sbuf.toString().toLowerCase() == "h'" )
                    values.push(z80.Hx());
                else if ( sbuf.toString().toLowerCase() == "l" )
                    values.push(z80.L());
                else if ( sbuf.toString().toLowerCase() == "l'" )
                    values.push(z80.Lx());
                else if ( sbuf.toString().toLowerCase() == "ixh" )
                    values.push(z80.IXH());
                else if ( sbuf.toString().toLowerCase() == "ixl" )
                    values.push(z80.IXL());
                else if ( sbuf.toString().toLowerCase() == "iyh" )
                    values.push(z80.IYH());
                else if ( sbuf.toString().toLowerCase() == "iyl" )
                    values.push(z80.IYL());
                else if ( sbuf.toString().toLowerCase() == "i" )
                    values.push(z80.I());
                else if ( sbuf.toString().toLowerCase() == "r" )
                    values.push(z80.R());
                else 
                    throw new UnsupportedOperationException("Unknown register");

                // constant pushed on value stack, adjust for i to point on first char
                // after constant when loop increment
                --i;
            }

            // Current token is an opening brace, push it to 'ops'
            else if (tokens[i] == '(')
                ops.push(tokens[i]);

            // Closing brace encountered, solve entire brace
            else if (tokens[i] == ')') {
                while (ops.peek() != '(')
                  values.push(applyOp(ops.pop(), values.pop(), values.pop()));
                ops.pop();
            }

            // Current token is an operator.
            else if (
                     tokens[i] == '*' || tokens[i] == '/' || tokens[i] == '%' ||
                     tokens[i] == '+' || tokens[i] == '-' ||
                     tokens[i] == '<' || tokens[i] == '>' ||
                     tokens[i] == '&' || tokens[i] == '|' || tokens[i] == '^'
                    )
            {
                // While top of 'ops' has same or greater precedence to current
                // token, which is an operator. Apply operator on top of 'ops'
                // to top two elements in values stack
                while (!ops.empty() && hasPrecedence(tokens[i], ops.peek()))
                  values.push(applyOp(ops.pop(), values.pop(), values.pop()));

                // Push current token to 'ops'.
                ops.push(tokens[i]);
            } else {
                // syntax error, non-accepted token
                throw new UnsupportedOperationException("Syntax error");
            }
        }

        // Entire expression has been parsed at this point, apply remaining
        // ops to remaining values
        while (!ops.empty())
            values.push(applyOp(ops.pop(), values.pop(), values.pop()));

        // Top of 'values' contains result, return it
        return values.pop();
    }

    // Returns true if 'op2' has higher or same precedence as 'op1',
    // otherwise returns false.
    private static boolean hasPrecedence(char op1, char op2)
    {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/' || op1 == '%') && (op2 == '+' || op2 == '-'))
            return false;
        if ((op1 == '*' || op1 == '/' || op1 == '%') && (op2 == '<' || op2 == '>'))
            return false;
        if ((op1 == '*' || op1 == '/' || op1 == '%') && (op2 == '&' || op2 == '|' || op2 == '^'))
            return false;
        if ((op1 == '+' || op1 == '-') && (op2 == '&' || op2 == '|' || op2 == '^'))
            return false;
        if ((op1 == '<' || op1 == '>') && (op2 == '&' || op2 == '|' || op2 == '^'))
            return false;

        return true;
    }

    // A utility method to apply an operator 'op' on operands 'a'
    // and 'b'. Return the result.
    private static int applyOp(char op, int b, int a)
    {
        switch (op)
        {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '>':
            return a >> b;
        case '<':
            return a << b;
        case '&':
            return a & b;
        case '|':
            return a | b;
        case '^':
            return a ^ b;
        case '*':
            return a * b;
        case '%':
            if (b != 0)
                return a % b;
            else
                throw new UnsupportedOperationException("Cannot divide by zero");
        case '/':
            if (b != 0)
                return a / b;
            else
                throw new UnsupportedOperationException("Cannot divide by zero");
        }

        return 0;
    }
}


