/*
 * SaveRestoreVM.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import com.gitlab.z88.ozvm.datastructures.SlotInfo;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.swing.filechooser.FileFilter;

/**
 * Management of saving and resuming a OZvm machine instance. The machine state
 * is saved in a Zip file containing a combination of a properties file, and a
 * memory dump of current slot contents, saved as slotX.ram or slotX.epr,
 * depending of the type.
 */
public class SaveRestoreVM {

    private final Blink blink;
    private final Z80Processor z80;
    private final Memory memory;

    /**
     * Constructor
     */
    public SaveRestoreVM() {
        z80 = Z88.getInstance().getProcessor();
        memory = Z88.getInstance().getMemory();
        blink = Z88.getInstance().getBlink();
    }

    /**
     * Preserve state of the Z80 CPU registers.
     */
    private void storeZ80Regs(Properties properties) {
        properties.setProperty("AF", Dz.addrToHex(z80.AF(), false));
        properties.setProperty("BC", Dz.addrToHex(z80.BC(), false));
        properties.setProperty("DE", Dz.addrToHex(z80.DE(), false));
        properties.setProperty("HL", Dz.addrToHex(z80.HL(), false));
        properties.setProperty("IX", Dz.addrToHex(z80.IX(), false));
        properties.setProperty("IY", Dz.addrToHex(z80.IY(), false));
        properties.setProperty("PC", Dz.addrToHex(z80.PC(), false));
        properties.setProperty("SP", Dz.addrToHex(z80.SP(), false));
        z80.ex_af_af();
        properties.setProperty("_AF", Dz.addrToHex(z80.AF(), false));
        z80.ex_af_af();
        z80.exx();
        properties.setProperty("_BC", Dz.addrToHex(z80.BC(), false));
        properties.setProperty("_DE", Dz.addrToHex(z80.DE(), false));
        properties.setProperty("_HL", Dz.addrToHex(z80.HL(), false));
        z80.exx();
        properties.setProperty("I", Dz.byteToHex(z80.I(), false));
        properties.setProperty("R", Dz.byteToHex(z80.R(), false));
        properties.setProperty("IM", Dz.byteToHex(z80.IM(), false));
        properties.setProperty("IFF1", Boolean.toString(z80.IFF1()));
        properties.setProperty("IFF2", Boolean.toString(z80.IFF2()));
    }

    /**
     * Restore register values into the Z80 engine.
     */
    private void loadZ80Regs(Properties properties) {
        z80.AF(Integer.parseInt(properties.getProperty("AF"), 16));
        z80.BC(Integer.parseInt(properties.getProperty("BC"), 16));
        z80.DE(Integer.parseInt(properties.getProperty("DE"), 16));
        z80.HL(Integer.parseInt(properties.getProperty("HL"), 16));
        z80.IX(Integer.parseInt(properties.getProperty("IX"), 16));
        z80.IY(Integer.parseInt(properties.getProperty("IY"), 16));
        z80.PC(Integer.parseInt(properties.getProperty("PC"), 16));
        z80.SP(Integer.parseInt(properties.getProperty("SP"), 16));
        z80.ex_af_af();
        z80.AF(Integer.parseInt(properties.getProperty("_AF"), 16));
        z80.ex_af_af();
        z80.exx();
        z80.BC(Integer.parseInt(properties.getProperty("_BC"), 16));
        z80.DE(Integer.parseInt(properties.getProperty("_DE"), 16));
        z80.HL(Integer.parseInt(properties.getProperty("_HL"), 16));
        z80.exx();
        z80.I(Integer.parseInt(properties.getProperty("I"), 16));
        z80.R(Integer.parseInt(properties.getProperty("R"), 16));
        z80.IM(Integer.parseInt(properties.getProperty("IM"), 16));
        z80.IFF1(Boolean.valueOf(properties.getProperty("IFF1")).booleanValue());
        z80.IFF2(Boolean.valueOf(properties.getProperty("IFF2")).booleanValue());
    }

    /**
     * Restore register values into the Blink hardware.
     */
    private void storeBlinkRegs(Properties properties) {
        properties.setProperty("PB0", Dz.addrToHex(blink.getPb0(), false));
        properties.setProperty("PB1", Dz.addrToHex(blink.getPb1(), false));
        properties.setProperty("PB2", Dz.addrToHex(blink.getPb2(), false));
        properties.setProperty("PB3", Dz.addrToHex(blink.getPb3(), false));
        properties.setProperty("SBR", Dz.addrToHex(blink.getSbr(), false));

        properties.setProperty("SCW", Dz.addrToHex(blink.getSCW(), false)); // New: LCD Width in pixels / 8
        properties.setProperty("SCH", Dz.addrToHex(blink.getSCH(), false)); // New: LCD Height in pixels / 8

        properties.setProperty("COM", Dz.byteToHex(blink.getCom(), false));
        properties.setProperty("INT", Dz.byteToHex(blink.getInt(), false));
        properties.setProperty("STA", Dz.byteToHex(blink.getSta(), false));

        // KBD,ACK,TACK is not preserved (not needed)
        // EPR not yet implemented

        properties.setProperty("TMK", Dz.byteToHex(blink.getTmk(), false));
        properties.setProperty("TSTA", Dz.byteToHex(blink.getTsta(), false));

        properties.setProperty("SR0", Dz.byteToHex(blink.getSegmentBank(0), false));
        properties.setProperty("SR1", Dz.byteToHex(blink.getSegmentBank(1), false));
        properties.setProperty("SR2", Dz.byteToHex(blink.getSegmentBank(2), false));
        properties.setProperty("SR3", Dz.byteToHex(blink.getSegmentBank(3), false));

        properties.setProperty("TIM0", Dz.byteToHex(blink.getTim0(), false));
        properties.setProperty("TIM1", Dz.byteToHex(blink.getTim1(), false));
        properties.setProperty("TIM2", Dz.byteToHex(blink.getTim2(), false));
        properties.setProperty("TIM3", Dz.byteToHex(blink.getTim3(), false));
        properties.setProperty("TIM4", Dz.byteToHex(blink.getTim4(), false));

        // UART Registers not yet implemented
    }

    /**
     * Restore state of Blink Hardware Registers.
     */
    private void loadBlinkRegs(Properties properties) {
        blink.setPb0(Integer.parseInt(properties.getProperty("PB0"), 16));
        blink.setPb1(Integer.parseInt(properties.getProperty("PB1"), 16));
        blink.setPb2(Integer.parseInt(properties.getProperty("PB2"), 16));
        blink.setPb3(Integer.parseInt(properties.getProperty("PB3"), 16));
        blink.setSbr(Integer.parseInt(properties.getProperty("SBR"), 16));

        if (properties.getProperty("SCW") != null) {
            blink.setSCW(Integer.parseInt(properties.getProperty("SCW"), 16)); // New: LCD Width in pixels / 8
        } else {
            blink.setSCW(640/8); // preset default Z88 LCD Width
        }
        if (properties.getProperty("SCH") != null) {
            blink.setSCH(Integer.parseInt(properties.getProperty("SCH"), 16)); // New: LCD Height in pixels / 8
        } else {
            blink.setSCH(64/8); // preset default Z88 LCD Height
        }

        blink.setCom(Integer.parseInt(properties.getProperty("COM"), 16));
        blink.setInt(Integer.parseInt(properties.getProperty("INT"), 16));
        blink.setSta(Integer.parseInt(properties.getProperty("STA"), 16));

        // KBD,ACK,TACK is not preserved (not needed)
        // EPR not yet implemented

        blink.setTmk(Integer.parseInt(properties.getProperty("TMK"), 16));
        blink.setTsta(Integer.parseInt(properties.getProperty("TSTA"), 16));

        blink.setSegmentBank(0, Integer.parseInt(properties.getProperty("SR0"), 16));
        blink.setSegmentBank(1, Integer.parseInt(properties.getProperty("SR1"), 16));
        blink.setSegmentBank(2, Integer.parseInt(properties.getProperty("SR2"), 16));
        blink.setSegmentBank(3, Integer.parseInt(properties.getProperty("SR3"), 16));

        blink.setTim0(Integer.parseInt(properties.getProperty("TIM0"), 16));
        blink.setTim1(Integer.parseInt(properties.getProperty("TIM1"), 16));
        blink.setTim2(Integer.parseInt(properties.getProperty("TIM2"), 16));
        blink.setTim3(Integer.parseInt(properties.getProperty("TIM3"), 16));
        blink.setTim4(Integer.parseInt(properties.getProperty("TIM4"), 16));
        
        // adjust elapsed time
        blink.adjustLostTime();
                
        // UART Registers not yet implemented
    }

    /**
     * Fetch the 'Breakpoints' entry from the properties collection, which is a
     * comma separated list of breakpoint adresses and install them into the
     * breakpoint container (part of Blink).
     *
     * @param properties
     */
    private void loadBreakpoints(Properties properties) {
        Breakpoints bp = z80.getBreakpoints();

        // remove current breakpoints before loading a new set
        // from the snapshot (the old breakpoints doesn't theoretically
        // match the memory of another snapshot)
        bp.removeBreakPoints();

        String breakpoints = properties.getProperty("Breakpoints");
        if (breakpoints == null) {
            return;
        }

        String[] breakpointList = breakpoints.split(",");
        for (int l = 0; l < breakpointList.length; l++) {
            String breakpointAscii = breakpointList[l];

            if (breakpointAscii != null) {
                if (breakpointAscii.length() > 0) {
                    if (breakpointAscii.startsWith("[d]") == true) {
                        // remove display breakpoint indicator
                        breakpointAscii = breakpointAscii.substring(3);
                        bp.toggleBreakpoint(Integer.parseInt(breakpointAscii, 16), false);
                    } else {
                        bp.toggleBreakpoint(Integer.parseInt(breakpointAscii, 16));
                    }
                }
            }
        }
    }

    /**
     * Save contents of virtual machine into a snapshot file (Zip file).
     *
     * @param autorun
     * @param snapshotFileName
     * @throws IOException
     */
    public void storeSnapShot(String snapshotFileName, boolean autorun) throws IOException {
        String propfilename = System.getProperty("user.home") + File.separator + "snapshot.settings";
        String snapshotPngFile = System.getProperty("user.home") + File.separator + "snapshot.png";
        Properties properties = new Properties();

        if (snapshotFileName.toLowerCase().lastIndexOf(".z88") == -1) {
            snapshotFileName += ".z88"; // '.z88' extension is missing.
        }
        // save Z80 & Blink registers to properties collection
        storeZ80Regs(properties);
        storeBlinkRegs(properties);

        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(snapshotFileName));
        zipOut.setLevel(9); // compress contents as much as possible...

        // save a copy of the current screen...
        BufferedImage bi = Z88.getInstance().getDisplay().getScreenFrame();
        File imgf = new File(snapshotPngFile);
        ImageIO.write(bi, "PNG", imgf);
        copyToZip(snapshotPngFile, "snapshot.png", zipOut);

        for (int slotNo = 0; slotNo <= 3; slotNo++) {
            if (memory.isSlotEmpty(slotNo) == false) {
                properties.setProperty("SLOT" + slotNo + "TYPE", "" + SlotInfo.getInstance().getCardType(slotNo));

                if (slotNo == 0) {
                    // dump ROM.0 and RAM.0
                    memory.dumpSlot(slotNo, false, System.getProperty("user.home"), "");
                    copyToZip(System.getProperty("user.home") + File.separator + "rom.bin", "rom.bin", zipOut);
                    copyToZip(System.getProperty("user.home") + File.separator + "ram.bin", "ram.bin", zipOut);
                } else {
                    String slotShortFilename = "slot" + slotNo + ".bin";
                    memory.dumpSlot(slotNo, false, System.getProperty("user.home"), slotShortFilename);
                    if (
                            SlotInfo.getInstance().getCardType(slotNo) == SlotInfo.AmicHybridRamCard |
                            SlotInfo.getInstance().getCardType(slotNo) == SlotInfo.AmdHybridRamCard |
                            SlotInfo.getInstance().getCardType(slotNo) == SlotInfo.SstHybridRamCard
                            ) {
                        copyToZip(System.getProperty("user.home") + File.separator + "ram"+slotNo+".bin", "ram"+slotNo+".bin", zipOut);
                        copyToZip(System.getProperty("user.home") + File.separator + "flash"+slotNo+".bin", "flash"+slotNo+".bin", zipOut);
                    } else {
                        copyToZip(System.getProperty("user.home") + File.separator + slotShortFilename, slotShortFilename, zipOut);
                    }
                }
            } else {
                properties.setProperty("SLOT" + slotNo + "TYPE", "" + SlotInfo.EmptySlot);
            }
        }

        // remember the Host computer system time
        // when a snapshot is installed and the Blink TIMx register are adjusted to "lost" time...
        properties.setProperty("Z88StoppedAtTime", "" + z80.getZ88StoppedAtTime());

        // remember the breakpoints
        properties.setProperty("Breakpoints", z80.getBreakpoints().breakpointList());

        // remember virtual machine is to be auto-executed after restore
        // or just activate the debug command line..
        properties.setProperty("Autorun", Boolean.toString(autorun));

        // save the properties to a temp. file
        File pf = new File(propfilename);
        FileOutputStream pfs = new FileOutputStream(pf);
        properties.store(pfs, null);
        pfs.close();
        // Transfer the properties file to the ZIP archive
        copyToZip(propfilename, "snapshot.settings", zipOut);
        pf.delete(); // temp. properties file no longer needed in filing system...

        zipOut.close();
    }

    /**
     * Copy a file into a Zip archive.
     *
     * @param fileName the external file to be copied
     * @param zipFilename the name of the file on the Zip archive
     * @param zip the open Zip archive resource
     * @throws IOException
     */
    private void copyToZip(String fileName, String zipFilename, ZipOutputStream zip) throws IOException {
        int len;
        byte[] buffer = new byte[Bank.SIZE];
        FileInputStream in = new FileInputStream(fileName);
        zip.putNextEntry(new ZipEntry(zipFilename));
        while ((len = in.read(buffer)) > 0) {
            zip.write(buffer, 0, len);
        }
        in.close();

        new File(fileName).delete();
    }

    /**
     * Restore virtual machine from snapshot file.
     *
     * @param snapshotFileName
     * @return true if virtual machine is to be automatically executed after
     * restore
     * @throws IOException
     */
    public boolean loadSnapShot(String snapshotFileName) throws IOException {
        Bank cardBanks[] = null, flashBanks[] = null, ramBanks[] = null;
        ZipFile zf;
        ZipEntry ze;
        Properties properties = new Properties();
        boolean autorun;
        int slotType;

        if (snapshotFileName.toLowerCase().lastIndexOf(".z88") == -1) {
            snapshotFileName += ".z88"; // '.z88' extension is missing.
        }
        // Remove all current active memory and reset Blink before restoring a snapshot.
        memory.setVoidMemory();
        blink.resetBlink();

        // Open the snapshot (Zip) file
        zf = new ZipFile(snapshotFileName);

        // start with loading the properties...
        ze = zf.getEntry("snapshot.settings");
        if (ze != null) {
            properties.load(zf.getInputStream(ze));
        } else {
            throw new IOException("'snapshot.settings' is missing!");
        }

        ze = zf.getEntry("rom.bin"); // default slot 0 ROM
        if (ze != null) {
            if (properties.getProperty("SLOT0TYPE") != null ) {
                // a type has been defined for slot 0 (AMD or STM or ROM)
                slotType = Integer.parseInt(properties.getProperty("SLOT0TYPE"), 10);
                memory.loadRomBinary((int) ze.getSize(), slotType, zf.getInputStream(ze));
                switch(slotType) {
                    case SlotInfo.AmdFlashCard:
                        OZvm.getInstance().getGui().getAmd512KRomTypeMenuItem().setSelected(true);
                        break;
                    case SlotInfo.StmFlashCard:
                        OZvm.getInstance().getGui().getStm512KRomTypeMenuItem().setSelected(true);
                        break;
                }

            } else {
                // define default type by size of ROM binary
                memory.loadRomBinary((int) ze.getSize(), zf.getInputStream(ze));
            }
        } else {
            throw new IOException("ROM image is missing!");
        }

        ze = zf.getEntry("ram.bin"); // default slot 0 RAM
        if (ze != null) {
            memory.loadCardBinary(0, (int) ze.getSize(), SlotInfo.RamCard, zf.getInputStream(ze));
        } else {
            throw new IOException("RAM.0 image is missing!");
        }

        for (int slotNo = 1; slotNo <= 3; slotNo++) {
            slotType = Integer.parseInt(properties.getProperty("SLOT" + slotNo + "TYPE"), 10);

            if (slotType == SlotInfo.AmicHybridRamCard | slotType == SlotInfo.AmdHybridRamCard | slotType == SlotInfo.SstHybridRamCard) {
                cardBanks = memory.createCard(1024, slotType);

                ze = zf.getEntry("ram"+slotNo+".bin");
                ramBanks = memory.createCard(512, SlotInfo.RamCard);
                if (ze != null) {
                    memory.loadBinaryImageIntoContainer(ramBanks, (int) ze.getSize(), zf.getInputStream(ze));
                }

                switch(slotType) {
                    case SlotInfo.AmicHybridRamCard:
                        flashBanks = memory.createCard(512, SlotInfo.AmicFlashCard);
                        break;
                    case SlotInfo.AmdHybridRamCard:
                        flashBanks = memory.createCard(512, SlotInfo.AmdFlashCard);
                        break;
                    case SlotInfo.SstHybridRamCard:
                        flashBanks = memory.createCard(512, SlotInfo.SstFlashCard);
                        break;
                }

                ze = zf.getEntry("flash"+slotNo+".bin");
                if (ze != null) {
                    memory.loadBinaryImageIntoContainer(flashBanks, (int) ze.getSize(), zf.getInputStream(ze));
                } else {
                    // previous versions of snapshot incorrectly saved hybrid card only as a 512K flash image
                    ze = zf.getEntry("slot"+slotNo+".bin");
                    if (ze != null) {
                        memory.loadBinaryImageIntoContainer(flashBanks, (int) ze.getSize(), zf.getInputStream(ze));
                    }
                }

                System.arraycopy(ramBanks, 0, cardBanks, 0, ramBanks.length); // load 512K RAM into lower 1Mb of card space
                System.arraycopy(flashBanks, 0, cardBanks, flashBanks.length, flashBanks.length); // load 512K flash into upper part of card space

                memory.insertCard(cardBanks, slotNo);
            } else {
                ze = zf.getEntry("slot" + slotNo + ".bin");
                if (ze != null) {
                    memory.loadCardBinary(slotNo, (int) ze.getSize(), slotType, zf.getInputStream(ze));
                }
            }
        }

        loadZ80Regs(properties); // restore Z80 processor registers

        if (properties.getProperty("Z88StoppedAtTime") != null) {
            z80.setZ88StoppedAtTime(Long.parseLong(properties.getProperty("Z88StoppedAtTime")));
        } else {
            z80.setZ88StoppedAtTime(System.currentTimeMillis());
        }
        
        loadBlinkRegs(properties); // restore Blink hardware registers
        loadBreakpoints(properties); // restore breakpoints from snapshot

        if (properties.getProperty("Autorun") != null) {
            autorun = Boolean.valueOf(properties.getProperty("Autorun")).booleanValue();
        } else {
            autorun = true;
        }

        // V1.3, restore the resolution LCD settings in Gui checkbox sub-menu
        if (properties.getProperty("SCW") != null && properties.getProperty("SCH") != null) {
            int lcdScrWidth = Integer.parseInt(properties.getProperty("SCW"), 16) * 8;
            int lcdScrHeight = Integer.parseInt(properties.getProperty("SCH"), 16) * 8;
            if (lcdScrWidth == 640 && lcdScrHeight == 64) OZvm.getInstance().getGui().getScreen640x64MenuItem().setSelected(true);
            if (lcdScrWidth == 640 && lcdScrHeight == 320) OZvm.getInstance().getGui().getScreen640x320MenuItem().setSelected(true);
            if (lcdScrWidth == 640 && lcdScrHeight == 480) OZvm.getInstance().getGui().getScreen640x480MenuItem().setSelected(true);
            if (lcdScrWidth == 800 && lcdScrHeight == 320) OZvm.getInstance().getGui().getScreen800x320MenuItem().setSelected(true);
            if (lcdScrWidth == 800 && lcdScrHeight == 480) OZvm.getInstance().getGui().getScreen800x480MenuItem().setSelected(true);
        } else {
            // these registers were not defined in snapshot file, default to 640x64 resolution
            OZvm.getInstance().getGui().getScreen640x64MenuItem().setSelected(true);
        }

        zf.close();

        return autorun;
    }

    /**
     * Get a JFileChooser filter for Z88 snapshot files
     */
    public SnapshotFilter getSnapshotFilter() {
        return new SnapshotFilter();
    }

    /**
     * JFileChooser filter to validate z88 snapshot files
     */
    private class SnapshotFilter extends FileFilter {

        /**
         * Accept all directories and z88 files
         */
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }

            String extension = getExtension(f);
            if (extension != null) {
                if (extension.equalsIgnoreCase("z88") == true) {
                    ZipFile zf;
                    ZipEntry ze;
                    Properties properties = new Properties();

                    try {
                        // Try to open the snapshot (Zip) file
                        zf = new ZipFile(f);

                        // try to load the properties...
                        ze = zf.getEntry("snapshot.settings");
                        if (ze != null) {
                            properties.load(zf.getInputStream(ze));
                        } else {
                            return false;
                        }

                        ze = zf.getEntry("rom.bin");
                        if (ze == null) {
                            return false;
                        }

                        zf.close();
                    } catch (IOException e) {
                        return false;
                    }

                    // the Zip file was polled successfully.
                    return true;

                } else {
                    // ignore files that doesn't use the 'z88' extension.
                    return false;
                }
            }

            // file didn't have an extension...
            return false;
        }

        /**
         * The description of this filter
         */
        public String getDescription() {
            return "Z88 snapshot files";
        }

        /**
         * Get the extension of a file
         */
        private String getExtension(File f) {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 && i < s.length() - 1) {
                ext = s.substring(i + 1).toLowerCase();
            }
            return ext;
        }
    }
}
