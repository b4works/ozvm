/*
 * Z80Processor.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

/**
 * Z80 processor implementation in Z88.
 */
public class Z80Processor extends Z80 implements Runnable {

    private Blink blink;
    private Breakpoints breakpoints;
    private final Watchpoints watchpoints;
    private long z88StoppedAtTime;
    private boolean z80ThreadRunning;
    private boolean simulateInterrupts;

    /**
     * Internal signal for stopping the Z80 execution engine
     */
    private boolean stopZ88;

    public Z80Processor() {

        blink = null;
        breakpoints = new Breakpoints();
        watchpoints = new Watchpoints();
        simulateInterrupts = true;
        stopZ88 = false;
        z80ThreadRunning = false;
    }

    /**
     * This method is used by Z88 Class to define the Blink chip for the processor
     */
    public void connectBlink(Blink bl) {
        blink = bl;
    }

    /**
     * helper functionality for "z" debug and "trace <address>" commands
     * @param oneStopBreakpoint
     * @return
     */
    public int trace(final int oneStopBreakpoint) {
        setOneStopBreakpoint(oneStopBreakpoint);
        return trace();
    }

    /**
     * execute Z80 instructions until a breakpoint is reached, stop command is
     * entered or F5 was pressed in Z88 screen
     */
    public void execute() {
        z80ThreadRunning = true;  // indicate running
        super.execute();          // run until we drop dread!
        z80ThreadRunning = false; // indicate running
    }

    /**
     * A HALT instruction was executed by the Z80 processor; Go into coma and
     * wait for an interrupt (normally when both SHIFT keys pressed) HALT is
     * ignored if Blink is in single stepping mode.
     *
     * HALT is doing nothing if the Z80 thread is not running
     */
    public synchronized void halt(long msTimeout) {
        if (z80ThreadRunning == true) {
            // HALT is simulated when running the Z80 CPU

            // During Coma, the I register hold the address lines to be read, and to generate an
            // interrupt when both SHIFT keys are pressed (matching the address line A8-A15)
            // Coma handling is done in Blink.signalKeyPressed() and Blink.Rtc.Counter.run()

            /**
             * The Z80Processor (and Z88) is set into coma mode when a HALT instruction
             * is executed and the screen is turned off. Only both SHIFT keys pressed
             * (defined by I register address line) can awake the Z80Processor from Coma
             * with an INT signal.
             *
             * This method is executed by the Z80Processor thread and is suspended while
             * waiting for an interrupt from the Blink.
             */

            try {
                blink.enableSnooze(msTimeout);
            } catch (InterruptedException e1) {
            }

            // Awakened from Coma..  maskable interrupt has been signaled to Z80..
        }
    }

    public void stopZ80Execution() {
        stopZ88 = true;
    }

    /**
     * @return the system time when Z88 was stopped.
     */
    public long getZ88StoppedAtTime() {
        return z88StoppedAtTime;
    }

    /**
     * Restore system time when Z88 was stopped (from snapshot).
     */
    public void setZ88StoppedAtTime(long time) {
        z88StoppedAtTime = time;
    }

    /**
     * Check if F5 key was pressed, or a stop was issued at command line.
     */
    public boolean isZ80Stopped() {
        if (stopZ88 == true) {
            stopZ88 = false;

            z88StoppedAtTime = System.currentTimeMillis();
            OZvm.displayRtmMessage("Z88 virtual machine was stopped at " + Dz.extAddrToHex(blink.decodeLocalAddress(getInstrPC()), true));

            return true;
        } else {
            return false;
        }
    }

    /**
     * Read byte from Z80 virtual memory model. <addr> is a 16bit word that
     * points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * If a databus read watchpoint is enabled for the current address, the
     * Watchpoint Manager takes appropriate action for the specified address;
     * typically stopping the Z80 virtual machine from executing.
     *
     * Read watchpoints are ignored while single stepping in command mode
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return byte at bank, mapped into segment for specified address
     */
    public int readByte(int addr) {
        int b = blink.readByte(addr);

        if ( (blink.isReadWatchpoint(addr) == true) && (isZ80running() == true || isTracing() == true) ) {
            watchPointAction(addr);
        }

        return b;
    }

    /**
     * Z80 Program Counter read from Z80 virtual memory model. <addr> is a 16bit
     * word that points into the Z80 64K address space.
     *
     * If a breakpoint is enabled for the current address, the Breakpoint
     * Manager takes appropriate action for the specified address; typically
     * stopping the Z80 virtual machine from executing.
     *
     * This method is called only when the Z80 processor is decoding the first
     * instruction opcode.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return byte at bank, mapped into segment for specified address
     */
    public int pcReadByte(int addr) {
        int opcode = blink.readByte(addr);

        if ( (blink.isBreakpoint(addr) == true) && (isZ80running() == true || isTracing() == true)) {
            breakPointAction();
        } else {
            if (opcode == 231) {
                if (breakpoints.isBreakOzCallActive() == true) {
                    ozCallBreakAction();
                }
                if (breakpoints.isDisplayOzCallActive() == true) {
                    ozCallDisplayAction();
                }
            }
        }

        return opcode;
    }

    /**
     * Write byte to Z80 virtual memory model. <addr> is a 16bit word that
     * points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * If a databus write watchpoint is enabled for the current address, the
     * Watchpoint Manager takes appropriate action for the specified address;
     * typically stopping the Z80 virtual machine from executing. The byte
     * is written even though the Z80 CPU was stopped by a watchpoint.
     *
     * Write watchpoints are ignored while single stepping in command mode
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @param b byte to be written into Z80 64K Address Space
     */
    public void writeByte(int addr, int b) {
        if ( (blink.isWriteWatchpoint(addr) == true) && (isZ80running() == true || isTracing() == true) ) {
            watchPointAction(addr);
        }

        blink.writeByte(addr, b);
    }

    /**
     * Read word (16bits) from Z80 virtual memory model. <addr> is a 16bit word
     * that points into the Z80 64K address space.
     *
     * Reading a 16bit integer is done across 2 x 8bits databus reads by
     * the Z80 CPU but is regarded as an atomic operation.
     *
     * 16bit word fetches across bank boundaries are automatically handled.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * If a databus read watchpoint is enabled for the current address(es), the
     * Watchpoint Manager takes appropriate action for the specified address;
     * typically stopping the Z80 virtual machine from executing.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return word at bank, mapped into segment for specified address
     */
    public int readWord(int addr) {
        return (readByte(addr + 1) << 8) | readByte(addr);
    }

    /**
     * Write word (16bits) to Z80 virtual memory model.
     *
     * Writing a 16bit integer is done across 2 x 8bits databus writes by
     * the Z80 CPU but is regarded as an atomic operation.
     *
     * 16bit word write across bank boundaries are automatically handled.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * If a databus write watchpoint is enabled for the current address, the
     * Watchpoint Manager takes appropriate action for the specified address;
     * typically stopping the Z80 virtual machine from executing. The 16bits
     * is written even though the Z80 CPU was stopped by a watchpoint.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @param w word to be written into Z80 64K Address Space
     */
    public void writeWord(int addr, int w) {
        writeByte(addr, w);
        writeByte(addr + 1, w >>> 8);
    }

    /**
     * Handle action on encountered breakpoint.<p>
     */
    private void breakPointAction() {
        int bpAddress = blink.decodeLocalAddress(getInstrPC());

        if (breakpoints.isActive(bpAddress) == true) {
            if (breakpoints.isStoppable(bpAddress) == true) {
                stopZ80Execution();
                breakpoints.recordTstates(bpAddress, getTstates());

                if (z80ThreadRunning == true) {
                    DebugGui.getInstance().activateDebugCommandLine(); // Activate Debug Command Line Window...
                    CommandLine.getInstance().cmdlineFirstSingleStep();
                    DebugGui.getInstance().lockZ88MachinePanel(true);
                }

                if (breakpoints.hasCommands(bpAddress) == true) {
                    breakpoints.runCommands(bpAddress);
                }

                if (breakpoints.isSingleStopBreakpoint(bpAddress) == true) {
                    breakpoints.clearBreakpoint(bpAddress);
                }
            } else {
                OZvm.displayRtmMessage(Z88Info.dzPcStatus(getInstrPC()));   // dissassemble original instruction, with Z80 main reg dump
            }
        }
    }

    /**
     * Handle action on encountered databus Read/Write watchpoint.
     * The watchpoint action is skipped, if the Z80 processor already
     * has been signaled to stop.
     *
     * (typically, a previous watchpoint was triggered in the same
     * executing Z80 instruction)
     */
    private void watchPointAction(int databusAddress) {
        if (z80ThreadRunning == true) {
            databusAddress = blink.decodeLocalAddress(databusAddress);
            if (watchpoints.isActive(databusAddress) == true) {

                if (watchpoints.isStoppable(databusAddress) == true) {
                    // since a watchpoint is happing while instruction is executing,
                    // signal to the processor to stop immediately after completing the instruction
                    z80Stopped = true;

                    // Activate Debug Command Line Window...
                    DebugGui.getInstance().activateDebugCommandLine();
                    // output the instruction that caused the watchpoint
                    DebugGui.getInstance().consoleOutput(Z88Info.dzPcStatus(getInstrPC()));
                    // and display message about watchpoint triggered instruction
                    DebugGui.getInstance().consoleOutput("Watchpoint "+Dz.extAddrToHex(databusAddress, true)+" signaled CPU stop");
                    if (watchpoints.hasCommands(databusAddress) == true) {
                        watchpoints.runCommands(databusAddress);
                    }
                } else {
                    // runtime display instruction that caused watchpoint, with Z80 main reg dump
                    OZvm.displayRtmMessage(Z88Info.dzPcStatus(getInstrPC()));
                }
            }
        }
    }

    /**
     * Execute a stop Z80 engine action when an OZ system call is about to be executed.<p>
     */
    private void ozCallBreakAction() {
        OZvm.displayRtmMessage(Z88Info.dzPcStatus(getInstrPC()));   // dissassemble original instruction, with Z80 main reg dump
        stopZ80Execution();
        DebugGui.getInstance().activateDebugCommandLine(); // Activate Debug Command Line Window...
    }

    /**
     * Display OZ Call details in Runtime Messages when OZ system call is about to be executed.<p>
     */
    private void ozCallDisplayAction() {
        OZvm.displayRtmMessage(Z88Info.dzPcStatus(getInstrPC()));   // dissassemble original instruction, with Z80 main reg dump
    }

    /**
     * @return Returns the breakpoints.
     */
    public Breakpoints getBreakpoints() {
        return breakpoints;
    }

    /**
     * @return Returns the watchpoints.
     */
    public Watchpoints getWatchpoints() {
        return watchpoints;
    }

    /**
     * @param breakpoints The breakpoints to set.
     */
    public void setBreakpoints(Breakpoints breakpoints) {
        this.breakpoints = breakpoints;
    }

    /**
     * Implement Z88 output port Blink hardware. (RTC, Screen, Keyboard, Memory
     * model, Serial port, CPU state).
     *
     * @param addrA8 LSB of port address
     * @param addrA15 MSB of port address
     * @param outByte the data to send to the hardware
     */
    public final void outByte(final int addrA8, final int addrA15, final int outByte) {
        switch (addrA8) {
            case 0xD0: // SR0, Segment register 0
            case 0xD1: // SR1, Segment register 1
            case 0xD2: // SR2, Segment register 2
            case 0xD3: // SR3, Segment register 3
                blink.setSegmentBank(addrA8, outByte);
                break;

            case 0xB0: // COM, Set Command Register
                blink.setCom(outByte);
                break;

            case 0xB1: // INT, Set Main Blink Interrupts
                blink.setInt(outByte);
                break;

            case 0xB3: // EPR, Eprom Programming Register
                blink.setEpr(outByte);
                break;

            case 0xB4: // TACK, Set Timer Interrupt Acknowledge
                blink.setTack(outByte);
                break;

            case 0xB5: // TMK, Set Timer interrupt Mask
                blink.setTmk(outByte);
                break;

            case 0xB6: // ACK, Acknowledge INT Interrupts
                blink.setAck(outByte);
                break;

            case 0x70: // PB0, Pixel Base Register 0 (Screen)
                blink.setPb0((addrA15 << 8) | outByte);
                break;

            case 0x71: // PB1, Pixel Base Register 1 (Screen)
                blink.setPb1((addrA15 << 8) | outByte);
                break;

            case 0x72: // PB2, Pixel Base Register 2 (Screen)
                blink.setPb2((addrA15 << 8) | outByte);
                break;

            case 0x73: // PB3, Pixel Base Register 3 (Screen)
                blink.setPb3((addrA15 << 8) | outByte);
                break;

            case 0x74: // SBR, Screen Base Register
                blink.setSbr((addrA15 << 8) | outByte);
                break;

            case 0xE2: // RXC, UART Receiver Control (not yet implemented)
                break;

            case 0xE3: // TXD, UART Transmit Data, output to Runtime window
                OZvm.getInstance().getRtmMsgGui().displayRtmChar((char) outByte);
                break;

            case 0xE4: // TXC, UART Transmit Control (not yet implemented)
                break;

            case 0xE5: // UMK, UART Int. mask (not yet implemented)
                break;

            case 0xE6: // UAK, UART acknowledge int. mask (not yet implemented)
                break;

            default:
                OZvm.displayRtmMessage("WARNING:\n"
                        + Z88Info.dzPcStatus(getInstrPC()) + "\n"
                        + "Blink Write Register " + Dz.byteToHex(addrA8, true) + " does not exist.");
        }
    }

    /**
     * Implement Z88 input port BLINK hardware (Registers STA, KBD, TSTA,
     * TIM0-TIM4, RXD, RXE, UIT).
     *
     * @param addrA8 Port number (low byte address)
     * @param addrA15 high byte address
     */
    public final int inByte(int addrA8, int addrA15) {
        int res = 0;

        switch (addrA8) {
            case 0xB0:
                // Machine Identification (MID)
                //      $01: F88
                //      $80: ZVM
                //      $FF: Z88 (Blink on Cambridge Z88 does not implement read operation, and returns $FF)
                res = 0x80;
                break;
            case 0xB1:
                res = blink.getSta(); // STA, Main Blink Interrupt Status
                break;

            case 0xB2:
                if (isZ80ThreadRunning() == true) {
                    // KBD, get Keyboard column for specified row via full Blink / Z88 simulation (Halt, Snooze etc).
                    res = blink.getKbd(addrA15);
                } else {
                    if (isTracing() == true) {
                        // this instruction is run via trace() and must stop because of dead lock (no interrupts are fired to collect key press)
                        stopZ80Execution();

                        // Activate Debug Command Line Window...
                        DebugGui.getInstance().activateDebugCommandLine();
                        // output the IN X,($B2) instruction
                        DebugGui.getInstance().consoleOutput(Z88Info.dzPcStatus(getInstrPC()));
                    }

                    // scan keyboard in raw mode, no Blink Z88 simulation
                    res = blink.scanKeyRow(addrA15);
                }
                break;

            case 0xB5:
                res = blink.getTsta(); // TSTA, which RTC interrupt occurred...
                break;

            case 0xD0:
                res = blink.getTim0(); // TIM0, 5ms period, counts to 199
                break;

            case 0xD1:
                res = blink.getTim1(); // TIM1, 1 second period, counts to 59
                break;

            case 0xD2:
                res = blink.getTim2(); // TIM2, 1 minute period, counts to 255
                break;

            case 0xD3:
                res = blink.getTim3(); // TIM3, 256 minutes period, counts to 255
                break;

            case 0xD4:
                res = blink.getTim4(); // TIM4, 64K minutes Period, counts to 31
                break;

            case 0xE0:                  // RxD
                res = 0;
                break;

            case 0xE1:                  // RxE
                res = 0;
                break;

            case 0xE5:                  // UIT, UART Int status, always ready to receive...
                res = 0x10;
                break;

            case 0x70:                  // SCW, get screen width in pixels / 8
                res = blink.getSCW();
                break;

            case 0x71:                  // SCH, get screen height in pixels / 8
                res = blink.getSCH();
                break;

            default:
                OZvm.displayRtmMessage("WARNING:\n"
                        + Z88Info.dzPcStatus(getInstrPC()) + "\n"
                        + "Blink Read Register " + Dz.byteToHex(addrA8, true) + " does not exist.");
                res = 0;
        }

        return res;
    }

    /**
     * Thread start; execute the Z80 processor
     */
    public void run() {
        Breakpoints breakPointManager = getBreakpoints();
        Thread.currentThread().setName("Z80Processor");

        if (breakPointManager.isStoppable(blink.decodeLocalAddress(PC())) == true) {
            // we need to do a single step to move
            // past the break point at current instruction
            step();
        }

        if (simulateInterrupts == true) {
            blink.startRtcInterrupts(); // enable Z80/Z88 core interrupts
        }

        // execute Z80 code at full speed until breakpoint is encountered...
        // (or F5 emergency break is used!)
        execute();

        // Z80 engine has now stopped (F5 or breakpoint encountered)
        if (simulateInterrupts == true) {
            blink.stopRtcInterrupts();
        }
    }

    public boolean isZ80ThreadRunning() {
        return z80ThreadRunning;
    }

    public void setInterrupts(boolean interrupts) {
        this.simulateInterrupts = interrupts;
    }

    public void setOneStopBreakpoint(int oneStopBreakpoint) {
        breakpoints.setSingleStopBreakpoint(oneStopBreakpoint);
    }

    public int getPcAddress() {
        return blink.decodeLocalAddress(PC());
    }

    /**
     * Signal /INT signal to Z80 by returning boolean true
     *
     * Return true, if a Blink interrupt has been enabled in STA (and indirectly TSTA) and INT.GINT = 1
     */
    public boolean intRequest() {
        return (((blink.getInt() & Blink.BM_INTGINT) == Blink.BM_INTGINT) & (blink.getInt() & blink.getSta()) != 0);
    }
}
