/*
 * Z88.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import java.io.File;
import java.io.IOException;

/**
 * The Z88 class defines the Z88 virtual machine (Processor, Blink, Memory,
 * Display & Keyboard).
 */
public class Z88 {

    private final Blink blink;
    private final Memory memory;
    private final Z88Keyboard keyboard;
    private final Z88display display;
    private final Z80Processor z80;
    /**
     * Reference to the current executing Z80 processor.
     */
    private Thread z80Thread;

    /**
     * Z88 class default constructor.
     */
    private Z88() {
        z80 = new Z80Processor();
        blink = new Blink();
        memory = new Memory();
        display = new Z88display();
        keyboard = new Z88Keyboard();

        z80.connectBlink(blink);

        blink.connectProcessor(z80);
        blink.connectMemory(memory);
        blink.connectKeyboard(keyboard);

        display.connectProcessor(z80);
        display.connectBlink(blink);
        display.connectMemory(memory);

        // map Host keyboard events to this z88 keyboard, so that the emulator responds to keypresses.
        display.addKeyListener(keyboard.processKeyInput());       
    }

    private static final class singletonContainer {

        static final Z88 singleton = new Z88();
    }

    private void stopZ80Cpu() {
        if (getProcessorThread() != null) {
            // stop the Z80 CPU, if it's alive; exit the Z80Processor.run() method (via signal-to-stop variable)
            getProcessor().stopZ80Execution();

            // if thread is sleeping, there is nothing to stop... so force a wake-up, so Z80 can read signal-to-stop variable
            blink.awakeZ80();

            while (z80.isZ80ThreadRunning() == true) Thread.yield(); // wait until the Z80 thread has terminated..
        }

        z80Thread = null;
    }

    public static Z88 getInstance() {
        return singletonContainer.singleton;
    }

    public Blink getBlink() {
        return blink;
    }

    public Memory getMemory() {
        return memory;
    }

    public Z88display getDisplay() {
        return display;
    }

    public Z88Keyboard getKeyboard() {
        return keyboard;
    }

    /**
     * 'Press' the reset button on the left side of the Z88 (hidden in the small
     * crack next to the power plug)
     */
    public void pressResetButton() {
        stopZ80Cpu();
        z80.reset();                     // Reset Z80 CPU registers
        runZ80Cpu();                     // then run again...
    }

    public void hardResetWithSlot0Rom(File rom) throws IOException {
        stopZ80Cpu();

        getMemory().loadRomBinary(rom); // then load a new ROM image into slot 0
        getMemory().resetRam(); // wipe all RAM in Z88 computer
        blink.resetBlink();
        getProcessor().reset();

        runZ80Cpu(); // boot slot 0 ROM
    }

    /**
     * Perform a Hard Reset of the Z88 Virtual Machine.
     *
     * 1) The flap must have been opened, before executing this functionality.
     */
    public void hardReset() {
        stopZ80Cpu();
        memory.resetRam();              // wipe all RAM
        blink.resetBlink();
        pressResetButton();             // Reset Z80 CPU registers
    }

    public Z80Processor getProcessor() {
        return z80;
    }

    public Thread getProcessorThread() {
        if (z80Thread != null && z80Thread.isAlive() == true) {
            return z80Thread;
        } else {
            z80Thread = null;

            return null;
        }
    }

    /**
     * Execute a Z80 thread until breakpoint is encountered (or F5 is pressed)
     *
     * @param oneStopBreakpoint
     *
     * @return true if thread was successfully started.
     */
    public boolean runZ80Cpu() {
        if (z80Thread != null && z80Thread.isAlive() == true) {
            return false;
        } else {
            OZvm.displayRtmMessage("Z88 virtual machine was started.");

            z80Thread = new Thread(z80);
            z80Thread.start();

            return true;
        }
    }

    /**
     * Execute a Z80 thread until temporary breakpoint is encountered (or F5 is
     * pressed)
     *
     * @param oneStopBreakpoint
     *
     * @return true if thread was successfully started.
     */
    public boolean runZ80Cpu(final int oneStopBreakpoint) {
        if (z80Thread != null && z80Thread.isAlive() == true) {
            return false;
        } else {
            z80.setOneStopBreakpoint(oneStopBreakpoint);
            return runZ80Cpu();
        }
    }
}
