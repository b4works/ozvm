/*
 * AmicFlashBank.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

/**
 * This class represents the 16Kb Flash Memory Bank on an AMIC A29F040B 512k chip.
 *
 * The characteristics of a Flash Memory bank is chip memory that can be read at
 * all times and only be written (and erased) using a combination of AMD Flash
 * command sequences (write byte to address cycles), in ALL available slots on
 * the Z88.
 *
 * The emulation of the AMD/AMIC Flash Memory solely implements the chip command
 * mode programming, since the Z88 Flash Cards only responds to those command
 * sequences (and not the hardware pin manipulation). Erase Suspend and Erase
 * Resume commands are also not implemented.
 *
 * The essential emulation is implemented to respond to the Standard Flash Eprom
 * Library (which implements all Flash chip manipulation, issuing commands on a
 * bank, typically specified indirectly using the BHL Z80 registers).
 */
public class AmicFlashBank extends GenericAmdFlashBank {

    /**
     * Device Code for 512Kb memory, 8 x 64K erasable sectors, 32 x 16K banks
     */
    public static final int DEVICECODE = 0x86;

    /**
     * Manufacturer Code for A29F0xxx Flash Memory chips
     */
    public static final int MANUFACTURERCODE = 0x37;

    /**
     * The actual Flash Memory Device Size in 16K banks (512K)
     */
    public static final int TOTALCARDBANKS = 32;

    /**
     * Default Constructor. Assign the Flash Memory bank to the 4Mb memory
     * model.
     *
     */
    public AmicFlashBank() {
        super();
    }

    /**
     * @return the Flash Memory Device Code (A29F040B)
     * which this bank is part of.
     */
    public final int getDeviceCode() {
        return DEVICECODE;
    }

    /**
     * @return the Flash Memory Manufacturer Code
     *
     */
    public final int getManufacturerCode() {
        return MANUFACTURERCODE;
    }

    /**
     * Return the actual Flash Memory Device Size in 16K banks
     */
    public final int getDeviceSize() {
        return TOTALCARDBANKS;
    }

    /**
     * Erase the 64K Flash Memory Sector which this bank is part of
     * (always succeeds in emulation).
     *
     * @param addr the bank offset where the erase sector command is targeted
     */
    public void eraseSector(int addr) {
        // erase the bottom bank of the current sector and the next three following banks
        // (the address is not used in this case, we're already in the bank)
        int bottomBankOfSector = getBankNumber() & 0xFC;  // bottom bank of sector

        for (int thisBank = bottomBankOfSector; thisBank <= (bottomBankOfSector + 3); thisBank++) {
            eraseBank(thisBank);
        }

        // finalize internal chip mode after sector erasure.
        super.eraseSector(addr);
    }
}
