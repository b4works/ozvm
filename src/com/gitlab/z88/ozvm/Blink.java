/*
 * Blink.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import com.imagero.util.ThreadManager;
import java.util.Timer;
import java.util.TimerTask;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JMenu;

/**
 * Blink chip, the "body" of the Z88, defining the surrounding hardware of the
 * Z80 "mind" processor.
 */
public final class Blink {

    /**
     * Blink Snooze state
     */
    private volatile boolean snooze;
    /**
     * Blink Coma state
     */
    private boolean coma;
    /**
     * Access to the Z88 Memory Model
     */
    private Memory memory;
    /**
     * Access to Z80 Processor
     */
    private Z80Processor z80;
    /**
     * Access to the Z88 keyboard
     */
    private Z88Keyboard keyboard;

    /**
     * The main Timer daemon that runs the Rtc clock and generates 5ms TICK INT's
     * to the Z80 virtual processor.
     */
    private final Timer rtcTimer;

    /**
     * The Real Time Clock (RTC) inside the BLINK.
     */
    private final Rtc rtc;

    /**
     * TIM0, 5 millisecond period, counts to 199, Z80 IN Register
     */
    private int TIM0;
    /**
     * TIM1, 1 second period, counts to 59, Z80 IN Register
     */
    private int TIM1;
    /**
     * TIM2, 1 minutes period, counts to 255, Z80 IN Register
     */
    private int TIM2;
    /**
     * TIM3, 256 minutes period, counts to 255, Z80 IN Register
     */
    private int TIM3;
    /**
     * TIM4, 64K minutes period, counts to 31, Z80 IN Register
     */
    private int TIM4;
    /**
     * TSTA, Timer interrupt status, Z80 IN Read Register
     */
    private int TSTA;
    /**
     * TMK, Timer interrupt mask, Z80 OUT Write Register
     */
    private int TMK;

    /**
     * Main Blink Interrrupts (INT).
     *
     * <PRE>
     * BIT 7, KWAIT  If set, reading the keyboard will Snooze
     * BIT 6, A19    If set, an active high on A19 will exit Coma
     * BIT 5, FLAP   If set, flap interrupts are enabled
     * BIT 4, UART   If set, UART interrupts are enabled
     * BIT 3, BTL    If set, battery low interrupts are enabled
     * BIT 2, KEY    If set, keyboard interrupts (Snooze or Coma) are enabl.
     * BIT 1, TIME   If set, RTC interrupts are enabled
     * BIT 0, GINT   If clear, no interrupts get out of blink
     * </PRE>
     */
    private int INT;
    public static final int BM_INTKWAIT = 0x80; // Bit 7, If set, reading the keyboard will Snooze
    public static final int BM_INTA19 = 0x40;   // Bit 6, If set, an active high on A19 will exit Coma
    public static final int BM_INTFLAP = 0x20;  // Bit 5, If set, flap interrupts are enabled
    public static final int BM_INTUART = 0x10;  // Bit 4, If set, UART interrupts are enabled
    public static final int BM_INTBTL = 0x08;   // Bit 3, If set, battery low interrupts are enabled
    public static final int BM_INTKEY = 0x04;   // Bit 2, If set, keyboard interrupts (Snooze or Coma) are enabl.
    public static final int BM_INTTIME = 0x02;  // Bit 1, If set, RTC interrupts are enabled
    public static final int BM_INTGINT = 0x01;  // Bit 0, If clear, no interrupts get out of blink

    /**
     * Main Blink Interrupt Status (STA)
     *
     * <PRE>
     * Bit 7, FLAPOPEN, If set, flap open, else flap closed
     * Bit 6, A19, If set, high level on A19 occurred during coma
     * Bit 5, FLAP, If set, positive edge has occurred on FLAPOPEN
     * Bit 4, UART, If set, an enabled UART interrupt is active
     * Bit 3, BTL, If set, battery low pin is active
     * Bit 2, KEY, If set, a column has gone low in snooze (or coma)
     * Bit 1, not defined.
     * Bit 0, TIME, If set, an enabled TSTA interrupt is active
     * </PRE>
     */
    private int STA;

    public static final int BM_STAFLAPOPEN = 0x80;  // Bit 7, If set, flap open, else flap closed
    public static final int BM_STAA19 = 0x40;       // Bit 6, If set, high level on A19 occurred during coma
    public static final int BM_STAFLAP = 0x20;      // Bit 5, If set, positive edge has occurred on FLAPOPEN
    public static final int BM_STAUART = 0x10;      // Bit 4, If set, an enabled UART interrupt is active
    public static final int BM_STABTL = 0x08;       // Bit 3, If set, battery low pin is active
    public static final int BM_STAKEY = 0x04;       // Bit 2, If set, a column has gone low in snooze (or coma)
    public static final int BM_STATIME = 0x01;      // Bit 0, If set, an enabled TSTA interrupt is active

    public static final int BM_TSTAMIN = 0x04;      // TSTA: Set if minute interrupt has occurred
    public static final int BM_TSTASEC = 0x02;      // TSTA: Set if second interrupt has occurred
    public static final int BM_TSTATICK = 0x01;     // TSTA: Set if tick interrupt has occurred
    public static final int BM_TMKMIN = 0x04;       // TMK: Set to enable minute interrupt
    public static final int BM_TMKSEC = 0x02;       // TMK: Set to enable second interrupt
    public static final int BM_TMKTICK = 0x01;      // TMK: Set to enable tick interrupt
    public static final int BM_TACKMIN = 0x04;      // TACK: Set to acknowledge minute interrupt
    public static final int BM_TACKSEC = 0x02;      // TACK: Set to acknowledge second interrupt
    public static final int BM_TACKTICK = 0x01;     // TACK: Set to acknowledge tick interrupt

    /**
     * LORES0 (PB0, 16bits register).<br> The 6 * 8 pixel per char User Defined
     * Fonts.
     */
    private int PB0;
    /**
     * LORES1 (PB1, 16bits register).<br> The 6 * 8 pixel per char fonts.
     */
    private int PB1;
    /**
     * HIRES0 (PB2 16bits register) (The 8 * 8 pixel per char PipeDream Map)
     */
    private int PB2;
    /**
     * HIRES1 (PB3, 16bits register) (The 8 * 8 pixel per char fonts for the OZ
     * window)
     */
    private int PB3;
    /**
     * Screen Base Register (16bits register) (The Screen base File (2K size),
     * containing char info about screen) If this register is 0, then the system
     * cannot render the pixel screen.
     */
    private int SBR;

    /**
     * Blink Read register, SCW ($70)
     * LCD Horisontal resolution in pixels / 8
     * Available horisontal resolutions:
     * 640 pixels ($FF or 80), 800 pixels (100)
     */
    private int SCW = 0xff; // define default value (not implemented) as on Cambridge Z88

    /**
     * Blink Read register, SCH ($71)
     * LCD Vertical resolution in pixels / 8
     * Available horisontal resolutions:
     * 64 pixels ($FF or 8), 256 pixels (32), 480 pixels (60)
     */
    private int SCH = 0xff; // define default value (not implemented) as on Cambridge Z88

    /**
     * System bank for lower 8K of segment 0. References bank 0x00 or 0x20 of
     * slot 0.
     */
    private Bank RAMS;
    /**
     * Segment register array for SR0 - SR3.
     *
     * <PRE>
     * Segment register 0, SR0, bank binding for 0x2000 - 0x3FFF
     * Segment register 1, SR1, bank binding for 0x4000 - 0x7FFF
     * Segment register 2, SR2, bank binding for 0x8000 - 0xBFFF
     * Segment register 3, SR3, bank binding for 0xC000 - 0xFFFF
     * </PRE>
     *
     * Any of the registers contains a bank number, 0 - 255 that is currently
     * bound into the corresponding segment in the Z80 address space.
     */
    private int sR[];
    /**
     * BLINK Command Register.
     *
     * <PRE>
     *  Bit  7, SRUN
     *  Bit  6, SBIT
     *  Bit  5, OVERP
     *  Bit  4, RESTIM
     *  Bit  3, PROGRAM
     *  Bit  2, RAMS
     *  Bit  1, VPPON
     *  Bit  0, LCDON
     * </PRE>
     */
    private int COM;
    public static final int BM_COMSRUN = 0x80; // Bit 7, SRUN
    public static final int BM_COMSBIT = 0x40; // Bit 6, SBIT
    public static final int BM_COMOVERP = 0x20; // Bit 5, OVERP
    public static final int BM_COMRESTIM = 0x10; // Bit 4, RESTIM
    public static final int BM_COMPROGRAM = 0x08; // Bit 3, PROGRAM
    public static final int BM_COMRAMS = 0x04; // Bit 2, RAMS
    public static final int BM_COMVPPON = 0x02; // Bit 1, VPPON
    public static final int BM_COMLCDON = 0x01; // Bit 0, LCDON
    /**
     * BLINK Eprom Programming Register.
     *
     * <PRE>
     *  Bit  7, PD1
     *  Bit  6, PD0
     *  Bit  5, PGMD
     *  Bit  4, EOED
     *  Bit  3, SE3D
     *  Bit  2, PGMP
     *  Bit  1, EOEP
     *  Bit  0, SE3P
     * </PRE>
     */
    private int EPR;
    public static final int BM_EPRPD1 = 0x80; // Bit 7, PD1
    public static final int BM_EPRPD0 = 0x40; // Bit 6, PD0
    public static final int BM_EPRPGMD = 0x20; // Bit 5, PGMD
    public static final int BM_EPREOED = 0x10; // Bit 4, EOED
    public static final int BM_EPRSE3D = 0x08; // Bit 3, SE3D
    public static final int BM_EPRPGMP = 0x04; // Bit 2, PGMP
    public static final int BM_EPREOEP = 0x02; // Bit 1, EOEP
    public static final int BM_EPRSE3P = 0x01; // Bit 0, SE3P

    /**
     * Used for beep()
     */
    private static final int sampleFrequency = 44100;

    /**
     * The output volume of the 3200Hz beeper
     */
    private static final double beepVolume = 0.6;

    /**
     * Used for 3200hz beeper
     */
    private AudioFormat af;

    /**
     * Used for 3200hz beeper
     */
    private SourceDataLine sdl;

    private byte[] sound3200Hz;
    private byte[] silence3200Hz;
    /**
     * Blink class default constructor.
     */
    public Blink() {
        super();

        try {
            initAudioSystem();
            // pre-generate the 3200Hz sound pattern for 10ms, that is executed by RTC
            sound3200Hz = generate10msX3200hzBits(beepVolume);
            silence3200Hz = generate10msX3200hzBits(0.0);
        } catch (LineUnavailableException ex) {}

        snooze = false;
        z80 = null;
        memory = null;
        keyboard = null;

        // the segment register SR0 - SR3
        sR = new int[4];

        rtcTimer = new Timer(true);
        rtc = new Rtc();                // the Real Time Clock counter, not yet started...

        resetBlink();
    }

    private void initAudioSystem() throws LineUnavailableException {

        af = new AudioFormat(sampleFrequency, // sampleRate
                   16, // sampleSizeInBits
                   1, // channels
                   true, // signed
                   false); // bigEndian

        DataLine.Info info = new DataLine.Info(SourceDataLine.class, af);

        if (!AudioSystem.isLineSupported(info)) {
            sdl = null;
            OZvm.displayRtmMessage("Line matching '" + info + "' is not supported.");
            throw new LineUnavailableException();
        } else {
            sdl = (SourceDataLine) AudioSystem.getLine(info);
            sdl = AudioSystem.getSourceDataLine(af);
            sdl.open(af);
            sdl.start();
        }
    }

    /**
     * Produce a 10ms x 3200Hz sound bit pattern that can be delivered to Java Sound API
     * @return
     */
    private byte[] generate10msX3200hzBits(double bpVol) {
        int soundFrames = 10 * sampleFrequency/1000; // 16bits * 10ms * sampleFrequency/1000
        byte[] soundBuffer = new byte[soundFrames*2];

        float numberOfSamplesToRepresentFullSin = (float) sampleFrequency / 3200;
        int i = 0;
        for (int sf = 0; sf < soundFrames; sf++) { // 1000 ms in 1 second
          double angle = sf / (numberOfSamplesToRepresentFullSin/ 2.0) * Math.PI;  // /divide with 2 since sin goes 0PI to 2PI
          short a = (short) (Math.sin(angle) * 32767 * bpVol);  //32767 - max value for sample to take (-32767 to 32767)
          soundBuffer[i] = (byte) (a & 0xFF); //write 8bits ________WWWWWWWW out of 16
          soundBuffer[i+1] = (byte) (a >> 8); //write 8bits WWWWWWWW________ out of 16
          i += 2;
        }

        return soundBuffer;
    }

    /**
     * @return the SCW
     */
    public int getSCW() {
        return SCW;
    }

    /**
     * Define Blink LCD horisontal solution / 8
     * Allowed input is 640/8 or 800/8
     *
     * @param width8 the SCW to set
     */
    public void setSCW(int width8) {
        switch(width8) {
            case 80:    // 640/8
            case 100:   // 800/8
                this.SCW = width8;
                break;
            default:
                this.SCW = 80;    // fallback default 640 pixels, horisontal
        }
    }

    /**
     * @return the SCH
     */
    public int getSCH() {
        return SCH;
    }

    /**
     * Define Blink LCD vertical solution / 8
     *
     * @param height8 the SCH to set
     */
    public void setSCH(int height8) {
        switch(height8) {
            case 8:     // 64/8
            case 40:    // 320/8
            case 60:    // 480/8
                this.SCH = height8;
                break;
            default:
                this.SCH = 8;     // fallback default 64 pixels, vertical
        }
    }

    /**
     * This method is used by Z88 Class to define the Z80 processor for the Blink
     */
    public void connectProcessor(Z80Processor z80Proc) {
        z80 = z80Proc;
    }

    /**
     * This method is used by Z88 Class to define the memory access for the Blink
     */
    public void connectMemory(Memory mem) {
        memory = mem;               // access to Z88 memory model (4Mb)
        RAMS = memory.getBank(0);   // point at ROM bank 0
    }

    /**
     * This method is used by Z88 Class to define the keyboard matrix for the Blink
     */
    public void connectKeyboard(Z88Keyboard kb) {
        keyboard = kb;
    }

    /**
     * Reset Blink Registers to Power-On-State.
     * SCH, SCW are not reset (resolution is "hardwired" in the Blink)
     */
    public void resetBlink() {
        PB0 = PB1 = PB2 = PB3 = SBR = 0;
        COM = 0;

        resetTimx();

        INT = BM_INTFLAP | BM_INTTIME | BM_INTGINT;
        TMK = BM_TMKTICK;

        STA = TSTA = 0;

        // SR0, SR1, SR2, SR3 = 0
        for (int segment = 0; segment < sR.length; segment++) {
            sR[segment] = 0;
        }
    }

    public void resetTimx() {
        TIM0 = TIM1 = TIM2 = TIM3 = TIM4 = 0;
    }

    /**
     * Set main Blink Interrupts (INT), via Z80 OUT instruction.
     *
     * <pre>
     * BIT 7, KWAIT  If set, reading the keyboard will Snooze
     * BIT 6, A19    If set, an active high on A19 will exit Coma
     * BIT 5, FLAP   If set, flap interrupts are enabled
     * BIT 4, UART   If set, UART interrupts are enabled>
     * BIT 3, BTL    If set, battery low interrupts are enabled
     * BIT 2, KEY    If set, keyboard interrupts (Snooze or Coma) are enabled
     * BIT 1, TIME   If set, RTC interrupts are enabled
     * BIT 0, GINT   If clear, no interrupts get out of blink
     * </pre>
     *
     * @param bits
     */
    public void setInt(int bits) {
        //String dzAddr = Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()) & 0xff0000 | z80.getInstrPC(),true);

        if (((INT & Blink.BM_INTKWAIT) == Blink.BM_INTKWAIT) & ((bits & Blink.BM_INTKWAIT) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTKWAIT -> 0");
        }
        if (((INT & Blink.BM_INTKWAIT) == 0) & ((bits & Blink.BM_INTKWAIT) == Blink.BM_INTKWAIT)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTKWAIT -> 1");
        }

        if (((INT & Blink.BM_INTA19) == 0) & ((bits & Blink.BM_INTA19) == Blink.BM_INTA19)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTA19 -> 1");
        }
        if (((INT & Blink.BM_INTA19) == Blink.BM_INTA19) & ((bits & Blink.BM_INTA19) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTA19 -> 0");
        }

        if (((INT & Blink.BM_INTFLAP) == 0) & ((bits & Blink.BM_INTFLAP) == Blink.BM_INTFLAP)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTFLAP -> 1");
        }
        if (((INT & Blink.BM_INTFLAP) == Blink.BM_INTFLAP) & ((bits & Blink.BM_INTFLAP) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTFLAP -> 0");
        }

        if (((INT & Blink.BM_INTUART) == 0) & ((bits & Blink.BM_INTUART) == Blink.BM_INTUART)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTUART -> 1");
        }
        if (((INT & Blink.BM_INTUART) == Blink.BM_INTUART) & ((bits & Blink.BM_INTUART) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTUART -> 0");
        }

        if (((INT & Blink.BM_INTBTL) == 0) & ((bits & Blink.BM_INTBTL) == Blink.BM_INTBTL)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTBTL -> 1");
        }
        if (((INT & Blink.BM_INTBTL) == Blink.BM_INTBTL) & ((bits & Blink.BM_INTBTL) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTBTL -> 0");
        }

        if (((INT & Blink.BM_INTKEY) == 0) & ((bits & Blink.BM_INTKEY) == Blink.BM_INTKEY)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTKEY -> 1");
        }
        if (((INT & Blink.BM_INTKEY) == Blink.BM_INTKEY) & ((bits & Blink.BM_INTKEY) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTKEY -> 0");
        }

        if (((INT & Blink.BM_INTTIME) == 0) & ((bits & Blink.BM_INTTIME) == Blink.BM_INTTIME)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTTIME -> 1");
        }
        if (((INT & Blink.BM_INTTIME) == Blink.BM_INTTIME) & ((bits & Blink.BM_INTTIME) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTTIME -> 0");
        }

        if (((INT & Blink.BM_INTGINT) == 0) & ((bits & Blink.BM_INTGINT) == Blink.BM_INTGINT)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTGINT -> 1");
        }
        if (((INT & Blink.BM_INTGINT) == Blink.BM_INTGINT) & ((bits & Blink.BM_INTGINT) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": INT.BM_INTGINT -> 0");
        }

        INT = bits;
    }

    /**
     * Get main Blink Interrupts (INT)
     *
     * <pre>
     * BIT 7, KWAIT  If set, reading the keyboard will Snooze
     * BIT 6, A19    If set, an active high on A19 will exit Coma
     * BIT 5, FLAP   If set, flap interrupts are enabled
     * BIT 4, UART   If set, UART interrupts are enabled>
     * BIT 3, BTL    If set, battery low interrupts are enabled
     * BIT 2, KEY    If set, keyboard interrupts (Snooze or Coma) are enabled
     * BIT 1, TIME   If set, RTC interrupts are enabled
     * BIT 0, GINT   If clear, no interrupts get out of blink
     * </pre>
     *
     * @return INT Blink Register
     */
    public int getInt() {
        return INT;
    }

    /**
     * Set Main Blink Interrupt Acknowledge (ACK), via Z80 OUT instruction
     *
     * <PRE>
     * BIT 6, A19    Acknowledge active high on A19
     * BIT 5, FLAP   Acknowledge Flap interrupts
     * BIT 3, BTL    Acknowledge battery low interrupt
     * BIT 2, KEY    Acknowledge keyboard interrupt
     * </PRE>
     *
     * @param bits
     */
    public void setAck(int bits) {
        setSta( getSta() & ~bits); // Acknowledge (and clear) occurred STA interrupt (NAND)

        String dzAddr = Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()) & 0xff0000 | z80.getInstrPC(),true);
        if ((bits & BM_STAA19) == BM_STAA19) {
            //OZvm.displayRtmMessage(dzAddr + ", ACK -> A19, STA = " + Dz.byteToBin(STA, true));
        }
        if ((bits & BM_STAFLAP) == BM_STAFLAP) {
            OZvm.displayRtmMessage(dzAddr + ", ACK -> FLAP, STA = " + Dz.byteToBin(getSta(), true));
        }
        if ((bits & BM_STAFLAPOPEN) == BM_STAFLAPOPEN) {
            OZvm.displayRtmMessage(dzAddr + ", ACK -> FLAPOPEN, STA = " + Dz.byteToBin(getSta(), true));
        }
        if ((bits & BM_STABTL) == BM_STABTL) {
            OZvm.displayRtmMessage(dzAddr + ", ACK -> BTL, STA = " + Dz.byteToBin(getSta(), true));
        }
        if ((bits & BM_STAKEY) == BM_STAKEY) {
            OZvm.displayRtmMessage(dzAddr + ", ACK -> KEY, STA = " + Dz.byteToBin(STA, true));
        }
        if ((bits & BM_STAUART) == BM_STAUART) {
            //OZvm.displayRtmMessage(dzAddr + ", ACK -> TIME, STA = " + Dz.byteToBin(STA, true));
        }
        if ((bits & BM_STATIME) == BM_STATIME) {
            OZvm.displayRtmMessage(dzAddr + ", ACK -> TIME, STA = " + Dz.byteToBin(getSta(), true));
        }
    }

    /**
     * Get Main Blink Interrupt Status (STA).
     *
     * <PRE>
     * Bit 7, FLAPOPEN, If set, flap open, else flap closed
     * Bit 6, A19, If set, high level on A19 occurred during coma
     * Bit 5, FLAP, If set, positive edge has occurred on FLAPOPEN
     * Bit 4, UART, If set, an enabled UART interrupt is active
     * Bit 3, BTL, If set, battery low pin is active
     * Bit 2, KEY, If set, a column has gone low in snooze (or coma)
     * </PRE>
     */
    public synchronized int getSta() {
        return STA;
    }

    /**
     * Set Main Blink Interrupt Status (STA).
     * Used for restore machine state functionality
     *
     * <PRE>
     * Bit 7, FLAPOPEN, If set, flap open, else flap closed
     * Bit 6, A19, If set, high level on A19 occurred during coma
     * Bit 5, FLAP, If set, positive edge has occurred on FLAPOPEN
     * Bit 4, UART, If set, an enabled UART interrupt is active
     * Bit 3, BTL, If set, battery low pin is active
     * Bit 2, KEY, If set, a column has gone low in snooze (or coma)
     * Bit 1, not defined.
     * Bit 0, TIME, If set, an enabled TSTA interrupt is active
     * </PRE>
     *
     * @param staBits
     */
    public synchronized void setSta(int staBits) {
        STA = staBits;
    }

    /**
     * Return Timer Interrupt Status (TSTA).
     *
     * <PRE>
     * BIT 2, MIN, Set if minute interrupt has occurred
     * BIT 1, SEC, Set if second interrupt has occurred
     * BIT 0, TICK, Set if tick interrupt has occurred
     * </PRE>
     *
     * @return TSTA
     */
    public synchronized int getTsta() {
        return TSTA;
    }

    /**
     * Set Timer Interrupt Status (TSTA).
     * Used for restore machine state functionality
     *
     * <PRE>
     * BIT 2, MIN, Set if minute interrupt has occurred
     * BIT 1, SEC, Set if second interrupt has occurred
     * BIT 0, TICK, Set if 10ms tick interrupt has occurred
     * </PRE>
     */
    public synchronized void setTsta(int tstaBits) {
        TSTA = tstaBits;
    }

    /**
     * Set Timer Interrupt Acknowledge (TACK), via Z80 OUT instruction.
     *
     * <PRE>
     * BIT 2, MIN, Set to acknowledge minute interrupt
     * BIT 1, SEC, Set to acknowledge second interrupt
     * BIT 0, TICK, Set to acknowledge tick interrupt
     * </PRE>
     */
    public void setTack(int bits) {
        //String dzAddr = Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()) & 0xff0000 | z80.getInstrPC(),true);
        //String msg = "";

        if ( (bits & BM_TSTATICK) == BM_TSTATICK) {
            //msg = dzAddr + ", TACK -> TICK, TSTA(" + Dz.byteToBin(getTsta(), true) + "), STA("+ Dz.byteToBin(getSta(), true) + ") -> ";
            setTsta( getTsta() & ~BM_TSTATICK );
        }
        if ( (bits & BM_TSTASEC) == BM_TSTASEC) {
            //msg = dzAddr + ", TACK -> SEC, TSTA(" + Dz.byteToBin(getTsta(), true) + "), STA("+ Dz.byteToBin(getSta(), true) + ") -> ";
            setTsta( getTsta() & ~BM_TSTASEC );
        }
        if ( (bits & BM_TSTAMIN) == BM_TSTAMIN) {
            //msg = dzAddr + ", TACK -> MIN, TSTA(" + Dz.byteToBin(getTsta(), true) + "), STA("+ Dz.byteToBin(getSta(), true) + ") -> ";
            setTsta( getTsta() & ~BM_TSTAMIN );
        }

        if ( getTsta() == 0) {
            setSta( getSta() & ~BM_STATIME);
            //msg += "TSTA = " + Dz.byteToBin(getTsta(), true) + ", STA = " + Dz.byteToBin(getSta(), true) + " (TIM0=" + getTim0() + ",TIM1=" + getTim1() + ",TIM2=" + getTim2() +")";
            //OZvm.displayRtmMessage(msg);
        }
    }

    /**
     * Set Timer Interrupt Mask (TMK), via Z80 OUT instruction
     *
     * <PRE>
     * BIT 2, MIN, Set to enable minute interrupt
     * BIT 1, SEC, Set to enable second interrupt
     * BIT 0, TICK, Set enable tick interrupt
     * </PRE>
     */
    public void setTmk(int bits) {
        TMK = bits;
        //String msg = Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()) & 0xff0000 | z80.getInstrPC(),true) + ", TMK -> " + Dz.byteToBin(bits, true);
        //OZvm.displayRtmMessage(msg);
    }

    /**
     * Get Timer Interrupt Mask (TMK)
     *
     * <PRE>
     * BIT 2, MIN, Set to enable minute interrupt
     * BIT 1, SEC, Set to enable second interrupt
     * BIT 0, TICK, Set enable tick interrupt
     * </PRE>
     */
    public int getTmk() {
        return TMK;
    }

    /**
     * Get current TIM0 register from the RTC.
     *
     * @return int
     */
    public synchronized int getTim0() {
        return TIM0;
    }

    /**
     * set current Real Time Clock TIM0 register. (Used for restore machine
     * state functionality)
     */
    public void setTim0(int tim0Bits) {
        TIM0 = tim0Bits;
    }

    /**
     * Get current TIM1 register from the RTC.
     *
     * @return int
     */
    public synchronized int getTim1() {
        return TIM1;
    }

    /**
     * set current Real Time Clock TIM1 register. (Used for restore machine
     * state functionality)
     */
    public void setTim1(int bits) {
        TIM1 = bits;
    }

    /**
     * Get current TIM2 register from the RTC.
     *
     * @return int
     */
    public synchronized int getTim2() {
        return TIM2;
    }

    /**
     * set current Real Time Clock TIM2 register. (Used for restore machine
     * state functionality)
     */
    public synchronized void setTim2(int bits) {
        TIM2 = bits;
    }

    /**
     * Get current TIM3 register from the RTC.
     *
     * @return int
     */
    public int getTim3() {
        return TIM3;
    }

    /**
     * set current Real Time Clock TIM3 register.
     * Internal use: restore machine state functionality
     */
    public void setTim3(int bits) {
        TIM3 = bits;
    }

    /**
     * Get current TIM4 register from the RTC.
     *
     * @return int
     */
    public int getTim4() {
        return TIM4;
    }

    /**
     * set current Real Time Clock TIM4 register.
     * Internal use: restore machine state functionality
     */
    public void setTim4(int bits) {
        TIM4 = bits;
    }

    /**
     * Set LORES0 (PB0, 16bits register).<br> The 6 * 8 pixel per char User
     * Defined Fonts.
     */
    public void setPb0(int bits) {
        PB0 = bits;
    }

    /**
     * Get LORES0 (PB0, 16bits register).<br> The 6 * 8 pixel per char User
     * Defined Fonts.
     */
    public int getPb0() {
        return PB0;
    }

    /**
     * Get Address of LORES0 (PB0 16bits register) in 24bit extended address
     * format.<br> The 6 * 8 pixel per char User Defined Fonts.
     */
    public int getPb0Address() {
        int extAddressBank = (PB0 << 3) & 0xF700;
        int extAddressOffset = (PB0 << 1) & 0x003F;

        return (extAddressBank | extAddressOffset) << 8;
    }

    /**
     * Set LORES1 (PB1, 16bits register).<br> The 6 * 8 pixel per char fonts.
     */
    public void setPb1(int bits) {
        PB1 = bits;
    }

    /**
     * Get LORES1 (PB1, 16bits register).<br> The 6 * 8 pixel per char fonts.
     */
    public int getPb1() {
        return PB1;
    }

    /**
     * Get Address of LORES1 (PB1 16bits register) in 24bit extended address
     * format.<br> The 6 * 8 pixel per char fonts.
     */
    public int getPb1Address() {
        int extAddressBank = (PB1 << 6) & 0xFF00;
        int extAddressOffset = (PB1 << 4) & 0x0030;

        return (extAddressBank | extAddressOffset) << 8;
    }

    /**
     * Set HIRES0 (PB2 16bits register) (The 8 * 8 pixel per char PipeDream Map)
     */
    public void setPb2(int bits) {
        PB2 = bits;
    }

    /**
     * Get HIRES0 (PB2 16bits register) (The 8 * 8 pixel per char PipeDream Map)
     */
    public int getPb2() {
        return PB2;
    }

    /**
     * Get Address of HIRES0 (PB2 register) in 24bit extended address format.
     * (The 8 * 8 pixel per char PipeDream Map)
     */
    public int getPb2Address() {
        int extAddressBank = (PB2 << 7) & 0xFF00;
        int extAddressOffset = (PB2 << 5) & 0x0020;

        return (extAddressBank | extAddressOffset) << 8;
    }

    /**
     * Set HIRES1 (PB3, 16bits register) (The 8 * 8 pixel per char fonts for the
     * OZ window)
     */
    public void setPb3(int bits) {
        PB3 = bits;
    }

    /**
     * Set HIRES1 (PB3, 16bits register) (The 8 * 8 pixel per char fonts for the
     * OZ window)
     */
    public int getPb3() {
        return PB3;
    }

    /**
     * Get Address of HIRES1 (PB3 16bits register) in 24bit extended address
     * format. (The 8 * 8 pixel per char fonts for the OZ window)
     */
    public int getPb3Address() {
        int extAddressBank = (PB3 << 5) & 0xFF00;
        int extAddressOffset = (PB3 << 3) & 0x0038;

        return (extAddressBank | extAddressOffset) << 8;
    }

    /**
     * Set Screen Base Register (16bits register) (The Screen base File (2K
     * size), containing char info about screen) If this register is 0, then the
     * system cannot render the pixel screen.
     */
    public void setSbr(int bits) {
        SBR = bits;
    }

    /**
     * Get Screen Base Register (16bits register) (The Screen base File (2K
     * size), containing char info about screen) If this register is 0, then the
     * system cannot render the pixel screen.
     */
    public int getSbr() {
        return SBR;
    }

    /**
     * Get Screen Base in 24bit extended address format. (The Screen base File
     * (2K or bigger size), containing char info about screen) If this register
     * is 0, then the system cannot render the pixel screen.
     *
     * The SBR register holds a 16bit value of Bank no + Page offset within bank
     * The location of the SBR is always from top of bank, downwards.
     * First page in bank (no segment specifier) defines base of SBR.
     * SBR has 11 bits size (8bit bank no, 3bit page offset modulus 8)
     * It looks like this, uncompressed:
     *      bbbbbbbb 00ppp000   16bit bank + page offset, modulus 8
     * This is shifted down 5 bits, like this in the SBR register (11 bits):
     *      00000bbb bbbbbppp   16bit bank + page offset, modulus 8
     *
     * The segment specifier in the Page offset stripped (bits 7,6).
     * The highest Page offset is therefore 38H (00111000b). The lowest 8 or 0.
     */
    public int getSbrAddress() {
        int extAddressBank = (SBR << 5) & 0xFF00;
        int extAddressOffset = (SBR << 3) & 0x38; // Page offset is always 00ppp000

        return (extAddressBank | extAddressOffset) << 8;
    }

    /**
     * Signal to the Z80 processor that a key was pressed. The internal state
     * machine inside the Blink resolves the snooze state or coma state and
     * fires KEY interrupts, if enabled.
     *
     * This method is called from the Z88Keyboard thread, and will awake
     * Z80Processor thread (if the Z88 was snoozing or in coma).
     */
    public synchronized void signalKeyPressed() {
        int blSta = getSta();

        if (((INT & BM_INTKEY) == BM_INTKEY) & ((blSta & BM_STAKEY) == 0)) {
            // If keyboard interrupts are enabled, then signal that a key was pressed.
            // But only if the interrupt is allowed to escape the Blink...
            setSta( blSta | BM_STAKEY);
            //OZvm.displayRtmMessage("Blink Int STA.KEY -> 1");
        }

        // processor always awakes on a key press (even if INT.GINT = 0)
        awakeZ80();
    }

    /**
     * Fetch a keypress from the specified row(s) matrix, or 0 for all rows.<br>
     * Interface call for IN r,(B2h).<br>
     *
     * This method is executed through the Z80Processor thread. If Blink KWAIT
     * is enabled, the Z80 processor will snooze and wait for a key press
     * from the Blink.
     *
     * @param row, eg @10111111, or 0 for all rows.
     * @return int keycolumn status of row or merge of columns for specified
     * rows.
     */
    public synchronized int getKbd(int row) {
        if ((INT & Blink.BM_INTKWAIT) == Blink.BM_INTKWAIT) {
            try {
                enableSnooze(0);
            } catch (InterruptedException ie) {
            }
        }

        return keyboard.scanKeyRow(row);
    }


    /**
     * Fetch a keypress from the specified row(s) matrix, or 0 for all rows.<br>
     * Generic API for IN r,(B2h).<br>
     *
     * This method is used outside of Z88 hardware simulation and is used only
     * for simple single stepping / tracing on debug command line.
     *
     * (This is to avoid hanging the command line Gui thread WHEN single-stepping)
     *
     * @param row, eg @10111111, or 0 for all rows.
     * @return int keycolumn status of row or merge of columns for specified
     * rows.
     */
    public int scanKeyRow(int row) {
        return keyboard.scanKeyRow(row);
    }

    /**
     * Get current bank [0; 255] binding in segments [0; 3].
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however. Please refer to hardware section of the Developer's
     * Notes.
     *
     * @return int
     */
    public int getSegmentBank(final int segment) {
        return sR[segment & 0x03];
    }

    /**
     * Bind bank [0-255] to segments [0-3] in the Z80 address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 x 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however. Please refer to hardware section of the Developer's
     * Notes.
     */
    public void setSegmentBank(final int segment, final int BankNo) {
        sR[segment & 0x03] = BankNo;
    }

    /**
     * Decode Z80 Address Space to extended Blink Address (bank,offset).
     *
     * @param pc 16bit word that points into Z80 64K Address Space
     * @return int 24bit extended address (bank number, bank offset)
     */
    public int decodeLocalAddress(int pc) {
        int bankno;

        if (pc > 0x3FFF) {
            bankno = sR[(pc & 0xffff) >>> 14];
        } else {
            if (pc < 0x2000) // return lower 8K Bank binding
            // Lower 8K is System Bank 0x00 (ROM on hard reset)
            // or 0x20 (RAM for Z80 stack and system variables)
            {
                if ((COM & Blink.BM_COMRAMS) == Blink.BM_COMRAMS) {
                    bankno = 0x20;  // RAM Bank 20h
                } else {
                    bankno = 0x00;  // ROM bank 00h
                }
            } else {
                // 0x2000 <= pc <= 0x3FFF
                bankno = sR[0] & 0xFE; // banks are always even in SR0..
                if ((sR[0] & 1) == 0) {
                    // lower 8K of even bank bound into upper 8K of segment 0
                    // (relocate bank offset pointer to lower 8K)
                    pc &= 0x1FFF;
                }
            }
        }

        return (bankno << 16) | (pc & 0x3FFF);
    }

    /**
     * Decode Z88 Extended Blink Address (bank,offset) into specified Z80
     * Address Space segment (0 - 3)
     *
     * @param extaddr 24bit extended address (bank number & bank offset)
     * @return int 16bit word that points into Z80 64K Address Space
     */
    public int decodeExtendedAddress(int extaddr, int segment) {
        segment &= 0x03; // segments 0-3 in Z80 address space..

        if (segment > 0) {
            return (extaddr & 0x003fff) | (segment << 14);
        } else {
            return (extaddr & 0x001fff) | 0x2000;
        }
    }

    /**
     * Read byte from Z80 virtual memory model. <addr> is a 16bit word that
     * points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return byte at bank, mapped into segment for specified address
     */
    public final int readByte(final int addr) {
        try {
            if (addr > 0x3FFF) {
                return memory.getBank(sR[addr >>> 14]).readByte(addr & 0x3fff);
            } else {
                if (addr < 0x2000) // return lower 8K Bank binding
                // Lower 8K is System Bank 0x00 (ROM on hard reset)
                // or 0x20 (RAM for Z80 stack and system variables)
                {
                    return RAMS.readByte(addr);
                } else {
                    if ((sR[0] & 1) == 0) // lower 8K of even bank bound into upper 8K of segment 0
                    {
                        return memory.getBank(sR[0] & 0xFE).readByte(addr & 0x1FFF);
                    } else // upper 8K of even bank bound into upper 8K of segment 0
                    // addr <= 0x3FFF...
                    {
                        return memory.getBank(sR[0] & 0xFE).readByte(addr);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // PC is problably 0x10000
            return 0;
        }
    }

    /**
     * Read breakpoint status from Z80 virtual memory model. <addr> is a 16bit
     * word that points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return byte at bank, mapped into segment for specified address
     */
    public final boolean isBreakpoint(final int addr) {
        try {
            if (addr > 0x3FFF) {
                return memory.getBank(sR[addr >>> 14]).isBreakpoint(addr & 0x3fff);
            } else {
                if (addr < 0x2000) // return lower 8K Bank binding
                // Lower 8K is System Bank 0x00 (ROM on hard reset)
                // or 0x20 (RAM for Z80 stack and system variables)
                {
                    return RAMS.isBreakpoint(addr);
                } else {
                    if ((sR[0] & 1) == 0) // lower 8K of even bank bound into upper 8K of segment 0
                    {
                        return memory.getBank(sR[0] & 0xFE).isBreakpoint(addr & 0x1FFF);
                    } else // upper 8K of even bank bound into upper 8K of segment 0
                    // addr <= 0x3FFF...
                    {
                        return memory.getBank(sR[0] & 0xFE).isBreakpoint(addr);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // PC is problably 0x10000
            return false;
        }
    }

    /**
     * Check databus Read-Watchpoint status from Z80 virtual memory model. <addr> is a 16bit
     * word that points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return bool indicates whether a databus read watchpoint is active at address (true)
     */
    public final boolean isReadWatchpoint(final int addr) {
        try {
            if (addr > 0x3FFF) {
                return memory.getBank(sR[addr >>> 14]).isReadWatchpoint(addr & 0x3fff);
            } else {
                if (addr < 0x2000) // return lower 8K Bank binding
                // Lower 8K is System Bank 0x00 (ROM on hard reset)
                // or 0x20 (RAM for Z80 stack and system variables)
                {
                    return RAMS.isReadWatchpoint(addr);
                } else {
                    if ((sR[0] & 1) == 0) // lower 8K of even bank bound into upper 8K of segment 0
                    {
                        return memory.getBank(sR[0] & 0xFE).isReadWatchpoint(addr & 0x1FFF);
                    } else // upper 8K of even bank bound into upper 8K of segment 0
                    // addr <= 0x3FFF...
                    {
                        return memory.getBank(sR[0] & 0xFE).isReadWatchpoint(addr);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // PC is problably 0x10000
            return false;
        }
    }

    /**
     * Check databus Write-Watchpoint status from Z80 virtual memory model. <addr> is a 16bit
     * word that points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @return bool indicates whether a databus write watchpoint is active at address (true)
     */
    public final boolean isWriteWatchpoint(final int addr) {
        try {
            if (addr > 0x3FFF) {
                return memory.getBank(sR[addr >>> 14]).isWriteWatchpoint(addr & 0x3fff);
            } else {
                if (addr < 0x2000) // return lower 8K Bank binding
                // Lower 8K is System Bank 0x00 (ROM on hard reset)
                // or 0x20 (RAM for Z80 stack and system variables)
                {
                    return RAMS.isWriteWatchpoint(addr);
                } else {
                    if ((sR[0] & 1) == 0) // lower 8K of even bank bound into upper 8K of segment 0
                    {
                        return memory.getBank(sR[0] & 0xFE).isWriteWatchpoint(addr & 0x1FFF);
                    } else // upper 8K of even bank bound into upper 8K of segment 0
                    // addr <= 0x3FFF...
                    {
                        return memory.getBank(sR[0] & 0xFE).isWriteWatchpoint(addr);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // PC is problably 0x10000
            return false;
        }
    }

    /**
     * Write byte to Z80 virtual memory model. <addr> is a 16bit word that
     * points into the Z80 64K address space.
     *
     * On the Z88, the 64K is split into 4 sections of 16K segments. Any of the
     * 256 16K banks can be bound into the address space on the Z88. Bank 0 is
     * special, however.
     *
     * Please refer to hardware section of the Developer's Notes.
     *
     * @param addr 16bit word that points into Z80 64K Address Space
     * @param b byte to be written into Z80 64K Address Space
     */
    public final void writeByte(final int addr, final int b) {
        try {
            if (addr > 0x3FFF) {
                // write byte to segments 1 - 3
                memory.getBank(sR[addr >>> 14]).writeByte(addr & 0x3fff, b);
            } else {
                if (addr < 0x2000) {
                    // return lower 8K Bank binding
                    // Lower 8K is System Bank 0x00 (ROM on hard reset)
                    // or 0x20 (RAM for Z80 stack and system variables)
                    RAMS.writeByte(addr, b);
                } else {
                    Bank bank = memory.getBank(sR[0] & 0xFE);
                    if ((sR[0] & 1) == 0) {
                        // lower 8K of even bank bound into upper 8K of segment 0
                        bank.writeByte(addr & 0x1FFF, b);
                    } else {
                        // upper 8K of even bank bound into upper 8K of segment 0
                        // addr <= 0x3FFF...
                        bank.writeByte(addr, b);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // PC is problably 0x10000
        }
    }

    /**
     * Add the lost time to TIMx registers, which means when a virtual machine
     * was stopped (including RTC), time continues to run on the host operating
     * system.
     *
     * Add the time gone to the TIMx registers from the previous stop until NOW.
     */
    public void adjustLostTime() {
        long rtcElapsedTime = 0;

        rtcElapsedTime += getTim0() * 5; // convert to ms.
        rtcElapsedTime += getTim1() * 1000;  // convert from sec to ms.
        rtcElapsedTime += getTim2() * 60 * 1000;  // convert from min to ms.
        rtcElapsedTime += getTim3() * 256 * 60 * 1000;  // convert from 256 min to ms.
        rtcElapsedTime += getTim4() * 65536 * 60 * 1000;  // convert from 64K min to ms.
        rtcElapsedTime += (System.currentTimeMillis() - z80.getZ88StoppedAtTime()); // add host system elapsed time...

        setTim4(((int) (rtcElapsedTime / 65536 / 60 / 1000)) & 0xFF);

        setTim3(((int) (((rtcElapsedTime / 1000 / 60) - (getTim4() * 65536)) / 256)) & 0xFF);

        setTim2(((int) (((rtcElapsedTime / 1000 / 60) - (getTim4() * 65536)) - (getTim3() * 256))) & 0xFF);

        setTim1(((int) (((rtcElapsedTime / 1000) - (getTim4() * 65536 * 60))
                - (getTim3() * 256 * 60) - getTim2() * 60)) & 0xFF);

        setTim0(((int) (((rtcElapsedTime - (getTim4() * 65536 * 60 * 1000))
                - (getTim3() * 256 * 60 * 1000) - (getTim2() * 60 * 1000)
                - (getTim1() * 1000)) / 5)) & 0xFF);
    }

    /**
     * Set Blink Command Register flags, via Z80 OUT instruction.
     *
     * <PRE>
     *  Bit 7, SRUN
     *  Bit 6, SBIT
     *  Bit 5, OVERP
     *  Bit 4, RESTIM
     *  Bit 3, PROGRAM
     *  Bit 2, RAMS
     *  Bit 1, VPPON
     *  Bit 0, LCDON
     * </PRE>
     *
     * @param bits
     */
    public void setCom(int bits) {
        String dzAddr = Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()) & 0xff0000 | z80.getInstrPC(),true);

        if (((COM & Blink.BM_COMOVERP) == 0) & ((bits & Blink.BM_COMOVERP) == Blink.BM_COMOVERP)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.OVERP -> 1");
        }

        if (((COM & Blink.BM_COMOVERP) == Blink.BM_COMOVERP) & ((bits & Blink.BM_COMOVERP) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.OVERP -> 0");
        }

        if (((COM & Blink.BM_COMPROGRAM) == 0) & ((bits & Blink.BM_COMPROGRAM) == Blink.BM_COMPROGRAM)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.PROGRAM -> 1");
        }

        if (((COM & Blink.BM_COMPROGRAM) == Blink.BM_COMPROGRAM) & ((bits & Blink.BM_COMPROGRAM) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.PROGRAM -> 0");
        }

        if (((COM & Blink.BM_COMRAMS) == 0) & ((bits & Blink.BM_COMRAMS) == Blink.BM_COMRAMS)) {
            //OZvm.displayRtmMessage(dzAddr + ": COM.RAMS -> 1");
        }

        if (((COM & Blink.BM_COMRAMS) == Blink.BM_COMRAMS) & ((bits & Blink.BM_COMRAMS) == 0)) {
            //OZvm.displayRtmMessage(dzAddr  + ": COM.RAMS -> 0");
        }

        if (((COM & Blink.BM_COMVPPON) == 0) & ((bits & Blink.BM_COMVPPON) == Blink.BM_COMVPPON)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.VPPON -> 1");
        }

        if (((COM & Blink.BM_COMVPPON) == Blink.BM_COMVPPON) & ((bits & Blink.BM_COMVPPON) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.VPPON -> 0");
        }

        if (((COM & Blink.BM_COMLCDON) == 0) & ((bits & Blink.BM_COMLCDON) == Blink.BM_COMLCDON)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.LCDON -> 1");
        }

        if (((COM & Blink.BM_COMLCDON) == Blink.BM_COMLCDON) & ((bits & Blink.BM_COMLCDON) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.LCDON -> 0");
        }

        if (((COM & Blink.BM_COMRESTIM) == 0) & ((bits & Blink.BM_COMRESTIM) == Blink.BM_COMRESTIM)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.RESTIM -> 1");
            resetTimx();
        }

        if (((COM & Blink.BM_COMRESTIM) == Blink.BM_COMRESTIM) & ((bits & Blink.BM_COMRESTIM) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.RESTIM -> 0");
        }

        if (((COM & Blink.BM_COMSRUN) == 0) & ((bits & Blink.BM_COMSRUN) == Blink.BM_COMSRUN)) {
            OZvm.displayRtmMessage(dzAddr + ": COM.BM_COMSRUN -> 1");
        }
        if (((COM & Blink.BM_COMSRUN) == Blink.BM_COMSRUN) & ((bits & Blink.BM_COMSRUN) == 0)) {
            OZvm.displayRtmMessage(dzAddr  + ": COM.BM_COMSRUN -> 0");
        }

        if (((COM & Blink.BM_COMSBIT) == 0) & ((bits & Blink.BM_COMSBIT) == Blink.BM_COMSBIT)) {
            //OZvm.displayRtmMessage(dzAddr + ": COM.BM_COMSBIT -> 1");
        }
        if (((COM & Blink.BM_COMSBIT) == Blink.BM_COMSBIT) & ((bits & Blink.BM_COMSBIT) == 0)) {
            //OZvm.displayRtmMessage(dzAddr + ": COM.BM_COMSBIT -> 0");
        }

        COM = bits;

        if ((COM & Blink.BM_COMRAMS) == BM_COMRAMS) {
            // Slot 0 RAM Bank 0x20 will be bound into lower 8K of segment 0
            RAMS = memory.getBank(0x20);
        } else {
            // Slot 0 ROM bank 0 is bound into lower 8K of segment 0
            RAMS = memory.getBank(0x00);
        }
    }

    /**
     * Get Blink Command Register flags.
     *
     * <PRE>
     *  Bit 7, SRUN
     *  Bit 6, SBIT
     *  Bit 5, OVERP
     *  Bit 4, RESTIM
     *  Bit 3, PROGRAM
     *  Bit 2, RAMS
     *  Bit 1, VPPON
     *  Bit 0, LCDON
     * </PRE>
     *
     * @return COM
     */
    public final int getCom() {
        return COM;
    }

    /**
     * Get Blink Eprom Programming Register, port $B3
     *
     * <PRE>
     *  Bit  7, PD1
     *  Bit  6, PD0
     *  Bit  5, PGMD
     *  Bit  4, EOED
     *  Bit  3, SE3D
     *  Bit  2, PGMP
     *  Bit  1, EOEP
     *  Bit  0, SE3P
     * </PRE>
     */
    public int getEpr() {
        return EPR;
    }

    /**
     * Set Blink Eprom Programming Register, port $B3
     *
     * <PRE>
     *  Bit  7, PD1
     *  Bit  6, PD0
     *  Bit  5, PGMD
     *  Bit  4, EOED
     *  Bit  3, SE3D
     *  Bit  2, PGMP
     *  Bit  1, EOEP
     *  Bit  0, SE3P
     * </PRE>
     */
    public void setEpr(int epr) {
        EPR = epr;
    }

    public void startRtcInterrupts() {
        rtc.start();
    }

    public void stopRtcInterrupts() {
        rtc.stop();
    }

    /**
     * The Z80Processor is set into snooze mode when INT.KWAIT is enabled and
     * the hardware keyboard matrix is scanned. Any interrupt (e.g. RTC, FLAP)
     * or a key press awakes the processor (or if the Z80 engine is stopped by
     * F5 or debug 'stop' command)
     *
     * This method is executed by the Z80Processor thread and is suspended while
     * waiting for an interrupt from the Blink (keyboard, HW or Rtc)
     */
    public synchronized void enableSnooze(long msTimeout) throws InterruptedException {
        snooze = true;
        while (snooze == true & z80.isZ80running() == true) {
            //OZvm.displayRtmMessage(Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()), true) + ": Z80 pause (by "+msTimeout+"ms timout");
            wait(msTimeout);
        }

        //OZvm.displayRtmMessage(Dz.extAddrToHex(decodeLocalAddress(z80.getInstrPC()), true) + ": Z80 activated");
    }

    public synchronized void awakeZ80() {
        if (snooze == true) {
            snooze = false;
            notifyAll();
        }
    }

    private void updateMhzMenu() {
        JMenu mhzMenu = OZvm.getInstance().getGui().getMhzMenu();
        String mhzStr = Integer.toString((int) (Z80.getZ80Mhz() * 100));

        switch(mhzStr.length()) {
            case 3: // x.yy Mhz
                mhzMenu.setText(mhzStr.charAt(0) + "." + mhzStr.substring(1) + "Mhz");
                break;
            case 4:
                // xx.yy Mhz
                mhzMenu.setText(mhzStr.substring(0, 2) + "." + mhzStr.substring(2) + "Mhz");
                break;
            case 5:
                // xxx.yy Mhz
                mhzMenu.setText(mhzStr.substring(0, 3) + "." + mhzStr.substring(3) + "Mhz");
                break;
        }
    }

    /**
     * Generic beeper for any Hz
     * @param hz number of times in 1sec sin function repeats
     * @param msecs
     */
    public void beep(int hz, int durationMs) {
        byte[] buf = new byte[2];

        if (sdl != null) {
            float numberOfSamplesToRepresentFullSin = (float) sampleFrequency / hz;
            for (int i = 0; i < durationMs * (float) sampleFrequency / 1000; i++) { // 1000 ms in 1 second
              double angle = i / (numberOfSamplesToRepresentFullSin/ 2.0) * Math.PI;  // /divide with 2 since sin goes 0PI to 2PI
              short a = (short) (Math.sin(angle) * 32767 * beepVolume);  //32767 - max value for sample to take (-32767 to 32767)
              buf[0] = (byte) (a & 0xFF); //write 8bits ________WWWWWWWW out of 16
              buf[1] = (byte) (a >> 8); //write 8bits WWWWWWWW________ out of 16
              sdl.write(buf, 0, 2);
            }
        }
    }

    /**
     * RTC, BLINK Real Time Clock, updated each 5ms, unless COM.RESTIM=1
     * Fires 10ms, 1s or 1min interrupts (defined by TMK & TSTA)
     * 3200Hz beeper every 10ms COMS.SRUN = 1
     */
    public final class Rtc {

        private TimerTask countRtc;
        private boolean rtcRunning; // Rtc counting?
        private ThreadManager beeperHelper;

        private Rtc() {
            rtcRunning = false;
            beeperHelper = new ThreadManager();
        }

        private final class Counter extends TimerTask {
            private int tickEvent;

            /**
             * If COMS.SRUN = 1, start doing 3200Hz beep for 10ms
             * write to sound queue out of 10ms timer, so we keep feeding regular 10ms TSTA interrupts
             */
            private void rtcBeeper() {
                beeperHelper.addTask(new Runnable() {
                    public void run() {
                        if (sdl != null && (COM & BM_COMSRUN) == BM_COMSRUN) {
                            if (sound3200Hz.length < sdl.available()) {
                                // protect buffer overflow, only write to it if there is space for the sound clip
                                sdl.write(sound3200Hz, 0, sound3200Hz.length);
                            }
                        }
                        if (sdl != null && (COM & BM_COMSRUN) == 0) {
                            if (silence3200Hz.length < sdl.available()) {
                                sdl.write(silence3200Hz, 0, silence3200Hz.length);
                            }
                        }
                    }
                });
            }

            /**
             * counts RTC TIM0-4 and sends 3200hz 10ms sound clips if COM.SRUN = 1
             */
            private void tickTock() {
                tickEvent = 0;

                setTim0( getTim0() + 1);
                if (getTim0() > 199) {
                    // When this counter reaches 200, wrap back to 0
                    setTim0(0);
                } else {
                    if ((getTim0() & 1) == 1) {
                        // a 10ms TSTA.TICK event as occurred (every 2nd 5ms count)
                        tickEvent = BM_TSTATICK;
                        
                        // the beeper is also managed via 10ms on or off sound clips
                        if ( OZvm.getInstance().get3200hzBeeperEmulation() == true )
                            rtcBeeper();
                    }

                    if (getTim0() == 128) {
                        // According to blink dump monitoring on Z88, when TIM0 reaches 0x80 (bit 7), a second has passed
                        setTim1( getTim1() + 1 );
                        tickEvent = BM_TSTASEC;

                        if (getTim1() > 59) {
                            setTim1(0);

                            setTim2( getTim2() + 1);
                            if (getTim2() > 255) {
                                setTim2(0); // 256 minutes has passed

                                ++TIM3;
                                if (TIM3 > 255) {
                                    TIM3 = 0; // 65536 minutes has passed

                                    ++TIM4;
                                    if (TIM4 > 31) {
                                        TIM4 = 0; // 65536 * 32 minutes has passed
                                    }
                                }
                            }

                        }

                        if (getTim1() == 32) {
                            // 1 minute has passed
                            tickEvent = BM_TSTAMIN;
                        }
                        updateMhzMenu();
                    }
                }

                //String msg = "(" +System.currentTimeMillis() + ") Blink TickTock Event = " + Dz.byteToBin(tickEvent, true) + " (TIM0=" + getTim0() + ",TIM1=" + getTim1() + ",TIM2=" + getTim2() +")";
                //OZvm.displayRtmMessage(msg);
            }

            public Counter() {
                tickEvent = 0;
            }

            /**
             * Execute the RTC counter each 5ms, and set the various RTC
             * interrupts if they are enabled, but only if INT.TIME = 1.
             *
             * @see java.lang.Runnable#run()
             */
            public void run() {
                if ((getCom() & Blink.BM_COMRESTIM) == Blink.BM_COMRESTIM) {
                    // Hold Real Time Clock in reset - TIM0-4 = 0, no 5ms counting (RESTIM = 1)
                    tickEvent = 0;
                    resetTimx();
                    return;
                }

                if (z80.isZ80ThreadRunning() == false) {
                    // Don't update RTC registers when we're in debugging mode
                    return;
                }

                // RTC is not held in reset and threads are running.
                // Update TIM0-TIM4 (a 10ms Tick, a Second or a Minute)
                tickTock();

                if (((INT & BM_INTGINT) == 0)) {
                    // No interrupts get out of Blink
                    return;
                }

                if ( (getSta() & BM_STAFLAPOPEN) == BM_STAFLAPOPEN ) {
                    // Flap is Open
                    // (on real hardware, there is no interrupt - in OZvm it's a trick to get LCD switched off properly by the ROM)
                    // (guarantee there is no STA.TIME event)
                    setSta( getSta() & ~BM_STATIME);
                    if (getTim0() % 3 == 0) {
                        // only signal the interrupt line 3rd TIM0 count
                        awakeZ80();
                    }
                    return;
                }

                if ( (INT & BM_INTTIME) == 0) {
                    // INT.TIME is not enabled (no RTC interrupts)
                    // No interrupt is fired
                    setSta( getSta() & ~BM_STATIME);
                    return;
                }

                if ( getTmk() == 0 ) {
                    // No time event (a TMK.TICK, TMK.SEC or TMK.MIN) is enabled (not allowed to happen)
                    // No interrupt is fired
                    setSta( getSta() & ~BM_STATIME);
                    return;
                }

                if ( (tickEvent != 0)) {
                    // always signal what RTC event happened in Blink
                    setTsta(tickEvent);

                    if ( ((getTmk() & tickEvent) == tickEvent) ) {
                        // only fire the interrupt that TMK is allowing to come out
                        setSta( getSta() | BM_STATIME);

                        //String msg = "Blink RTC: tickEvent=" + tickEvent + " TMK=" + Dz.byteToBin(getTmk(), true) + ", TSTA=" + Dz.byteToBin(getTsta(), true) + ", STA=" + Dz.byteToBin(getSta(), true) + " (TIM0=" + getTim0() + ",TIM1=" + getTim1() + ",TIM2=" + getTim2() +")";
                        //OZvm.displayRtmMessage(msg);

                        awakeZ80();
                    }
                }
            }
        }

        /**
         * Stop the Rtc counter, but don't reset the counters themselves.
         */
        public void stop() {
            if (countRtc != null) {
                countRtc.cancel();
            }
            rtcRunning = false;
        }

        /**
         * Start the Rtc counter immediately, and execute the run() method every
         * 5 millisecond.
         */
        public void start() {
            if (rtcRunning == false) {
                rtcRunning = true;
                countRtc = new Counter();
                rtcTimer.scheduleAtFixedRate(countRtc, 0, 5);
            }
        }

        /**
         * Is the RTC running?
         *
         * @return boolean
         */
        public boolean isRunning() {
            return rtcRunning;
        }
    }


    /**
     * @return Returns the current RAMS bank binding (Bank 00/ROM or Bank
     * 20h/RAM).
     */
    public Bank getRAMS() {
        return RAMS;
    }

    /**
     * @param rams Define the current Bank binding for RAMS (Bank 00/ROM or Bank
     * 20h/RAM)
     */
    public void setRAMS(Bank rams) {
        RAMS = rams;
    }

    public void signalBattLow() {
        setSta( getSta() | BM_STABTL);
        awakeZ80();
        //OZvm.displayRtmMessage("Blink Int STA.BTL -> 1");
    }

    /**
     * The Blink only fires an IM 1 interrupt when the flap is opened and when
     * INT.FLAP is enabled. Both STA.FLAPOPEN and STA.FLAP is set at the time of
     * the interrupt. As long as the flap is open, no STA.TIME interrupts gets
     * out of the Blink (even though INT.TIME may be enabled and signals it to
     * fire those RTC interrupts). The Flap interrupt is only fired once; when
     * the flap is closed, and then opened. STA.FLAPOPEN remains enabled as long
     * as the flap is open; when the flap is closed, NO interrupt is fired -
     * only STA.FLAPOPEN is set to 0.
     */
    public void signalFlapOpened() {
        if (((INT & BM_INTFLAP) == BM_INTFLAP) & ((INT & BM_INTGINT) == BM_INTGINT)) {
            // when flap is opened, time int's no longer come out..
            setSta( getSta() | BM_STAFLAP);
            awakeZ80();
            setSta( getSta() | BM_STAFLAPOPEN);
            OZvm.displayRtmMessage("Blink Int STA.FLAPOPEN -> 1");
        }
    }

    /**
     * Signal an NMI to Z80 CPU (Not used currently)
     */
    public void signalNmi() {
        //z80.setNmiSignal();
        //awakeZ80();
    }

    /**
     * Signal that the flap was closed.<p> The Blink will start to fire STA.TIME
     * interrupts again if the INT.TIME is enabled and TMK has been setup to
     * fire Minute, Second or TICK's.
     *
     * This is not an interrupt (but Z80 goes out of snooze), only the STA.FLAPOPEN bit set to 0
     */
    public void signalFlapClosed() {
        setAck(BM_STAFLAPOPEN);
        awakeZ80();
        //OZvm.displayRtmMessage("Blink STA.FLAPOPEN -> 0");
    }

    /**
     * return true, if Flap is open, otherwise false
     */
    public boolean isFlapOpen() {
        if ((getSta() & BM_STAFLAPOPEN) == BM_STAFLAPOPEN)
            return true;
        else
            return false;
    }
}
