/*
 * MX29F040Bank.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

/**
 * This class represents the 16Kb Generic Flash Memory Bank on a Macronix
 * MX29F040C 512K chip, which is compatible with AMD 29FxxxB series chips.
 * The package format only exists for SOP sockets (ie. in PCB for external memory cards).
 *
 * The chip only exists in PLCC and TSOP format and so is not able to be used in
 * slot 0 socket. The chip may be used as part of and external memory card.
 *
 * The characteristics of a Flash Memory bank is chip memory that can be read at
 * all times and only be written (and erased) using a combination of MX Flash
 * command sequences (write byte to address cycles), in ALL available slots on
 * the Z88.
 *
 * The emulation of the Macronix Flash Memory inherits the AMD implementation of the
 * chip. Please refer to GenericAmdFlashBank class for more information.
 */
public class MX29F040Bank extends GenericAmdFlashBank {

    /**
     * Device Code for 512Kb memory, 8 x 64K erasable sectors, 32 x 16K banks
     */
    public static final int DEVICECODE = 0xA4;

    /**
     * Manufacturer Code for MX29F040C Flash Memory chip
     */
    public static final int MANUFACTURERCODE = 0xC2;

    /**
     * The actual Flash Memory Device Size in 16K banks (512K)
     */
    public static final int TOTALCARDBANKS = 32;

    /**
     * Constructor. Assign the Flash Memory bank to the 4Mb memory model.
     */
    public MX29F040Bank() {
        super();

    }

    /**
     * @return the Flash Memory Device Code (MX29F040C)
     * which this bank is part of.
     */
    public final int getDeviceCode() {
        return DEVICECODE;
    }

    /**
     * @return the Flash Memory Manufacturer Code
     */
    public final int getManufacturerCode() {
        return MANUFACTURERCODE;
    }

    /**
     * Return the actual Flash Memory Device Size in 16K banks
     */
    public final int getDeviceSize() {
        return TOTALCARDBANKS;
    }

    /**
     * Erase the 64K Flash Memory Sector which this bank is part of
     * (always succeeds in emulation).
     *
     * @param addr the bank offset where the erase sector command is targeted
     */
    public void eraseSector(int addr) {
        // erase the bottom bank of the current sector and the next three following banks
        // (the address is not used in this case, we're already in the bank)
        int bottomBankOfSector = getBankNumber() & 0xFC;  // bottom bank of sector

        for (int thisBank = bottomBankOfSector; thisBank <= (bottomBankOfSector + 3); thisBank++) {
            eraseBank(thisBank);
        }

        // finalize internal chip mode after sector erasure.
        super.eraseSector(addr);
    }
}
