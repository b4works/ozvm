/*
 * Gui.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2024
 *
 */
package com.gitlab.z88.ozvm;

import com.gitlab.z88.ozvm.datastructures.SlotInfo;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;

/**
 * The end user Gui (Main menu, screen, runtime messages, keyboard & slot
 * management)
 */
public class Gui extends JFrame {

    private static final String aboutDialogText =
            "<html><center>"
            + "<h2>OZvm " + OzvmVersion.release + " rev. " + GitRevision.commit + "</h2>"
            + "<h3>The Z88 emulator & debugging environment</h3>"
            + "GPL v2 licensed software<br>"
            + "Copyright (C) 2000-2024 Gunther Strube, <tt>hello@bits4fun.net</tt><br><br>"
            + "<tt>https://gitlab.com/z88/ozvm</tt>"
            + "</center>"
            + "<br><hr><br>"
            + "<table align='center' cellspacing='0' border='0'>"
            + "<tr><td>Java:</td><td>"+ System.getProperty("java.vendor") + " " + System.getProperty("java.version") + "; " + System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version") + "</td></tr>"
            + "<tr><td>Runtime:</td><td>" + System.getProperty("java.runtime.name") + " " + System.getProperty("java.runtime.version") + "</td></tr>"
            + "<tr><td>System:</td><td>" + System.getProperty("os.name") + " V" + System.getProperty("os.version") + " on " + System.getProperty("os.arch") + "</td></tr>"
            + "</table>"
            + "</html>";
    private Z88BodyGui zb;

    private Blink blink;
    private ButtonGroup kbLayoutButtonGroup;
    private ButtonGroup scrRefreshRateButtonGroup;
    private ButtonGroup scrResolutionButtonGroup;
    private JToolBar toolBar;
    private JButton toolBarButton1;
    private JButton toolBarButton2;
    private Z88display z88Display;
    private Z80Processor z80proc;
    private JPanel z88ScreenPanel;
    private RubberKeyboard keyboardPanel;
    private Slots slotsPanel;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu helpMenu;
    private JMenu mHzMenu;
    private JMenu msgMenu;
    private JMenu viewMenu;
    private JMenu z88Menu;
    private JMenu screenMenu;
    private JMenu keyboardMenu;
    private JMenu screenResolutionMenu;
    private JMenu screenRefreshRateMenu;
    private JMenu _512KRomTypeMenu;
    private JCheckBoxMenuItem amd512KRomTypeMenuItem;
    private JCheckBoxMenuItem stm512KRomTypeMenuItem;
    private JCheckBoxMenuItem sst512KRomTypeMenuItem;
    private ButtonGroup _512KRomTypeButtonGroup;
    private JMenu installRomMenuItem;
    private JMenuItem fileExitMenuItem;
    private JMenuItem debugCmdlineMenuItem;
    private JMenuItem aboutOZvmMenuItem;
    private JMenuItem createSnapshotMenuItem;
    private JMenuItem loadSnapshotMenuItem;
    private JMenuItem softResetMenuItem;
    private JMenuItem hardResetMenuItem;
    private JMenuItem installOz43RomMenuItem;  // International OZ V4.3.1 ROM menu
    private JMenuItem installOz44RomMenuItem;  // International OZ V4.4 ROM menu
    private JMenuItem installOz45RomMenuItem;  // International OZ V4.5 ROM menu
    private JMenuItem installOz46RomMenuItem;  // International OZ V4.6 ROM menu
    private JMenuItem installOz47RomMenuItem;  // International OZ V4.7 ROM menu
    private JMenuItem installUk400RomMenuItem; // British V4.0 ROM menu
    private JMenuItem installUk300RomMenuItem; // British V3.0 ROM menu
    private JMenuItem installUk220RomMenuItem; // British V2.2 ROM menu
    private JMenuItem installFr326RomMenuItem; // French V3.26 ROM menu
    private JMenuItem installEs319RomMenuItem; // Spanish V3.19 ROM menu
    private JMenuItem installDe318RomMenuItem; // German V3.18 ROM menu
    private JMenuItem installIt323RomMenuItem; // Italian V3.23 ROM menu
    private JMenuItem installDk321RomMenuItem; // Danish V3.21 ROM menu
    private JMenuItem installSe250RomMenuItem; // Swedish V2.50 ROM menu
    private JMenuItem installNo260RomMenuItem; // Norwegian V2.60 ROM menu
    private JMenuItem installFi401RomMenuItem; // Finnish V4.01 ROM menu
    private JMenuItem installHe313RomMenuItem; // Swizz V3.13 ROM menu
    private JMenuItem installTk317RomMenuItem; // Turkish V3.17 ROM menu
    private JMenuItem userManualMenuItem;
    private JMenuItem z88UserGuideMenuItem;
    private JMenuItem z88DevNotesMenuItem;
    private JMenuItem gifMovieMenuItem;
    private JMenuItem screenSnapshotMenuItem;
    private JMenuItem viewMemoryMenuItem;
    private JMenuItem battLowIntMenuItem;
    private JMenuItem shiftKeysMenuItem;
    private JMenuItem clearKbMatrixMenuItem;
    private JMenuItem projectWebMenuItem;
    private JCheckBoxMenuItem screenDoubleSizeMenuItem;
    private JCheckBoxMenuItem rtmMessagesMenuItem;
    private JCheckBoxMenuItem seLayoutMenuItem;
    private JCheckBoxMenuItem frLayoutMenuItem;
    private JCheckBoxMenuItem dkLayoutMenuItem;
    private JCheckBoxMenuItem ukLayoutMenuItem;
    private JCheckBoxMenuItem showRubberKbMenuItem;
    private JCheckBoxMenuItem screen640x64MenuItem;
    private JCheckBoxMenuItem screen640x320MenuItem;
    private JCheckBoxMenuItem screen640x480MenuItem;
    private JCheckBoxMenuItem screen800x320MenuItem;
    private JCheckBoxMenuItem screen800x480MenuItem;
    private JCheckBoxMenuItem scr10FpsMenuItem;
    private JCheckBoxMenuItem scr25FpsMenuItem;
    private JCheckBoxMenuItem scr50FpsMenuItem;
    private JCheckBoxMenuItem scr100FpsMenuItem;
    private JCheckBoxMenuItem z80CpuRealSpeedMenuItem;
    private JCheckBoxMenuItem showSlotsMenuItem;
    private JCheckBoxMenuItem beeper3200hzMenuItem;

    /**
     * Default Window mode Gui constructor
     */
    public Gui() {
        super();

        initialize();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JPanel getZ88ScreenPanel() {
        Dimension dim;
        if (z88ScreenPanel == null) {
            z88ScreenPanel = new JPanel();
            dim = getZ88Display().getPreferredSize();

            // adjust
            dim.setSize(dim.getWidth()+4, dim.getHeight()+8);
            z88ScreenPanel.setPreferredSize(dim);

            z88ScreenPanel.setBackground(Color.GRAY);
            z88ScreenPanel.add(getZ88Display());
        }

        return z88ScreenPanel;
    }

    private Z88display getZ88Display() {
        if (z88Display == null) {
            z88Display = Z88.getInstance().getDisplay();
            z88Display.setLayout(null);
            z88Display.setForeground(Color.WHITE);
            z88Display.setText("This is the Z88 Screen");
        }
        return z88Display;
    }

    private JToolBar getToolBar() {
        if (toolBar == null) {
            toolBar = new JToolBar();
            toolBar.add(getToolBarButton1());
            toolBar.add(getToolBarButton2());
            toolBar.setVisible(false);
        }
        return toolBar;
    }

    private JButton getToolBarButton1() {
        if (toolBarButton1 == null) {
            toolBarButton1 = new JButton();
            toolBarButton1.setText("New JButton");
        }
        return toolBarButton1;
    }

    private JButton getToolBarButton2() {
        if (toolBarButton2 == null) {
            toolBarButton2 = new JButton();
            toolBarButton2.setText("New JButton");
        }
        return toolBarButton2;
    }

    /**
     * This method initializes main Help Menu dropdown
     *
     * @return javax.swing.JMenu
     */
    private javax.swing.JMenu getHelpMenu() {
        if (helpMenu == null) {
            helpMenu = new javax.swing.JMenu();
            helpMenu.setText("Help");

            helpMenu.add(getUserManualMenuItem());
            helpMenu.add(getProjectWebMenuItem());
            helpMenu.add(getZ88UserGuideMenuItem());
            helpMenu.add(getZ88DevNotesMenuItem());
            helpMenu.add(getAboutOZvmMenuItem());
        }

        return helpMenu;
    }

    /**
     * This method initializes Z80 Mhz Menu Item
     *
     * @return javax.swing.JMenu
     */
    public javax.swing.JMenu getMhzMenu() {
        if (mHzMenu == null) {
            mHzMenu = new javax.swing.JMenu();
            mHzMenu.setText(z80proc.getZ80Mhz() + "Mhz");
        }

        return mHzMenu;
    }

    /**
     * This method initializes Z80 Mhz Menu Item
     *
     * @return javax.swing.JMenu
     */
    public javax.swing.JMenu getMessageMenu() {
        if (msgMenu == null) {
            msgMenu = new javax.swing.JMenu();
            msgMenu.setText("");
        }

        return msgMenu;
    }

    public void launchBrowserUrl(String url) {
        final String errMsg = "Could not launch URL in Desktop Browser.";
        String os = System.getProperty("os.name").toLowerCase();
        Runtime rt = Runtime.getRuntime();

        try {
            if (os.indexOf("win") >= 0) {

                // this doesn't support showing urls in the form of "page.html#nameLink"
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);

            } else if (os.indexOf("mac") >= 0) {

                rt.exec("open " + url);

            } else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {

                // Do a best guess on unix until we get a platform independent way
                // Build a list of browsers to try, in this order.
                String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
                    "netscape", "opera", "links", "lynx"};

                // Build a command string which looks like "browser1 "url" || browser2 "url" ||..."
                StringBuffer cmd = new StringBuffer();
                for (int i = 0; i < browsers.length; i++) {
                    cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \"" + url + "\" ");
                }

                rt.exec(new String[]{"sh", "-c", cmd.toString()});

            }

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(Gui.this, errMsg, "Online Help", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex2) {
            JOptionPane.showMessageDialog(Gui.this, errMsg, "Online Help", JOptionPane.ERROR_MESSAGE);
        }

    }

    private JMenuItem getUserManualMenuItem() {

        if (userManualMenuItem == null) {
            userManualMenuItem = new JMenuItem();
            userManualMenuItem.setText("OZvm User Guide");

            userManualMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    launchBrowserUrl("https://gitlab.com/z88/ozvm/-/wikis/");
                }
            });
        }

        return userManualMenuItem;
    }

    private JMenuItem getZ88UserGuideMenuItem() {

        if (z88UserGuideMenuItem == null) {
            z88UserGuideMenuItem = new JMenuItem();
            z88UserGuideMenuItem.setText("Z88 User Guide");

            z88UserGuideMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    launchBrowserUrl("https://cambridgez88.jira.com/wiki/spaces/UG/");
                }
            });
        }

        return z88UserGuideMenuItem;
    }

    private JMenuItem getZ88DevNotesMenuItem() {

        if (z88DevNotesMenuItem == null) {
            z88DevNotesMenuItem = new JMenuItem();
            z88DevNotesMenuItem.setText("Z88 Developers' Notes");

            z88DevNotesMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    launchBrowserUrl("https://cambridgez88.jira.com/wiki/x/NQAE");
                }
            });
        }

        return z88DevNotesMenuItem;
    }

    private JMenuItem getProjectWebMenuItem() {

        if (projectWebMenuItem == null) {
            projectWebMenuItem = new JMenuItem();
            projectWebMenuItem.setText("OZvm Gitlab Project");

            projectWebMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    launchBrowserUrl("https://gitlab.com/z88/ozvm");
                }
            });
        }

        return projectWebMenuItem;
    }

    private JMenuItem getAboutOZvmMenuItem() {
        if (aboutOZvmMenuItem == null) {
            aboutOZvmMenuItem = new JMenuItem();
            aboutOZvmMenuItem.setMnemonic(KeyEvent.VK_A);
            aboutOZvmMenuItem.setText("About");
            aboutOZvmMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(Gui.this, aboutDialogText, "About OZvm", JOptionPane.PLAIN_MESSAGE);
                }
            });
        }
        return aboutOZvmMenuItem;
    }

    private JMenuBar getMainMenuBar() {
        if (menuBar == null) {
            menuBar = new JMenuBar();
            menuBar.setBorder(new EmptyBorder(0, 0, 0, 0));
            menuBar.add(getFileMenu());
            menuBar.add(getZ88Menu());
            menuBar.add(getKeyboardMenu());
            menuBar.add(getViewMenu());
            menuBar.add(getHelpMenu());
            menuBar.add(Box.createHorizontalGlue());
            menuBar.add(getMessageMenu());
            menuBar.add(getMhzMenu());
        }

        return menuBar;
    }

    private JMenu getFileMenu() {
        if (fileMenu == null) {
            fileMenu = new JMenu();
            fileMenu.setText("File");

            fileMenu.add(getLoadSnapshotMenuItem());
            fileMenu.add(getCreateSnapshotMenuItem());
            fileMenu.add(getCreateScreenMenu());

            fileMenu.addSeparator();
            fileMenu.add(getFileExitMenuItem());
        }

        return fileMenu;
    }

    private JMenuItem getDebugCmdlineMenuItem() {
        if (debugCmdlineMenuItem == null) {
            debugCmdlineMenuItem = new JMenuItem();
            debugCmdlineMenuItem.setMnemonic(KeyEvent.VK_D);
            debugCmdlineMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
            debugCmdlineMenuItem.setText("Debug Command Line");

            debugCmdlineMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    DebugGui.getInstance().refreshZ88HardwareInfo();
                    DebugGui.getInstance().activateDebugCommandLine();
                    if (Z88.getInstance().getProcessor().isZ80ThreadRunning() == true) {
                        DebugGui.getInstance().consoleOutput("Z88 is currently running.");
                    } else {
                        DebugGui.getInstance().lockZ88MachinePanel(true);
                    }
                }
            });
        }

        return debugCmdlineMenuItem;
    }

    private JMenuItem getFileExitMenuItem() {
        if (fileExitMenuItem == null) {
            fileExitMenuItem = new JMenuItem();
            fileExitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.CTRL_MASK));
            fileExitMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
            fileExitMenuItem.setMnemonic(KeyEvent.VK_E);
            fileExitMenuItem.setText("Exit");
        }

        return fileExitMenuItem;
    }

    private JMenuItem getBattLowIntMenuItem() {
        if (battLowIntMenuItem == null) {
            battLowIntMenuItem = new JMenuItem();
            battLowIntMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    blink.signalBattLow();
                }
            });
            battLowIntMenuItem.setText("Indicate Battery Low");
        }

        return battLowIntMenuItem;
    }

    private JMenu getViewMenu() {
        if (viewMenu == null) {
            viewMenu = new JMenu();
            viewMenu.setText("View");
            viewMenu.add(getRtmMessagesMenuItem());
            viewMenu.add(getViewMemoryMenuItem());
        }

        return viewMenu;
    }

    public void displayZ88ScreenPanel(boolean display) {
        if (display == true) {
            getContentPane().remove(getZ88ScreenPanel());
            addZ88ScreenPanel();

            getZ88Display().grabFocus();
        } else {
            getContentPane().remove(getZ88ScreenPanel());
            z88ScreenPanel = null;
        }
    }

    public void displayRunTimeMessagesPane(boolean display) {
        OZvm.getInstance().getRtmMsgGui().setVisible(display);
        getZ88Display().grabFocus();
    }

    public void displayZ88Keyboard(boolean display) {
        if (display == true) {
            getContentPane().remove(getKeyboardPanel());

            addKeyboardPanel();
        } else {
            // in full screen mode, the keyboard cannot be removed
            getContentPane().remove(getKeyboardPanel());
        }

        getZ88Display().grabFocus();
    }

    public JCheckBoxMenuItem getRtmMessagesMenuItem() {
        if (rtmMessagesMenuItem == null) {
            rtmMessagesMenuItem = new JCheckBoxMenuItem();
            rtmMessagesMenuItem.setSelected(false);
            rtmMessagesMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, ActionEvent.CTRL_MASK));
            rtmMessagesMenuItem.setText("Runtime Messages");
            rtmMessagesMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    displayRunTimeMessagesPane(rtmMessagesMenuItem.isSelected());
                    Gui.this.pack();
                }
            });
        }

        return rtmMessagesMenuItem;
    }

    public JCheckBoxMenuItem getZ80CpuRealSpeedMenuItem() {
        if (z80CpuRealSpeedMenuItem == null) {
            z80CpuRealSpeedMenuItem = new JCheckBoxMenuItem();
            z80CpuRealSpeedMenuItem.setSelected(z80proc.isCpuRealSpeed());
            z80CpuRealSpeedMenuItem.setText("Z80 CPU 3.2768Mhz");
            z80CpuRealSpeedMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    z80proc.setCpuRealSpeed(z80CpuRealSpeedMenuItem.isSelected());
                }
            });
        }

        return z80CpuRealSpeedMenuItem;
    }

    public JCheckBoxMenuItem getShowSlotsMenuItem() {
        if (showSlotsMenuItem == null) {
            showSlotsMenuItem = new JCheckBoxMenuItem();
            showSlotsMenuItem.setSelected(OZvm.getInstance().getDisplayZ88CardSlots());
            showSlotsMenuItem.setText("Show Memory Card Slots");
            showSlotsMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    OZvm.getInstance().setDisplayZ88CardSlots(showSlotsMenuItem.isSelected());
                    getSlotsPanel().setVisible(showSlotsMenuItem.isSelected());
                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                }
            });
        }

        return showSlotsMenuItem;
    }

    public JCheckBoxMenuItem getBeeper3200hzMenuItem() {
        if (beeper3200hzMenuItem == null) {
            beeper3200hzMenuItem = new JCheckBoxMenuItem();
            beeper3200hzMenuItem.setSelected(OZvm.getInstance().get3200hzBeeperEmulation());
            beeper3200hzMenuItem.setText("3200Hz Beeper Emulation");
            beeper3200hzMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    OZvm.getInstance().set3200hzBeeperEmulation(beeper3200hzMenuItem.isSelected());
                }
            });
        }

        return beeper3200hzMenuItem;
    }

    public JMenuItem getViewMemoryMenuItem() {
        if (viewMemoryMenuItem == null) {
            viewMemoryMenuItem = new JMenuItem();
            viewMemoryMenuItem.setSelected(false);
            viewMemoryMenuItem.setText("View/edit memory");
            viewMemoryMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String input = JOptionPane.showInputDialog(
                            null,
                            "Enter extended address (eg. 073fc0): ",
                            "View/Edit Z88 Memory",
                            JOptionPane.QUESTION_MESSAGE);
                    int extAddress = Dz.stringAddr2Integer(input);
                    int bankNo = (extAddress >>> 16) & 0xFF;
                    int offset = extAddress & 0x3FFF;

                    Z88.getInstance().getMemory().getBank(bankNo).editMemory(offset, bankNo);
                }
            });
        }

        return viewMemoryMenuItem;
    }

    private void addZ88ScreenPanel() {
        getContentPane().add(getZ88ScreenPanel());
    }

    private void addKeyboardPanel() {
        if (getKeyboardPanel().isVisible()) {
            getContentPane().add(getKeyboardPanel());
        }
    }

    private RubberKeyboard getKeyboardPanel() {
        if (keyboardPanel == null) {
            keyboardPanel = RubberKeyboard.getInstance();
            keyboardPanel.setVisible(OZvm.getInstance().getDisplayRubberKeyboard());
            keyboardPanel.setKeyboardCountrySpecificIcons(Z88.getInstance().getKeyboard().getKeyboardLayout());
        }

        return keyboardPanel;
    }

    private JMenu getKeyboardMenu() {
        if (keyboardMenu == null) {
            keyboardMenu = new JMenu();
            keyboardMenu.setText("Keyboard");
            keyboardMenu.add(getShiftKeysMenuItem());
            keyboardMenu.add(getClearKbMatrixMenuItem());
            keyboardMenu.add(getUkLayoutMenuItem());
            keyboardMenu.add(getDkLayoutMenuItem());
            keyboardMenu.add(getFrLayoutMenuItem());
            keyboardMenu.add(getSeLayoutMenuItem());
            keyboardMenu.add(getShowRubberKbMenuItem());

        }
        return keyboardMenu;
    }

    public Slots getSlotsPanel() {
        if (slotsPanel == null) {
            slotsPanel = new Slots();
        }

        return slotsPanel;
    }

    private JMenuItem getShiftKeysMenuItem() {
        if (shiftKeysMenuItem == null) {
            shiftKeysMenuItem = new JMenuItem();
            shiftKeysMenuItem.setText("Press both SHIFT keys");
            shiftKeysMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));

            shiftKeysMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().typeShiftKeys();
                }
            });
        }

        return shiftKeysMenuItem;
    }

    private JMenuItem getClearKbMatrixMenuItem() {
        if (clearKbMatrixMenuItem == null) {
            clearKbMatrixMenuItem = new JMenuItem();
            clearKbMatrixMenuItem.setText("Reset Z88 Keyboard Matrix");

            clearKbMatrixMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().resetKeyboardMatrix();
                }
            });
        }

        return clearKbMatrixMenuItem;
    }


    public JCheckBoxMenuItem getShowRubberKbMenuItem() {
        if (showRubberKbMenuItem == null) {
            showRubberKbMenuItem = new JCheckBoxMenuItem();
            showRubberKbMenuItem.setText("Show Rubber Keyboard");
            showRubberKbMenuItem.setState(OZvm.getInstance().getDisplayRubberKeyboard());
            showRubberKbMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    OZvm.getInstance().setDisplayRubberKeyboard(showRubberKbMenuItem.isSelected());
                    getKeyboardPanel().setVisible(showRubberKbMenuItem.isSelected());
                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                }
            });

        }

        return showRubberKbMenuItem;
    }

    public JCheckBoxMenuItem getUkLayoutMenuItem() {
        if (ukLayoutMenuItem == null) {
            ukLayoutMenuItem = new JCheckBoxMenuItem();
            ukLayoutMenuItem.setText("English Layout");
            ukLayoutMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_UK);
                    getZ88Display().grabFocus();
                }
            });

            kbLayoutButtonGroup.add(ukLayoutMenuItem);
        }
        return ukLayoutMenuItem;
    }

    public JCheckBoxMenuItem getDkLayoutMenuItem() {
        if (dkLayoutMenuItem == null) {
            dkLayoutMenuItem = new JCheckBoxMenuItem();
            dkLayoutMenuItem.setText("Danish/Norwegian Layout");
            dkLayoutMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_DK);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_DK);
                    getZ88Display().grabFocus();
                }
            });

            kbLayoutButtonGroup.add(dkLayoutMenuItem);
        }
        return dkLayoutMenuItem;
    }

    public JCheckBoxMenuItem getFrLayoutMenuItem() {
        if (frLayoutMenuItem == null) {
            frLayoutMenuItem = new JCheckBoxMenuItem();
            frLayoutMenuItem.setText("French Layout");
            frLayoutMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_FR);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_FR);
                    getZ88Display().grabFocus();
                }
            });

            kbLayoutButtonGroup.add(frLayoutMenuItem);
        }
        return frLayoutMenuItem;
    }

    public JCheckBoxMenuItem getScreen640x64MenuItem() {
        if (screen640x64MenuItem == null) {
            screen640x64MenuItem = new JCheckBoxMenuItem();
            screen640x64MenuItem.setText("640 x 64 pixels");
            screen640x64MenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // The Blink is preset with the LCD resolution
                    blink.setSCW(640/8);
                    blink.setSCH(64/8);
                    getZ88Display().setBlinkLcdResolution();
                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                    // then a hard reset so the system software can read the LCD dimension during boot
                    Z88.getInstance().hardReset();
                }
            });

            scrResolutionButtonGroup.add(screen640x64MenuItem);
        }

        return screen640x64MenuItem;
    }

    public JCheckBoxMenuItem getScreen640x320MenuItem() {
        if (screen640x320MenuItem == null) {
            screen640x320MenuItem = new JCheckBoxMenuItem();
            screen640x320MenuItem.setText("640 x 320 pixels");
            screen640x320MenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // The Blink is preset with the LCD resolution
                    blink.setSCW(640/8);
                    blink.setSCH(320/8);
                    getZ88Display().setBlinkLcdResolution();

                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                    // then a hard reset so the system software can read the LCD dimension during boot
                    Z88.getInstance().hardReset();
                }
            });

            scrResolutionButtonGroup.add(screen640x320MenuItem);
        }

        return screen640x320MenuItem;
    }

    public JCheckBoxMenuItem getScreen640x480MenuItem() {
        if (screen640x480MenuItem == null) {
            screen640x480MenuItem = new JCheckBoxMenuItem();
            screen640x480MenuItem.setText("640 x 480 pixels");
            screen640x480MenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // The Blink is preset with the LCD resolution
                    blink.setSCW(640/8);
                    blink.setSCH(480/8);
                    getZ88Display().setBlinkLcdResolution();

                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                    // then a hard reset so the system software can read the LCD dimension during boot
                    Z88.getInstance().hardReset();
                }
            });

            scrResolutionButtonGroup.add(screen640x480MenuItem);
        }

        return screen640x480MenuItem;
    }

    public JCheckBoxMenuItem getScreen800x320MenuItem() {
        if (screen800x320MenuItem == null) {
            screen800x320MenuItem = new JCheckBoxMenuItem();
            screen800x320MenuItem.setText("800 x 320 pixels");
            screen800x320MenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // The Blink is preset with the LCD resolution
                    blink.setSCW(800/8);
                    blink.setSCH(320/8);
                    getZ88Display().setBlinkLcdResolution();

                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                    // then a hard reset so the system software can read the LCD dimension during boot
                    Z88.getInstance().hardReset();
                }
            });

            scrResolutionButtonGroup.add(screen800x320MenuItem);
        }

        return screen800x320MenuItem;
    }

    public JCheckBoxMenuItem getScreen800x480MenuItem() {
        if (screen800x480MenuItem == null) {
            screen800x480MenuItem = new JCheckBoxMenuItem();
            screen800x480MenuItem.setText("800 x 480 pixels");
            screen800x480MenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // The Blink is preset with the LCD resolution
                    blink.setSCW(800/8);
                    blink.setSCH(480/8);
                    getZ88Display().setBlinkLcdResolution();

                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                    // then a hard reset so the system software can read the LCD dimension during boot
                    Z88.getInstance().hardReset();
                }
            });

            scrResolutionButtonGroup.add(screen800x480MenuItem);
        }

        return screen800x480MenuItem;
    }

    public JCheckBoxMenuItem getScreen10FpsMenuItem() {
        if (scr10FpsMenuItem == null) {
            scr10FpsMenuItem = new JCheckBoxMenuItem();
            scr10FpsMenuItem.setText("10 Frames Per Second");
            scr10FpsMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    getZ88Display().setFrameRate(Z88display.FPS10);
                    getZ88Display().grabFocus();
                }
            });

            scrRefreshRateButtonGroup.add(scr10FpsMenuItem);
        }

        return scr10FpsMenuItem;
    }

    public JCheckBoxMenuItem getScreen25FpsMenuItem() {
        if (scr25FpsMenuItem == null) {
            scr25FpsMenuItem = new JCheckBoxMenuItem();
            scr25FpsMenuItem.setText("25 Frames Per Second");
            scr25FpsMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    getZ88Display().setFrameRate(Z88display.FPS25);
                    getZ88Display().grabFocus();
                }
            });

            scrRefreshRateButtonGroup.add(scr25FpsMenuItem);
        }

        return scr25FpsMenuItem;
    }

    public JCheckBoxMenuItem getScreen50FpsMenuItem() {
        if (scr50FpsMenuItem == null) {
            scr50FpsMenuItem = new JCheckBoxMenuItem();
            scr50FpsMenuItem.setText("50 Frames Per Second");
            scr50FpsMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    getZ88Display().setFrameRate(Z88display.FPS50);
                    getZ88Display().grabFocus();
                }
            });

            scrRefreshRateButtonGroup.add(scr50FpsMenuItem);
        }

        return scr50FpsMenuItem;
    }

    public JCheckBoxMenuItem getScreen100FpsMenuItem() {
        if (scr100FpsMenuItem == null) {
            scr100FpsMenuItem = new JCheckBoxMenuItem();
            scr100FpsMenuItem.setText("100 Frames Per Second");
            scr100FpsMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    getZ88Display().setFrameRate(Z88display.FPS100);
                    getZ88Display().grabFocus();
                }
            });

            scrRefreshRateButtonGroup.add(scr100FpsMenuItem);
        }

        return scr100FpsMenuItem;
    }

    public JCheckBoxMenuItem getSeLayoutMenuItem() {
        if (seLayoutMenuItem == null) {
            seLayoutMenuItem = new JCheckBoxMenuItem();
            seLayoutMenuItem.setText("Swedish/Finish Layout");
            seLayoutMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_SE);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_SE);
                    getZ88Display().grabFocus();
                }
            });

            kbLayoutButtonGroup.add(seLayoutMenuItem);
        }
        return seLayoutMenuItem;
    }

    private JMenuItem getLoadSnapshotMenuItem() {
        if (loadSnapshotMenuItem == null) {
            loadSnapshotMenuItem = new JMenuItem();
            loadSnapshotMenuItem.setText("Load Z88 snapshot");

            loadSnapshotMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    FileSystemView filesys = FileSystemView.getFileSystemView();
                    boolean resumeExecution;

                    SaveRestoreVM srVM = new SaveRestoreVM();
                    JFileChooser chooser = new JFileChooser(filesys.getHomeDirectory());
                    chooser.setDialogTitle("Load/resume Z88 snapshot");
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    chooser.setFileFilter(srVM.getSnapshotFilter());

                    if ((Z88.getInstance().getProcessorThread() != null)) {
                        resumeExecution = true;
                        Z88.getInstance().getProcessor().stopZ80Execution();
                        // if thread is sleeping, there is nothing to stop... so force a wake-up, so Z80 can stop
                        blink.awakeZ80();
                    } else {
                        resumeExecution = false;
                    }

                    int returnVal = chooser.showOpenDialog(getContentPane().getParent());
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        String fileName = chooser.getSelectedFile().getAbsolutePath();

                        try {
                            boolean autorun = srVM.loadSnapShot(fileName);
                            getSlotsPanel().refreshSlotInfo();
                            OZvm.displayRtmMessage("Snapshot successfully installed from " + fileName);
                            setWindowTitle("[" + (chooser.getSelectedFile().getName()) + "]");

                            if (autorun == true) {
                                // debugging is disabled while full screen mode is enabled
                                Z88.getInstance().runZ80Cpu();
                            } else {
                                DebugGui.getInstance().activateDebugCommandLine(); // Activate Debug Command Line Window...
                                CommandLine.getInstance().cmdlineFirstSingleStep();
                            }
                        } catch (IOException e1) {
                            Z88.getInstance().getMemory().setDefaultSystem();
                            Z88.getInstance().getProcessor().reset();
                            blink.resetBlink();
                            OZvm.displayRtmMessage("Loading of snapshot '" + fileName + "' failed. Z88 preset to default system.");
                            JOptionPane.showMessageDialog(null, "Loading of snapshot '" + fileName + "' failed. Z88 preset to default system.", "Z88 Snapshot", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        // User aborted Loading of snapshot..
                        if (resumeExecution == true) {
                            Z88.getInstance().runZ80Cpu();
                        }
                    }

                    // define LCD dimension, based on Blink SCW,SCH loaded from snapshot
                    getZ88Display().setBlinkLcdResolution();
                    getZ88Display().setScreenMessage("");

                    redrawGuiWindows(getScreenDoubleSizeMenuItem().getState());
                }
            });
        }

        return loadSnapshotMenuItem;
    }

    public void centerGui() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] allDevices = env.getScreenDevices();
        int topLeftX, topLeftY, screenX, screenY, windowPosX, windowPosY;

        topLeftX = allDevices[0].getDefaultConfiguration().getBounds().x;
        topLeftY = allDevices[0].getDefaultConfiguration().getBounds().y;

        screenX  = allDevices[0].getDefaultConfiguration().getBounds().width;
        screenY  = allDevices[0].getDefaultConfiguration().getBounds().height;

        windowPosX = ((screenX - this.getWidth())  / 2) + topLeftX;
        windowPosY = ((screenY - this.getHeight()) / 2) + topLeftY;

        this.setLocation(windowPosX, windowPosY);

    }

    private JMenuItem getCreateSnapshotMenuItem() {
        if (createSnapshotMenuItem == null) {
            createSnapshotMenuItem = new JMenuItem();
            createSnapshotMenuItem.setText("Save Z88 snapshot");

            createSnapshotMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    FileSystemView filesys = FileSystemView.getFileSystemView();
                    boolean autorun;

                    SaveRestoreVM srVM = new SaveRestoreVM();
                    JFileChooser chooser = new JFileChooser(filesys.getHomeDirectory());
                    chooser.setDialogTitle("Save Z88 snapshot");
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    chooser.setFileFilter(srVM.getSnapshotFilter());
                    chooser.setSelectedFile(new File(OZvm.defaultVmFile));

                    if ((Z88.getInstance().getProcessorThread() != null)) {
                        autorun = true;
                        Z88.getInstance().getProcessor().stopZ80Execution();
                        // if thread is sleeping, there is nothing to stop... so force a wake-up, so Z80 can stop
                        blink.awakeZ80();

                    } else {
                        autorun = false;
                    }

                    int returnVal = chooser.showSaveDialog(getContentPane().getParent());
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        String fileName = chooser.getSelectedFile().getAbsolutePath();
                        try {
                            srVM.storeSnapShot(fileName, autorun);
                            OZvm.displayRtmMessage("Snapshot successfully created in " + fileName);
                            setWindowTitle("[" + (chooser.getSelectedFile().getName()) + "]");
                        } catch (IOException e1) {
                            JOptionPane.showMessageDialog(null, "Creating snapshot '" + fileName + "' failed.", "Z88 Snapshot", JOptionPane.ERROR_MESSAGE);
                            OZvm.displayRtmMessage("Creating snapshot '" + fileName + "' failed.");
                        }
                    }

                    // the LAF changes sometimes affect the gui,
                    // redraw the slots panel and all is nice again...
                    getSlotsPanel().repaint();

                    if (autorun == true) {
                        // Z80 engine was temporary stopped, now continue to execute...
                        Z88.getInstance().runZ80Cpu();
                        Z88.getInstance().getDisplay().grabFocus(); // default keyboard input   focus to the Z88
                    }
                }
            });
        }
        return createSnapshotMenuItem;
    }

    private JMenu getZ88Menu() {
        if (z88Menu == null) {
            z88Menu = new JMenu();
            z88Menu.setText("Z88");
            z88Menu.add(getSoftResetMenuItem());
            z88Menu.add(getHardResetMenuItem());
            z88Menu.add(getDebugCmdlineMenuItem());
            z88Menu.add(getInstallRomMenuItem());
            z88Menu.addSeparator();
            z88Menu.add(getZ80CpuRealSpeedMenuItem());
            z88Menu.add(getShowSlotsMenuItem());
            z88Menu.add(getBeeper3200hzMenuItem());
            z88Menu.add(getScreenDoubleSizeMenuItem());
            z88Menu.add(getScreenResolutionMenu());
            z88Menu.add(getScreenRefreshRateMenu());
            z88Menu.add(get512KRomTypeMenu());
            z88Menu.add(getBattLowIntMenuItem());
        }

        return z88Menu;
    }

    public void redrawGuiWindows(boolean screenDoubleSize) {

        if (zb != null) {
            zb.dispose();
        }

        if (getKeyboardPanel().isVisible() | getSlotsPanel().isVisible()) {
            zb = new Z88BodyGui(getKeyboardPanel(), getSlotsPanel());
        }

        displayZ88ScreenPanel(false);
        getZ88Display().setDoubleScreenSize(screenDoubleSize);
        displayZ88ScreenPanel(true);

        Gui.this.pack();
        centerGui();

        toFront();
        getZ88Display().grabFocus();
    }

    public JCheckBoxMenuItem getScreenDoubleSizeMenuItem() {

        if (screenDoubleSizeMenuItem == null) {
            screenDoubleSizeMenuItem = new JCheckBoxMenuItem();
            screenDoubleSizeMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
            screenDoubleSizeMenuItem.setText("Screen Double Size");
            screenDoubleSizeMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    redrawGuiWindows(screenDoubleSizeMenuItem.getState());
                }
            });

        }

        return screenDoubleSizeMenuItem;
    }

    private JMenuItem getSoftResetMenuItem() {
        if (softResetMenuItem == null) {
            softResetMenuItem = new JMenuItem();
            softResetMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
            softResetMenuItem.setText("Soft Reset");
            softResetMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (JOptionPane.showConfirmDialog(Gui.this, "Soft Reset Z88?") == JOptionPane.YES_OPTION) {
                        Z88.getInstance().pressResetButton();
                    }
                }
            });
        }
        return softResetMenuItem;
    }

    private JMenuItem getHardResetMenuItem() {
        if (hardResetMenuItem == null) {
            hardResetMenuItem = new JMenuItem();
            hardResetMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
            hardResetMenuItem.setText("Hard Reset");
            hardResetMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (JOptionPane.showConfirmDialog(Gui.this, "Hard Reset Z88?") == JOptionPane.YES_OPTION) {
                        Z88.getInstance().hardReset();
                    }
                }
            });
        }
        return hardResetMenuItem;
    }

    private JMenuItem getInstallRomMenuItem() {
        if (installRomMenuItem == null) {
            installRomMenuItem = new JMenu();
            installRomMenuItem.setText("Install ROM in Slot 0");
            installRomMenuItem.add(getInstallOz47Rom());
            installRomMenuItem.add(getInstallOz46Rom());
            installRomMenuItem.add(getInstallOz45Rom());
            installRomMenuItem.add(getInstallOz44Rom());
            installRomMenuItem.add(getInstallOz43Rom());
            installRomMenuItem.add(getInstallUk400Rom());
            installRomMenuItem.add(getInstallUk300Rom());
            installRomMenuItem.add(getInstallUk220Rom());
            installRomMenuItem.add(getInstallFr326Rom());
            installRomMenuItem.add(getInstallEs319Rom());
            installRomMenuItem.add(getInstallDe318Rom());
            installRomMenuItem.add(getInstallIt323Rom());
            installRomMenuItem.add(getInstallDk321Rom());
            installRomMenuItem.add(getInstallSe250Rom());
            installRomMenuItem.add(getInstallNo260Rom());
            installRomMenuItem.add(getInstallFi401Rom());
            installRomMenuItem.add(getInstallHe313Rom());
            installRomMenuItem.add(getInstallTk317Rom());
        }

        return installRomMenuItem;
    }

    private JMenuItem getInstallOz43Rom() {
        if (installOz43RomMenuItem == null) {
            installOz43RomMenuItem = new JMenuItem();
            installOz43RomMenuItem.setText("OZ V4.3.1 ROM");
            installOz43RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {

                        if (JOptionPane.showConfirmDialog(Gui.this, "Install OZ V4.3.1 ROM in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz431 = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88OZ431.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz431.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz431);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installOz43RomMenuItem;
    }

    private JMenuItem getInstallOz44Rom() {
        if (installOz44RomMenuItem == null) {
            installOz44RomMenuItem = new JMenuItem();
            installOz44RomMenuItem.setText("OZ V4.4 ROM");
            installOz44RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install OZ V4.4 ROM in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz44 = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88OZ441.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz44.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz44);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installOz44RomMenuItem;
    }

    private JMenuItem getInstallOz45Rom() {
        if (installOz45RomMenuItem == null) {
            installOz45RomMenuItem = new JMenuItem();
            installOz45RomMenuItem.setText("OZ V4.5 ROM");
            installOz45RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install OZ V4.5 ROM in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz45 = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88OZ45.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz45.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz45);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installOz45RomMenuItem;
    }

    private JMenuItem getInstallOz46Rom() {
        if (installOz46RomMenuItem == null) {
            installOz46RomMenuItem = new JMenuItem();
            installOz46RomMenuItem.setText("OZ V4.6 ROM");
            installOz46RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install OZ V4.6 ROM in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz46 = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88OZ46.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz46.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz46);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installOz46RomMenuItem;
    }

    private JMenuItem getInstallOz47Rom() {
        if (installOz47RomMenuItem == null) {
            installOz47RomMenuItem = new JMenuItem();
            installOz47RomMenuItem.setText("OZ V4.7 ROM");
            installOz47RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install OZ V4.7 ROM in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz47 = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88OZ47.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz47.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz47);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installOz47RomMenuItem;
    }

    private JMenuItem getInstallUk400Rom() {
        if (installUk400RomMenuItem == null) {
            installUk400RomMenuItem = new JMenuItem();
            installUk400RomMenuItem.setText("British OZ V4.0 ROM");
            installUk400RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install British OZ V4.0 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz40uk = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88UK400.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz40uk.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz40uk);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installUk400RomMenuItem;
    }

    private JMenuItem getInstallUk300Rom() {
        if (installUk300RomMenuItem == null) {
            installUk300RomMenuItem = new JMenuItem();
            installUk300RomMenuItem.setText("British OZ V3.0 ROM");
            installUk300RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install British OZ V3.0 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz30uk = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88UK300.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz30uk.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz30uk);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installUk300RomMenuItem;
    }

    private JMenuItem getInstallUk220Rom() {
        if (installUk220RomMenuItem == null) {
            installUk220RomMenuItem = new JMenuItem();
            installUk220RomMenuItem.setText("British OZ V2.2 ROM");
            installUk220RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install British OZ V2.2 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz22uk = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88UK220.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz22uk.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz22uk);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installUk220RomMenuItem;
    }

    private JMenuItem getInstallFr326Rom() {
        if (installFr326RomMenuItem == null) {
            installFr326RomMenuItem = new JMenuItem();
            installFr326RomMenuItem.setText("French OZ V3.26 ROM");
            installFr326RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install French OZ V3.26 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88FR326.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_FR);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installFr326RomMenuItem;
    }

    private JMenuItem getInstallEs319Rom() {
        if (installEs319RomMenuItem == null) {
            installEs319RomMenuItem = new JMenuItem();
            installEs319RomMenuItem.setText("Spanish OZ V3.19 ROM");
            installEs319RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Spanish OZ V3.19 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88ES319.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installEs319RomMenuItem;
    }

    private JMenuItem getInstallDe318Rom() {
        if (installDe318RomMenuItem == null) {
            installDe318RomMenuItem = new JMenuItem();
            installDe318RomMenuItem.setText("German OZ V3.18 ROM");
            installDe318RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install German OZ V3.18 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88DE318.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installDe318RomMenuItem;
    }

    private JMenuItem getInstallIt323Rom() {
        if (installIt323RomMenuItem == null) {
            installIt323RomMenuItem = new JMenuItem();
            installIt323RomMenuItem.setText("Italian OZ V3.23 ROM");
            installIt323RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Italian OZ V3.23 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88IT323.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installIt323RomMenuItem;
    }

    private JMenuItem getInstallDk321Rom() {
        if (installDk321RomMenuItem == null) {
            installDk321RomMenuItem = new JMenuItem();
            installDk321RomMenuItem.setText("Danish OZ V3.21 ROM");
            installDk321RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Danish OZ V3.21 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88DK321.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_DK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installDk321RomMenuItem;
    }

    private JMenuItem getInstallSe250Rom() {
        if (installSe250RomMenuItem == null) {
            installSe250RomMenuItem = new JMenuItem();
            installSe250RomMenuItem.setText("Swedish OZ V2.50 ROM");
            installSe250RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Swedish OZ V2.50 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88SE250.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_SE);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installSe250RomMenuItem;
    }

    private JMenuItem getInstallNo260Rom() {
        if (installNo260RomMenuItem == null) {
            installNo260RomMenuItem = new JMenuItem();
            installNo260RomMenuItem.setText("Norwegian OZ V2.60 ROM");
            installNo260RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Norwegian OZ V2.60 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88NO260.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_DK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installNo260RomMenuItem;
    }

    private JMenuItem getInstallFi401Rom() {
        if (installFi401RomMenuItem == null) {
            installFi401RomMenuItem = new JMenuItem();
            installFi401RomMenuItem.setText("Finnish OZ V4.01 ROM");
            installFi401RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Finnish OZ V4.01 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88FI401.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");

                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_SE);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installFi401RomMenuItem;
    }

    private JMenuItem getInstallHe313Rom() {
        if (installHe313RomMenuItem == null) {
            installHe313RomMenuItem = new JMenuItem();
            installHe313RomMenuItem.setText("Swiss OZ V3.13 ROM");
            installHe313RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Swiss OZ V3.13 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88HE313.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");
                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installHe313RomMenuItem;
    }

    private JMenuItem getInstallTk317Rom() {
        if (installTk317RomMenuItem == null) {
            installTk317RomMenuItem = new JMenuItem();
            installTk317RomMenuItem.setText("Turkish OZ V3.17 ROM");
            installTk317RomMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (Z88.getInstance().getProcessorThread() != null) {
                        if (JOptionPane.showConfirmDialog(Gui.this, "Install Turkish OZ V3.17 in slot 0?") == JOptionPane.YES_OPTION) {
                            try {
                                File romFileOz = new File(URI.create("file:" + OZvm.getInstance().getAppPath() + "roms/Z88TK317.rom"));
                                OZvm.getInstance().getGui().setWindowTitle("[" + (romFileOz.getName()) + "]");

                                getUkLayoutMenuItem().setSelected(true);
                                Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                                Z88.getInstance().hardResetWithSlot0Rom(romFileOz);
                                getSlotsPanel().refreshSlotInfo();

                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file couldn't be opened!");
                            } catch (IllegalArgumentException ex) {
                                JOptionPane.showMessageDialog(Gui.this, "Selected file was not a Z88 ROM!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(Gui.this, "Z88 is not running");
                    }
                }
            });
        }

        return installTk317RomMenuItem;
    }

    private JMenu getScreenResolutionMenu() {
        if (screenResolutionMenu == null) {
            screenResolutionMenu = new JMenu();
            screenResolutionMenu.setText("LCD");
            screenResolutionMenu.add(getScreen640x64MenuItem());
            screenResolutionMenu.add(getScreen640x320MenuItem());
            screenResolutionMenu.add(getScreen640x480MenuItem());
            screenResolutionMenu.add(getScreen800x320MenuItem());
            screenResolutionMenu.add(getScreen800x480MenuItem());
        }

        return screenResolutionMenu;
    }

    private JMenu getScreenRefreshRateMenu() {
        if (screenRefreshRateMenu == null) {
            screenRefreshRateMenu = new JMenu();
            screenRefreshRateMenu.setText("Screen Fresh Rate");
            screenRefreshRateMenu.add(getScreen10FpsMenuItem());
            screenRefreshRateMenu.add(getScreen25FpsMenuItem());
            screenRefreshRateMenu.add(getScreen50FpsMenuItem());
            screenRefreshRateMenu.add(getScreen100FpsMenuItem());
        }

        return screenRefreshRateMenu;
    }

    private JMenu get512KRomTypeMenu() {
        if (_512KRomTypeMenu == null) {
            _512KRomTypeMenu = new JMenu();
            _512KRomTypeMenu.setText("512K ROM Flash Chip");
            _512KRomTypeMenu.add(getAmd512KRomTypeMenuItem());
            _512KRomTypeMenu.add(getStm512KRomTypeMenuItem());
            _512KRomTypeMenu.add(getSst512KRomTypeMenuItem());

        }

        return _512KRomTypeMenu;
    }

    public JCheckBoxMenuItem getAmd512KRomTypeMenuItem() {
        if (amd512KRomTypeMenuItem == null) {
            amd512KRomTypeMenuItem = new JCheckBoxMenuItem();
            amd512KRomTypeMenuItem.setText("AM29F040B 512K");
            amd512KRomTypeMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getMemory().setRomSlotFlashType(SlotInfo.AmdFlashCard);
                }
            });

            _512KRomTypeButtonGroup.add(amd512KRomTypeMenuItem);
        }

        return amd512KRomTypeMenuItem;
    }

    public JCheckBoxMenuItem getStm512KRomTypeMenuItem() {
        if (stm512KRomTypeMenuItem == null) {
            stm512KRomTypeMenuItem = new JCheckBoxMenuItem();
            stm512KRomTypeMenuItem.setText("ST29F040B 512K");
            stm512KRomTypeMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getMemory().setRomSlotFlashType(SlotInfo.StmFlashCard);
                }
            });

            _512KRomTypeButtonGroup.add(stm512KRomTypeMenuItem);
        }

        return stm512KRomTypeMenuItem;
    }

    public JCheckBoxMenuItem getSst512KRomTypeMenuItem() {
        if (sst512KRomTypeMenuItem == null) {
            sst512KRomTypeMenuItem = new JCheckBoxMenuItem();
            sst512KRomTypeMenuItem.setText("SST39FS040 512K");
            sst512KRomTypeMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    Z88.getInstance().getMemory().setRomSlotFlashType(SlotInfo.SstFlashCard);
                }
            });

            _512KRomTypeButtonGroup.add(sst512KRomTypeMenuItem);
        }

        return sst512KRomTypeMenuItem;
    }



    private JMenu getCreateScreenMenu() {
        if (screenMenu == null) {
            screenMenu = new JMenu();
            screenMenu.setText("Create Screen");
            screenMenu.add(getCreateScreenSnapshotMenuItem());
            screenMenu.add(getCreateGifMovieMenuItem());
        }
        return screenMenu;
    }

    private JMenuItem getCreateScreenSnapshotMenuItem() {
        if (screenSnapshotMenuItem == null) {
            screenSnapshotMenuItem = new JMenuItem();
            screenSnapshotMenuItem.setText("Screenshot as PNG image");
            screenSnapshotMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7, ActionEvent.CTRL_MASK));
            screenSnapshotMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // grab a copy of the current screen frame and write it to file "./z88screenX.png" (X = counter).
                    getZ88Display().grabScreenFrameToFile();
                }
            });
        }
        return screenSnapshotMenuItem;
    }

    private JMenuItem getCreateGifMovieMenuItem() {
        if (gifMovieMenuItem == null) {
            gifMovieMenuItem = new JMenuItem();
            gifMovieMenuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // record an animated Gif movie of the Z88 screen activity
                    getZ88Display().toggleMovieRecording();
                }
            });
            gifMovieMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
            gifMovieMenuItem.setText("GIF movie (start/stop toggle)");
        }
        return gifMovieMenuItem;
    }

    /**
     * Set the window title which is appended after the 'OZvm VX ' text
     *
     * @param title
     */
    public void setWindowTitle(String title) {
        this.setTitle("OZvm " + OzvmVersion.release + "  " + title);
    }

    /**
     * This method initializes the main z88 window with screen menus, runtime
     * messages and keyboard.
     */
    private void initialize() {
        zb = null;
        slotsPanel = null;
        blink = Z88.getInstance().getBlink();
        z80proc = Z88.getInstance().getProcessor();

        kbLayoutButtonGroup = new ButtonGroup();
        scrRefreshRateButtonGroup = new ButtonGroup();
        scrResolutionButtonGroup = new ButtonGroup();
        _512KRomTypeButtonGroup = new ButtonGroup();

        // Main Gui window is never resizable
        setResizable(false);
        setIconImage(new ImageIcon(this.getClass().getResource("/pixel/title.gif")).getImage());
        setJMenuBar(getMainMenuBar());

        getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        setBackground(Color.BLACK);
        getContentPane().add(getToolBar());

        setWindowTitle("");

        // preset default slot 0 512K type to AM29F040B
        getAmd512KRomTypeMenuItem().setSelected(true);

        // pre-select the Screen Refresh Rate Menu Item
        switch (getZ88Display().getCurrentFrameRate()) {
            case Z88display.FPS10:
                getScreen10FpsMenuItem().setSelected(true);
                break;
            case Z88display.FPS25:
                getScreen25FpsMenuItem().setSelected(true);
                break;
            case Z88display.FPS50:
                getScreen50FpsMenuItem().setSelected(true);
                break;
            case Z88display.FPS100:
                getScreen100FpsMenuItem().setSelected(true);
                break;
        }

        // pre-select the keyboard layout Menu Item
        switch (Z88.getInstance().getKeyboard().getKeyboardLayout()) {
            case Z88Keyboard.COUNTRY_UK:
            case Z88Keyboard.COUNTRY_US:
                getUkLayoutMenuItem().setSelected(true);
                break;
            // swedish/finish
            case Z88Keyboard.COUNTRY_SE:
                getSeLayoutMenuItem().setSelected(true);
                break;
            case Z88Keyboard.COUNTRY_DK:
                getDkLayoutMenuItem().setSelected(true);
                break;
            case Z88Keyboard.COUNTRY_FR:
                getFrLayoutMenuItem().setSelected(true);
                break;
            default:
                getUkLayoutMenuItem().setSelected(true);
        }
    }
}
