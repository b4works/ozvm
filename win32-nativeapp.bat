:: *************************************************************************************
:: Build script for ExcelsiorJet V7.6 Windows 32 Native application of OZvm for X86
:: (C) Gunther Strube (hello@bits4fun.net) 2000-2024
::
:: OZvm is free software; you can redistribute it and/or modify it under the terms of the
:: GNU General Public License as published by the Free Software Foundation;
:: either version 2, or (at your option) any later version.
:: OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
:: without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:: See the GNU General Public License for more details.
:: You should have received a copy of the GNU General Public License along with OZvm;
:: see the file COPYING. If not, write to the
:: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
:: *************************************************************************************

:: ExcelsiorJet must have been installed and available via PATH

:: first compile jar file
cmd /c makejar.bat

jc =p =a ".\win32-z88.prj"

:: cleanup prev. build of distributable
rmdir /S /Q win32-z88-native >nul

:: generate ozvm as distributable with ROM images
xpack win32-z88.jpn
