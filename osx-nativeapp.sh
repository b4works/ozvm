# *************************************************************************************
# # ExcelsiorJet Compile script for OZvm on OSX - the Z88 emulator.
# (C) Gunther Strube (hello@bits4fun.net) 2000-2024
#
# OZvm is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with OZvm;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *************************************************************************************


# name for app bundle and for Dock label
export SHORT_NAME=Z88

# name for menu bar, Activity Monitor and etc.
export LONG_NAME="Cambridge Z88 Emulator"

# unique identifier of you application
export BUNDLE_ID=com.jira.cambridgez88

# path to self-contained directory created by JetPackII
export SCD=$PWD/osx-z88-native

# name of executable, must be in the root of SCD
export EXE=z88

# path to directory with icon file
export ICON_PATH=$PWD/installer
# name of icon file (in ICNS format)
export ICON_NAME=1024px-Cambridge-Z88.icns

# version as three period-separated integers
export VER=1.3.0

# first compile OZvm sources to z88.jar
./makejar.sh

# run Jet compiler
jc =p =a "./osx-z88.prj"

rm -fR ./osx-z88-native
xpack "./osx-z88.jpn"

# final step, build the OSX Application from the Jet executable and runtime files in /z88-native
# ensure native application is executable
chmod +x $SCD/$EXE

APP=$SHORT_NAME.app
rm -fR $APP

if [ -d "$APP" ]; then
  echo Error: the application bundle \"$APP\" already exists.
  echo Remove it or change the SHORT_NAME of the new application bundle.
  exit 1
fi

mkdir $APP
mkdir $APP/Contents
mkdir $APP/Contents/MacOS
mkdir $APP/Contents/Resources

cat > $APP/Contents/Info.plist << EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>CFBundlePackageType</key>
  <string>APPL</string>
  <key>CFBundleExecutable</key>
  <string>$EXE</string>
  <key>CFBundleName</key>
  <string>$LONG_NAME</string>
  <key>CFBundleIdentifier</key>
  <string>$BUNDLE_ID</string>
  <key>CFBundleShortVersionString</key>
  <string>$VER</string>
  <key>CFBundleIconFile</key>
  <string>$ICON_NAME</string>
</dict>
</plist>
EOF

cp -r $SCD/ $APP/Contents/MacOS
cp $ICON_PATH/$ICON_NAME $APP/Contents/Resources

echo Successfully created the application bundle \"$APP\".
echo Open it by double-clicking in Finder or using the open command:
echo "    $ open $APP"
