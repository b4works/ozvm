:: *************************************************************************************
:: Windows compile script for OZvm - the Z88 emulator.
:: (C) Gunther Strube (hello@bits4fun.net) 2000-2024
::
:: OZvm is free software; you can redistribute it and/or modify it under the terms of the
:: GNU General Public License as published by the Free Software Foundation;
:: either version 2, or (at your option) any later version.
:: OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
:: without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
:: See the GNU General Public License for more details.
:: You should have received a copy of the GNU General Public License along with OZvm;
:: see the file COPYING. If not, write to the
:: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
::
:: ------------------------------------------------------------------------------------
:: Before executing this script, Git uility and a Java Runtime Environment (JRE) V1.7 or
:: later must have been installed.
::
:: To test the availablity of command line java interpreter, just type "java -version".
:: This will display the version and the runtime options to your console window.
:: ------------------------------------------------------------------------------------
::
:: *************************************************************************************
@echo off

git.exe 2>nul >nul
if ERRORLEVEL 9009 goto GIT_NOTAVAILABLE
java.exe 2>nul >nul
if ERRORLEVEL 9009 goto JAVA_NOTAVAILABLE

:: output current Git revision as a 32bit hex string, to be included as part of Ozvm release information
set revision_class_file=%cd%\src\com\gitlab\z88\ozvm\GitRevision.java
git.exe log  -n 1 --pretty=format:"package com.gitlab.z88.ozvm; public class GitRevision { public static String commit = \"%%h\";}" > %revision_class_file%

:: create a temporary dir for files to be included in the executable JAR file
mkdir ozvm-builddir >nul

:: compile the java classes of the project
echo compiling java classes
java -jar jdk\ecj.jar -d ozvm-builddir -nowarn -g:none -source 1.6 -target 1.6 src\com

:: copy the application files to included in JAR (without hidden files)
echo building executable jar
xcopy src\pixel ozvm-builddir\pixel /S /Y /I /Q >nul

:: finally, build the executable jar to <Project Home>\bin
cd ozvm-builddir >nul
java -jar ..\jdk\makejar.jar -cm ..\z88.jar ..\src\META-INF\MANIFEST.MF .  >nul

:: clean up the temp build stuff
cd ..
rmdir /S /Q ozvm-builddir >nul

if "%Z88WORKBENCH_HOME%"=="" goto END
:: copy Z88 Java executable to <Project Home>/bin
xcopy z88.jar "%Z88WORKBENCH_HOME%\bin" /S /Y /I /Q >nul
:: copy Z88 ROMs to <Project Home>/bin which is needed by z88.jar
xcopy roms "%Z88WORKBENCH_HOME%\bin" /S /Y /I /Q >nul
goto END

:GIT_NOTAVAILABLE
echo Git revision tool not found in PATH
EXIT

:JAVA_NOTAVAILABLE
echo JRE environment not available on system
EXIT

:END
echo z88.jar completed.
